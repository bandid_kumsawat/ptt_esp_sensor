import React from 'react';
import * as d3 from 'd3';
import './style.css'
import json_data_file from './data.json'
import DynaChart1 from './dynachart1.json'
import DynaChart2 from './dynachart2.json'
import _ from 'underscore'

class LineChart extends React.Component {
    constructor(){
        super();
        this.myRef = React.createRef(); 
    }

    componentDidMount(){
        this.LineChartFunction();
    }
    LineChartFunction() {
        var margin = {top: 10, right: 30, bottom: 30, left: 60},
            width = 460 - margin.left - margin.right,
            height = 400 - margin.top - margin.bottom;
    
        var SVG = d3.select(this.myRef.current)
            .append("svg")
              .attr("width", width + margin.left + margin.right)
              .attr("height", height + margin.top + margin.bottom)
            .append("g")
              .attr("transform",
                    "translate(" + margin.left + "," + margin.top + ")");

        var div = d3.select("body").append("div")
            .attr("class", "tooltip")
            .style("opacity", 0);

        var x = d3.scaleLinear()
            .domain([0,1])
            .range([ 0, width ]);
        var xAxis = SVG.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x));
    
        var y = d3.scaleLinear()
            .domain([0, 1])
            .range([ height, 0]);
        
        var yAxis = SVG.append("g")
            .call(d3.axisLeft(y));
    
        var clip = SVG.append("defs").append("SVG:clipPath")
            .attr("id", "clip")
            .append("SVG:rect")
            .attr("width", width )
            .attr("height", height )
            .attr("x", 0)
            .attr("y", 0);
    
        var scatter = []
        const color = "#0971bb"
        var i = 0;
        console.log(JSON.parse(window.localStorage.getItem('dynaimage'))) 
        var data = _.groupBy(JSON.parse(window.localStorage.getItem('dynaimage')), 'date');

        for (let name in data) {
            data[name][0].x = Math.random()
            data[name][0].y = Math.random()
        }

        console.log(JSON.stringify(data))
        for (let name in DynaChart1){

            scatter[i] = SVG.append('g')
                .attr("clip-path", "url(#clip)")
            scatter[i].selectAll("circle")
                .data(DynaChart1[name])
                .enter()
                .append("circle")
                    .attr("cx", function (d) { return x(d.x); } )
                    .attr("cy", function (d) { return y(d.y); } )
                    .attr("r", 2)
                    .style("fill", color)
                    .style("opacity", 1)
                    .on('mouseover', function (d, i) {
                        d3.select(this).transition()
                            .duration('100')
                            .attr("r", 6);
                        div.transition()
                            .duration(50)
                            .style("opacity", 1);
                        var str = d.WellName + "<br> [" + name + "]"
                        div.html(str)
                            .style("left", (d3.event.pageX + 10) + "px")
                            .style("top", (d3.event.pageY - 15) + "px");
                    })
                    .on('mouseout', function (d, i) {
                        d3.select(this).transition()
                            .duration('100')
                            .attr("r", 4);
                        div.transition()
                            .duration(50)
                            .style("opacity", 0);
                    })
                    .on('click', function (d, i) {
                        console.log(d3.event)
                        console.log(d)
                    })
            i++
        }
                    
        const updateChart = () => {
            var newX = d3.event.transform.rescaleX(x);
            var newY = d3.event.transform.rescaleY(y);
        
            xAxis.call(d3.axisBottom(newX))
            yAxis.call(d3.axisLeft(newY))
        
            for (let i = 0; i < scatter.length;i++){
                scatter[i]
                    .selectAll("circle")
                    .attr('cx', function(d) {return newX(d.x)})
                    .attr('cy', function(d) {return newY(d.y)});
                scatter[i]
                    .selectAll("circle")
                    .attr('cx', function(d) {return newX(d.x)})
                    .attr('cy', function(d) {return newY(d.y)});
            }
        }

        var zoom = d3.zoom()
            .scaleExtent([-100, 100])  
            .extent([[0, 0], [width, height]])
            .on("zoom", updateChart);

        // SVG.append("rect")
        //     .attr("width", width)
        //     .attr("height", height)
        //     .style("fill", "none")
        //     .style("pointer-events", "all")
        //     .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
        //     .call(zoom);

    }
    render(){
        return (
            <div ref={this.myRef}></div>
        );
    }
 
}
export default LineChart;