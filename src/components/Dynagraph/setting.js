import React, { Component } from 'react';

import {
  Container, Row, Col, Card, ListGroup, Button
} from 'react-bootstrap';
import * as Icon from 'react-bootstrap-icons';

class setting extends Component {

  constructor() {
    super();
    this.state = {
      count: 0,
      number: [1,2,3,4,5,6,7,8,9,10]
    };

    // This binding is necessary to make `this` work in the callback
    this.handleClickAdd = this.handleClickAdd.bind(this);
    this.handleClickSub = this.handleClickSub.bind(this);
  }


  handleClickAdd = (e) => {
    e.preventDefault();
    console.log('The link was clicked.');
    this.setState(state => ({
      count: state.count++
    }));
    console.log(this.state.count)
  }

  handleClickSub = (e) => {
    e.preventDefault();
    console.log('The link was clicked.');
    this.setState(state => ({
      count: state.count--
    }));
    console.log(this.state.count)
  }

  show_card = () => {
    return (
      <>
        <div>
          <Container>
            {
              this.state.number.map((item, index) => (
                <Row key={index.toString()} className={'pt-2 ' + (index === (this.state.number.length - 1) ? 'pb-5' : '')}>
                  <Col md={12}>
                    <Card style={{ width: '100%' }}>
                      <Card.Header>Connection {item}</Card.Header>
                      <ListGroup variant="flush">
                        <ListGroup.Item>
                          <Icon.HeartFill /> hello{}<br></br>
                          <Icon.HexagonFill /> hello{}<br></br>
                          <Icon.HouseFill /> hello{}<br></br>
                          <Icon.InfoSquareFill /> hello{}<br></br>
                        </ListGroup.Item>
                        <Button variant="secondary" onClick={this.handleClickAdd}>Click me! +</Button>{this.state.count}
                        <Button variant="secondary" onClick={this.handleClickSub}>Click me! -</Button>{this.state.count}
                      </ListGroup>
                    </Card>
                  </Col>
                </Row>
              ))
            }
          </Container>
        </div>
      </>
    )
  }

  render() {
    return (
      this.show_card()
    );
  }
  
}

export default setting;