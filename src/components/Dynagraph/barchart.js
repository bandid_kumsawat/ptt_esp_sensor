import React from 'react';
import * as d3 from 'd3';
import {
    Button
  } from 'react-bootstrap';

class App extends React.Component {
 constructor(){
    super();
    this.myRef = React.createRef(); 
    this.dataset = [100, 200, 300, 400, 500]
 }
 button_() {
   return (
    <>
      <Button variant="outline-primary" id='group1'>gruop1</Button>
      <Button variant="outline-primary" id='group2'>gruop2</Button>
      <Button variant="outline-primary" id='group3'>gruop3</Button>
      <Button variant="outline-primary" id='group4'>gruop4</Button>
    </>
   )
 }
 componentDidMount(){
    // console.log(this.myRef);
    // let size = 500
    // let svg = d3.select(this.myRef.current)
    //   .append("svg")
    //   .attr("width", size)
    //   .attr("height", size)
    
    // let rect_width = 95
    // svg.selectAll('rect')
    //   .data(this.dataset)
    //   .enter()
    //   .append('rect')
    //   .attr("x", (d, i) => 5 + i * (rect_width + 5))
    //   .attr("y", d => size - d)
    //   .attr("width", rect_width)
    //   .attr("height", d => d)
    //   .attr("fill", "teal")
    //   .on('mouseover', function (d, i) {
    //       d3.select(this).transition()
    //           .duration('50')
    //           .attr('opacity', '.50');
    //   })
    //   .on('mouseout', function (d, i) {
    //       d3.select(this).transition()
    //           .duration('50')
    //           .attr('opacity', '1');
    //   })
    //   .attr('transform', 'translate(0, 0)');
    //18-24
    var group1 = [{
        title: "Strongly Like",
        value: 22
    },
    {
        title: "Somewhat Like",
        value: 35
    },
    {
        title: "Somewhat Dislike",
        value: 15
    },
    {
        title: "Strongly Dislike",
        value: 16
    },
    {
        title: "Other/Not Sure",
        value: 12
    }
    ];

    //25-34
    var group2 = [{
        title: "Strongly Like",
        value: 25
    },
    {
        title: "Somewhat Like",
        value: 39
    },
    {
        title: "Somewhat Dislike",
        value: 14
    },
    {
        title: "Strongly Dislike",
        value: 15
    },
    {
        title: "Other/Not Sure",
        value: 7
    }
    ];

    //35-44
    var group3 = [{
        title: "Strongly Like",
        value: 30
    },
    {
        title: "Somewhat Like",
        value: 40
    },
    {
        title: "Somewhat Dislike",
        value: 12
    },
    {
        title: "Strongly Dislike",
        value: 12
    },
    {
        title: "Other/Not Sure",
        value: 6
    }
    ];
    //45-55
    var group4 = [{
        title: "Strongly Like",
        value: 39
    },
    {
        title: "Somewhat Like",
        value: 39
    },
    {
        title: "Somewhat Dislike",
        value: 9
    },
    {
        title: "Strongly Dislike",
        value: 9
    },
    {
        title: "Other/Not Sure",
        value: 4
    }
    ];
    //55+
    var group5 = [{
        title: "Strongly Like",
        value: 39
    },
    {
        title: "Somewhat Like",
        value: 42
    },
    {
        title: "Somewhat Dislike",
        value: 10
    },
    {
        title: "Strongly Dislike",
        value: 7
    },
    {
        title: "Other/Not Sure",
        value: 2
    }
    ];

    var donutTip = d3.select("body").append("div")
        .attr("class", "donut-tip")
        .style("opacity", 0)


    var width = 600,
    height = 600,
    radius = Math.min(width, height) / 2;

    var color = d3.scaleOrdinal().range(["#5EC9A9", "#AFE4B8", "#539CC6", "#323595", "#C2D5EB"]);

    var pie = d3.pie().value(function (d) {
        return d.value;
    })(group1);

    var arc = d3.arc().outerRadius(radius - 10).innerRadius(0);

    var svg = d3.select(this.myRef.current)
        .append("svg")
        // const svg = d3.create("svg")   //  ถ้าไม่ต้องการเลือก id ให้สร้าง <svg /> เลยแล้วแปลงเป็น string แล้วค่อยเปลี่นเป็น object element ของ react.
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    var g = svg.selectAll("arc")
        .data(pie)
        .enter().append("g")
        .attr("class", "arc")
        .on('click', () => {
            console.log('click !!')
        })
        .on('mouseover', function (d, i) {
            d3.select(this).transition()
                .duration('50')
                .attr('opacity', '.90')

            donutTip.transition()
                .duration(50)
                .style("opacity", 1)
                .style('postion', "absolute !important")
                .style('display', 'block')

            let num = (Math.round(d.value).toString() + " V.")

            donutTip.html(num)
                .style("left", (d3.event.pageX + 10) + "px")
                .style("top", (d3.event.pageY - 15) + "px")
        })
        .on('mouseout', function (d, i) {
            d3.select(this).transition()
                .duration('50')
                .attr('opacity', '1')
            donutTip.transition()
                .duration('50')
                .style("opacity", 0)
                .style('display', 'none')
        })
        .attr('transform', 'translate(0, 0)')

    g.append("path")
        .attr("d", arc)
        .style("fill", function (d) {
            return color(d.data.title);
        });

    const changeData = (data) => {
        var pie = d3.pie()
            .value(function (d) {
                return d.value;
            })(data);

        var path = d3.select(this.myRef.current)
            .selectAll("path")
            .data(pie); // Compute the new angles
        path.transition().duration(500).attr("d", arc); // redrawing the path with a smooth transition
    }
    d3.select("button#group1").on("click", function () {
        changeData(group1);
    })
    d3.select("button#group2").on("click", function () {
        changeData(group2);
    })
    d3.select("button#group3").on("click", function () {
        changeData(group3)
    })
    d3.select("button#group4").on("click", function () {
        changeData(group4)
    })
    d3.select("button#group5").on("click", function () {
        changeData(group5)
    })
 }
 render(){
  return (
    <div>
      <div ref={this.myRef}></div>
      {this.button_()}
    </div>
  );
 }
 
}
export default App;