import 'date-fns';
import React, { Component } from 'react';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';


class MaterialUIPickers extends Component {
  constructor() {
    super()
    this.myRef = React.createRef();
    this.state = {
      selectedDate: new Date(),
      setSelectedDate: new Date(),
      handleDateChange: (date) => {
        console.log(date)
        // this.state.setSelectedDate(date);
        this.setState(state => ({
          setSelectedDate : date
        }))
        this.setState(state => ({
          selectedDate : date
        }))
      }
    };
  }
  DateTimePicker = () => {
    // The first commit of Material-UI
  
    return (
      <MuiPickersUtilsProvider ref={this.myRef} utils={DateFnsUtils}>
          <KeyboardDatePicker
            disableToolbar
            variant="inline"
            format="MM/dd/yyyy"
            margin="normal"
            id="date-picker-inline"
            label=""
            value={this.state.selectedDate}
            onChange={this.state.handleDateChange}
            KeyboardButtonProps={{
              'aria-label': 'change date',
            }}
          />
      </MuiPickersUtilsProvider>
    );
  }
  render() {
    return (
      this.DateTimePicker()
    );
  }
}

export default MaterialUIPickers;