import React, { Component } from "react";
import withStyles from "@material-ui/styles/withStyles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";

// Components
// import ESPFrom from './cards/ESP_From'
import ESPPresentData from './cards/ESP_PresentData'
import Topbar from "./Topbar";

// store
import { store } from './../store'

const backgroundShape = require("../images/shape.svg");

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.grey["A500"],
    overflow: "hidden",
    background: `url(${backgroundShape}) no-repeat`,
    backgroundSize: "cover",
    backgroundPosition: "0 400px",
    marginTop: 0,
    padding: 20,
    paddingBottom: 0
  },
  grid: {
    width: 1000
  }
});

class EspSensor extends Component {
  constructor (){
    super()
    this.state = {
    }
  }
  componentDidMount() {
    // console.log(this.state)
  }
  render() {
    const { classes } = this.props;
    const currentPath = this.props.location.pathname;

    return (
      <React.Fragment>
      <CssBaseline />
      <Topbar currentPath={currentPath} />
      <div className={classes.root}>
        <Grid container justify="center">
          {/* <Grid item xs={12}>
            <ESPFrom store={store}/>
          </Grid> */}
          <Grid item xs={12}>
            <ESPPresentData store={store}/>
          </Grid>
        </Grid>
      </div>
    </React.Fragment>
    );
  }
}

export default withStyles(styles)(EspSensor);
