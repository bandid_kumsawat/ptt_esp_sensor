import React, { Component } from 'react';
import withStyles from '@material-ui/styles/withStyles';
import { red } from '@material-ui/core/colors';
import Grid from "@material-ui/core/Grid";

// More Function
// import API from './../../api/server'
import { 
  selectfthp, 
  selectftht, 
  selectcurrent, 
  selectflowrate, 
  selectmotortemp, 
  selectsuctionpressure, 
  selectsuctiontemp, 
  selectvoltage, 
  selectfrequency, 
  esprentderchart
} from './../../actions';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  root: {
    maxWidth: '100%',
    flexGrow: 1,
  },
  content: {
    fontSize: "16px"
  },
  media: {
    height: 0,
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
  formControl: {
    minWidth: "100%",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  buttonMargin: {
    marginTop: "12px",
    width: '100%'
  },
  buttonFloat: {
    display: 'block',
    float: 'right'
  },
  sbox:{
    paddingRight: '5%',
    paddingLeft: '2%',
    fontWeight: 700
  },
  FlexDirect: {
    flexDirection: 'row',
    display: 'flex',
  },
  CursorSpan: {
    cursor: 'pointer',
    fontWeight: 700
  },
  ActiveSelect: {
    textDecoration: 'line-through',
    color: "#99AAB5"
  }
});

class ESPSelectLine extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.state = {

    }
    this.handleClickFTHP = this.handleClickFTHP.bind(this);
    this.handleClickFTHT = this.handleClickFTHT.bind(this);
    this.handleClickFlowRate = this.handleClickFlowRate.bind(this);
    this.handleClickCurrent = this.handleClickCurrent.bind(this);
    this.handleClickFrequency = this.handleClickFrequency.bind(this);
    this.handleClickMotorTemp = this.handleClickMotorTemp.bind(this);
    this.handleClickSuctionPressure = this.handleClickSuctionPressure.bind(this);
    this.handleClickSuctionTemp = this.handleClickSuctionTemp.bind(this);
    this.handleClickVoltage = this.handleClickVoltage.bind(this);
  }
  async componentDidMount() {

  }
  callFn (){
    this.store.dispatch(esprentderchart(true))
  }
  handleClickFTHP() {
    this.store.dispatch(esprentderchart(false))
    this.store.dispatch(selectfthp(!this.store.getState().SelectFTHP))
    setTimeout(() => { this.callFn() }, 1);
  }
  handleClickFTHT() {
    this.store.dispatch(esprentderchart(false))
    this.store.dispatch(selectftht(!this.store.getState().SelectFTHT))
    setTimeout(() => { this.callFn() }, 1);
  }
  handleClickFlowRate () {
    this.store.dispatch(esprentderchart(false))
    this.store.dispatch(selectflowrate(!this.store.getState().SelectFlowRate))
    setTimeout(() => { this.callFn() }, 1);
  }
  handleClickCurrent () {
    this.store.dispatch(esprentderchart(false))
    this.store.dispatch(selectcurrent(!this.store.getState().SelectCurrent))
    setTimeout(() => { this.callFn() }, 1);
  }
  handleClickFrequency() {
    this.store.dispatch(esprentderchart(false))
    this.store.dispatch(selectfrequency(!this.store.getState().SelectFrequency))
    setTimeout(() => { this.callFn() }, 1);
  }
  handleClickMotorTemp () {
    this.store.dispatch(esprentderchart(false))
    this.store.dispatch(selectmotortemp(!this.store.getState().SelectMotorTemp))
    setTimeout(() => { this.callFn() }, 1);
  }
  handleClickSuctionPressure () {
    this.store.dispatch(esprentderchart(false))
    this.store.dispatch(selectsuctionpressure(!this.store.getState().SelectSuctionPressure))
    setTimeout(() => { this.callFn() }, 1);
  }
  handleClickSuctionTemp() {
    this.store.dispatch(esprentderchart(false))
    this.store.dispatch(selectsuctiontemp(!this.store.getState().SelectSuctionTemp))
    setTimeout(() => { this.callFn() }, 1);
  }
  handleClickVoltage () {
    this.store.dispatch(esprentderchart(false))
    this.store.dispatch(selectvoltage(!this.store.getState().SelectVoltage))
    setTimeout(() => { this.callFn() }, 1);
  }
  render() {
    const { classes } = this.props;
    if (1){
      return (
        <div className={classes.root}>
          <Grid container spacing={3}>
            <Grid item sm={12} className={classes.FlexDirect}>
              
              <div className={classes.sbox}>
                <button className={ 
                  classes.CursorSpan + " " +  ((this.store.getState().SelectFTHP) ? '' : classes.ActiveSelect)  
                } onClick={
                  this.handleClickFTHP
                }
                style={{color: ((this.store.getState().ESPLineColor[0] === undefined || !this.store.getState().SelectFTHP) ? "#99AAB5" : 
                  this.store.getState().ESPLineColor[0].FTHP
                )}}
                >FTHP</button>
              </div>
  
              <div className={classes.sbox}>
                <button className={ 
                  classes.CursorSpan + " " +  ((this.store.getState().SelectFTHT) ? '' : classes.ActiveSelect)  
                } onClick={
                  this.handleClickFTHT
                }
                style={{color: ((this.store.getState().ESPLineColor[0] === undefined || !this.store.getState().SelectFTHT) ? "#99AAB5" : 
                  this.store.getState().ESPLineColor[0].FTHT
                )}}
                >FTHT</button>
              </div>
  
              <div className={classes.sbox}>
                <button className={ 
                  classes.CursorSpan + " " +  ((this.store.getState().SelectFlowRate) ? '' : classes.ActiveSelect)  
                } onClick={
                  this.handleClickFlowRate
                }
                style={{color: ((this.store.getState().ESPLineColor[0] === undefined || !this.store.getState().SelectFlowRate) ? "#99AAB5" : 
                  this.store.getState().ESPLineColor[0].FlowRate
                )}}
                >FlowRate</button>
              </div>
  
              <div className={classes.sbox}>
                <button className={ 
                  classes.CursorSpan + " " +  ((this.store.getState().SelectCurrent) ? '' : classes.ActiveSelect)  
                } onClick={
                  this.handleClickCurrent
                }
                style={{color: ((this.store.getState().ESPLineColor[0] === undefined || !this.store.getState().SelectCurrent) ? "#99AAB5" : 
                  this.store.getState().ESPLineColor[0].Current
                )}}
                >Current</button>
              </div>
  
              <div className={classes.sbox}>
                <button className={ 
                  classes.CursorSpan + " " +  ((this.store.getState().SelectFrequency) ? '' : classes.ActiveSelect)  
                } onClick={
                  this.handleClickFrequency
                }
                style={{color: ((this.store.getState().ESPLineColor[0] === undefined || !this.store.getState().SelectFrequency) ? "#99AAB5" : 
                  this.store.getState().ESPLineColor[0].Frequency
                )}}
                >Frequency</button>
              </div>
  
              <div className={classes.sbox}>
                <button className={ 
                  classes.CursorSpan + " " +  ((this.store.getState().SelectMotorTemp) ? '' : classes.ActiveSelect)  
                } onClick={
                  this.handleClickMotorTemp
                }
                style={{color: ((this.store.getState().ESPLineColor[0] === undefined || !this.store.getState().SelectMotorTemp) ? "#99AAB5" : 
                  this.store.getState().ESPLineColor[0].MotorTemp
                )}}
                >Motor Temp</button>
              </div>
  
              <div className={classes.sbox}>
                <button className={ 
                  classes.CursorSpan + " " +  ((this.store.getState().SelectSuctionPressure) ? '' : classes.ActiveSelect)  
                } onClick={
                  this.handleClickSuctionPressure
                }
                style={{color: ((this.store.getState().ESPLineColor[0] === undefined || !this.store.getState().SelectSuctionPressure) ? "#99AAB5" : 
                  this.store.getState().ESPLineColor[0].SuctionPressure
                )}}
                >Suction Pressure</button>
              </div>
  
              <div className={classes.sbox}>
                <button className={ 
                  classes.CursorSpan + " " +  ((this.store.getState().SelectSuctionTemp) ? '' : classes.ActiveSelect)  
                } onClick={
                  this.handleClickSuctionTemp
                }
                style={{color: ((this.store.getState().ESPLineColor[0] === undefined || !this.store.getState().SelectSuctionTemp) ? "#99AAB5" : 
                  this.store.getState().ESPLineColor[0].SuctionTemp
                )}}
                >Suction Temp</button>
              </div>
  
              <div className={classes.sbox}>
                <button className={ 
                  classes.CursorSpan + " " +  ((this.store.getState().SelectVoltage) ? '' : classes.ActiveSelect)  
                } onClick={
                  this.handleClickVoltage
                }
                style={{color: ((this.store.getState().ESPLineColor[0] === undefined || !this.store.getState().SelectVoltage) ? "#99AAB5" : 
                  this.store.getState().ESPLineColor[0].Voltage
                )}}
                >Voltage</button>
              </div>
              
            </Grid>
          </Grid>
        </div>
      )
    }else {
      return (
        <div></div>
      )
    }

  }
}

export default withStyles(styles)(ESPSelectLine);
