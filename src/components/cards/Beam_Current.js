import React, { Component } from 'react';
import withStyles from '@material-ui/styles/withStyles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { Grid } from '@material-ui/core';
import DynaChart from './Chart/DynaChart'
import BeamPredictionGroup from './Beam_PredictionGroup'
import moment from 'moment'
import { store } from './../../store'
import './Beam_Current.css'
import { setcurrentdevice, setprediction } from './../../actions';

import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';

import _ from 'underscore'

import API from './../../api/server'

const styles = theme => ({
  root: {
    maxWidth: '100%',
    flexGrow: 1,
  },
  hieghtlightchart: {
    borderTop: "3px solid #0a88be"
  },
  content: {
    fontSize: "16px"
  },
  media: {
    height: 0,
    // paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  formControl: {
    minWidth: "100%",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  buttonMargin: {
    marginTop: "12px",
    width: '100%'
  },
  buttonFloat: {
    display: 'block',
    float: 'left'
  },
  containerCenter:{

  },
  title: {
    width: "100%",
    textAlign: "center",
    margin: "20px",
    padding: "8px",
    borderBottom: '1px solid #0000007a',
    fontSize: '20px'
  },
  title2: {
    width: "100%",
    textAlign: "center",
    margin: "0px",
    padding: "8px",
    fontSize: '20px'
  },
  spacingPadding: {
    paddingRight: "2%",
    paddingLeft: "2%",
  }
});

class BeamViewSelect extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.state = {
      localS: localStorage.getItem('select_Dynachart'),
      listDeivce: [],
      selectDevice: '',
      listType: [],
      selectType: ''
    }
    this.handleViewDevice = this.handleViewDevice.bind(this);
    this.handleSelectDevice = this.handleSelectDevice.bind(this)
    this.handleType = this.handleType.bind(this)
  }
  async componentDidMount() {
    const res = await API().get('/dynadata')
    _.sortBy(res.data.device);
    res.data.device.sort()
    this.setState({listDeivce: [...new Set(res.data.device)]});
    _.sortBy(res.data.type);
    res.data.type.sort()
    this.setState({listType: [...new Set(res.data.type)]});

    if (localStorage.getItem('device') === null 
      && localStorage.getItem('date') === null 
      && localStorage.getItem('offset') === null 
      && localStorage.getItem('type_current') === null
    ) {
      localStorage.setItem('device', 'LKU-Z11');
      localStorage.setItem('date', "2020-02-26");
      localStorage.setItem('offset', "10");
      localStorage.setItem('type_current', "FC")
    }

    if (localStorage.getItem('current_device') === null){
      this.setState({selectDevice: 'LKU-R16T'})
      localStorage.setItem('current_device', 'LKU-R16T' )
    } else {
      this.setState({selectDevice: localStorage.getItem('current_device')})
    }
    if (localStorage.getItem('type_current') === null){
      this.setState({selectType: "FC"})
    }else{
      this.setState({selectType: localStorage.getItem('type_current')})
    }
    const res2 = await API().post('/current', {
      "WellName": this.state.selectDevice,
      "type": this.state.selectType
    })
    console.log(res2.data[0])
    this.store.dispatch(setcurrentdevice([res2.data[0]]))
    this.store.dispatch(setprediction([res2.data[((res2.data.length >= 10) ? 10 : res2.data.length - 1)]]))
  }
  handleSelectDevice(ev) {
    this.setState({selectDevice: ev.target.value});
    localStorage.setItem('current_device', ev.target.value);
  }
  handleType(ev){
    this.setState({selectType: ev.target.value});
    localStorage.setItem('type_current', ev.target.value)
  }
  async handleViewDevice(ev) {
    this.store.dispatch(setcurrentdevice([undefined]))
    this.store.dispatch(setprediction([undefined]))
    const res = await API().post('/current', {

      "WellName": this.state.selectDevice,
      "type": this.state.selectType

    })
    this.store.dispatch(setcurrentdevice([res.data[0]]))
    this.store.dispatch(setprediction([res.data[((res.data.length >= 10) ? 10 : res.data.length - 1)]]))
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Card className={classes.hieghtlightchart}>
          <CardContent>
            <Grid container spacing={3}>
            <Grid item sm={4}>
                <FormControl className={classes.formControl}>
                  <InputLabel shrink htmlFor="select-native-label-placeholder">
                    เลือกอุปกรณ์
                  </InputLabel>
                  <NativeSelect
                    value={this.state.selectDevice}
                    onChange={this.handleSelectDevice }
                    inputProps={{
                      name: 'Select Device',
                      id: 'select-native-label-placeholder',
                    }}
                  >
                    <option aria-label="None" value="" />
                    {
                      this.state.listDeivce.map((item, index) => {
                        return (<option key={index} value={item}>{item}</option>)
                      })
                    }
                  </NativeSelect>
                  <FormHelperText></FormHelperText>
                </FormControl>
              </Grid>

              <Grid item sm={4}>
                <FormControl className={classes.formControl}>
                  <InputLabel shrink htmlFor="select-native-label-placeholder-type">
                    เลือกชนิดอุปกรณ์
                  </InputLabel>
                  <NativeSelect
                    value={this.state.selectType}
                    onChange={this.handleType }
                    inputProps={{
                      name: 'Select Type',
                      id: 'select-native-label-placeholder-type',
                    }}
                  >
                    <option aria-label="None" value="" />
                    {
                      this.state.listType.map((item, index) => {
                        return (<option key={index} value={item}>{item}</option>)
                      })
                    }
                  </NativeSelect>
                  <FormHelperText></FormHelperText>
                </FormControl>
              </Grid>

              <Grid item sm={4}>
                <div className={classes.buttonFloat}>  
                  <Button 
                    className={classes.buttonMargin}
                    variant="outlined" 
                    color="default" 
                    disabled={( this.state.selectDevice === "" || this.state.selectDate === "" || this.state.selectOffset === 0)}
                    onClick={this.handleViewDevice}
                  >
                    เลือกดูข้อมูล
                  </Button>
                </div>
              </Grid>

            </Grid>

            <Grid container justify="center">
              <Grid item xs={12}>
                <BeamPredictionGroup store={store} />
              </Grid>
            </Grid>

            <Grid container spacing={3}>
              <Grid item sm={6}>
                <div className={classes.title}>CURRENT</div>
                {
                  ((this.store.getState().currentWell[0] === undefined) ? "" : this.store.getState().currentWell.map((data ,index) => {
                    return (<DynaChart
                      cid={data.id}
                      className={classes.DynaChart}
                      store={store}
                      key={index} 
                      index={index}
                      Loads={data.Loads} 
                      Positions={data.Positions}
                      color='#29abe2'
                      fill='#E8F7FF'
                      width='800'
                      height='500'
                      title={data.WellName}
                      subTitle={data.CardDateTime}
                      hover={false}
                      border={false}
                      onclick={false}
                    />)
                  }))
                }
              </Grid>
              <Grid item sm={6}>
                <div className={classes.title}>PREDICTION</div>
                {
                  ((this.store.getState().currentWellPrediction[0] === undefined) ? "" : this.store.getState().currentWellPrediction.map((data ,index) => {
                    return (<DynaChart
                      cid={data.id}
                      className={classes.DynaChart}
                      store={store}
                      color='#FF46A4'
                      fill="#FFF7FB"
                      key={index} 
                      index={index}
                      Loads={data.Loads} 
                      Positions={data.Positions}
                      width='800'
                      height='500'
                      title={data.WellName}
                      subTitle={moment(this.store.getState().currentWell[index].CardDateTime).add(1, 'days').format('DD-MMMM-YYYY HH:MM:SS')}
                      hover={false}
                      border={false}
                      onclick={false}
                    />)
                  }))
                }
              </Grid>
            </Grid>
            <hr></hr>
            <Grid container spacing={1} >
              <Grid item sm={6} className={classes.spacingPadding}>
                <div className={classes.title2}>Well Information</div>
                <table id="table_detail_current">
                  <tbody>
                    <tr>
                      <td>Well Name</td>
                      <td>
                        {
                          ((this.store.getState().currentWellPrediction[0] === undefined) ? "" : this.store.getState().currentWellPrediction[0].WellName)
                        }
                      </td>
                    </tr>
                    <tr>
                      <td>Card Report Date</td>
                      <td>
                        {
                          ((this.store.getState().currentWellPrediction[0] === undefined) ? "" : this.store.getState().currentWellPrediction[0].CardDateTime)
                        }
                      </td>
                    </tr>
                    <tr>
                      <td>Card Report Type</td>
                      <td>
                        {
                          ((this.store.getState().currentWellPrediction[0] === undefined) ? "" : this.store.getState().currentWellPrediction[0].CardType)
                        }
                      </td>
                    </tr>
                  </tbody>
                </table>
              </Grid>
              <Grid item sm={6} className={classes.spacingPadding}>
                <div className={classes.title2}>Readings</div>
                <table id="table_detail_current">
                  <tbody>
                    <tr>
                      <td>Max Load (lbs)</td>
                      <td>
                        {
                          ((this.store.getState().currentWellPrediction[0] === undefined) ? "" : 
                            // var  max = () => { 
                            //   var temp = Math.max.apply(Math, data.map(function(o) { 
                            //       return o.y;  
                            //   })); 
                            //   return temp
                            // }   
                            // var  min = () => { 
                            //   var temp = Math.min.apply(Math, data.map(function(o) { 
                            //       return o.y;  
                            //   })); 
                            //   return temp
                            // } 
                            Math.min.apply(Math, this.store.getState().currentWellPrediction[0].Loads.map(function(o) { 
                                return o;  
                            }))
                          )
                        }
                      </td>
                    </tr>
                    <tr>
                      <td>Min Load (lbs)</td>
                      <td>
                        {
                          ((this.store.getState().currentWellPrediction[0] === undefined) ? "" : 
                            Math.max.apply(Math, this.store.getState().currentWellPrediction[0].Loads.map(function(o) { 
                              return o;  
                            }))
                          )
                        }
                      </td>
                    </tr>
                    <tr>
                      <td>Well State/ Shutdown Event</td>
                      <td>0</td>
                    </tr>
                    {/* <tr>
                      <td>Gross Stroke (in)</td>
                      <td>0</td>
                    </tr>
                    <tr>
                      <td>Net Stroke (in)</td>
                      <td>0</td>
                    </tr>
                    <tr>
                      <td>Pump Fillage (%)</td>
                      <td>0</td>
                    </tr>
                    <tr>
                      <td>Fluid Load (%)</td>
                      <td>0</td>
                    </tr> */}
                  </tbody>
                </table>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </div>
    )
  }
}

export default withStyles(styles)(BeamViewSelect);