import React, { Component } from 'react';
import * as d3 from 'd3';
import withStyles from '@material-ui/styles/withStyles';
import './ESPLineChart.css'
import moment from 'moment'
import data from './data.json'
import predictMock from './predict.json'
const styles = theme => ({
  root: {
    color: "#000000",
    fontSize: '1em',
    border: "2px solid #8c8b8b44",
    borderTop: "4px solid #8c8b8b44",
    borderBottom: "10px solid #8c8b8b44",
    borderRight: "4px solid #8c8b8b44",
    borderLeft: "4px solid #8c8b8b44",
    borderRadius: "3px"
  },
  rootNoBorder: {
    color: "#000000",
    fontSize: '1em',
  },
  spacePaddingSvg: {
    paddingTop: '10px'
  }
});

class DynaChart extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.myRef = React.createRef();
    this.state = {
    }
    this.width = this.props.width
    this.height = this.props.height
    this.x = Object
    this.y = Object
    this.svg = Object
    this.valueline = Object
    this.classselected = ''
    this.valuePadding = []
    this.btnStatus = {
      "FTHP": true,
      "FTHT": true,
      "Current": true,
      "FlowRate": true,
      "Frequency": true,
      "MotorTemp": true,
      "SuctionPressure": true,
      "SuctionTemp": true,
      "Voltage": true,
    }
  }
  componentDidUpdate() {
  }
  componentDidMount(){
    this.LineChartFunction();
  }
  LineChartFunction(){
    // section tooltip hover data
    var ToolTipFTHT = d3.select("body").append("div")
      .attr("class", "tooltip-dataFTHT")
      .attr("id", "tooltip_dataFTHT")
      .style("opacity", 0);
    var ToolTipFTHP = d3.select("body").append("div")
      .attr("class", "tooltip-dataFTHP")
      .attr("id", "tooltip_dataFTHP")
      .style("opacity", 0);
    // ---------------------------
    var that = this

    var dataFTHT = []
    var dataFTHP = []
    var dataCurrent = []
    var dataFlowRate = []
    var dataFrequency = []
    var dataMotorTemp = []
    var dataSuctionPressure = []
    var dataSuctionTemp = []
    var dataVoltage = []

    var dataAllPredict = []
    var dataAll = []

    var dataPredictFTHT = []
    var dataPredictFTHP = []
    // props data
    // var espdata = this.props.data
    var espdata = data
    var predict = predictMock

    predict.data.forEach((item, index) => {


      dataPredictFTHT.push({
        "x": Number(moment.utc(new Date(item.Time)).format("x")),
        "y": Number(item.FTHT),
        "tag": "FTHT"
      })
      dataAll.push({
        "x": Number(moment.utc(new Date(item.Time)).format("x")),
        "y": Number(item.FTHT),
        "tag": "FTHT"
      })


      dataPredictFTHP.push({
        "x": Number(moment.utc(new Date(item.Time)).format("x")),
        "y": Number(item.FTHP),
        "tag": "FTHP"
      })
      dataAll.push({
        "x": Number(moment.utc(new Date(item.Time)).format("x")),
        "y": Number(item.FTHP),
        "tag": "FTHP Predict"
      })



    })
    espdata.data.forEach((item, index, arr) => {
      // console.log(new Date(item.Time))



      dataAll.push({
        "x": Number(moment.utc(new Date(item.Time)).format("x")),
        "y": Number(item.FTHT),
        "tag": "FTHT"
      })
      dataFTHT.push({
        "x": Number(moment.utc(new Date(item.Time)).format("x")),
        "y": Number(item.FTHT),
        "tag": "FTHT"
      })



      dataAll.push({
        "x": Number(moment.utc(new Date(item.Time)).format("x")),
        "y": Number(item.FTHP),
        "tag": "FTHP"
      })
      dataFTHP.push({
        "x": Number(moment.utc(new Date(item.Time)).format("x")),
        "y": Number(item.FTHP),
        "tag": "FTHP"
      })


      dataAll.push({
        "x": Number(moment.utc(new Date(item.Time)).format("x")),
        "y": Number(item.Current),
        "tag": "Current"
      })
      dataCurrent.push({
        "x": Number(moment.utc(new Date(item.Time)).format("x")),
        "y": Number(item.Current),
        "tag": "Current"
      })


      dataAll.push({
        "x": Number(moment.utc(new Date(item.Time)).format("x")),
        "y": Number(item['Flow rate']),
        "tag": "FlowRate"
      })
      dataFlowRate.push({
        "x": Number(moment.utc(new Date(item.Time)).format("x")),
        "y": Number(item['Flow rate']),
        "tag": "FlowRate"
      })

      dataAll.push({
        "x": Number(moment.utc(new Date(item.Time)).format("x")),
        "y": Number(item['Frequency']),
        "tag": "Frequency"
      })
      dataFrequency.push({
        "x": Number(moment.utc(new Date(item.Time)).format("x")),
        "y": Number(item['Frequency']),
        "tag": "Frequency"
      })


    })

    const max = () => { 
      var temp = Math.max.apply(Math, dataAll.map(function(o) { 
          return o.y;  
      })); 
      return temp
    }   
    const min = () => { 
      var temp = Math.min.apply(Math, dataAll.map(function(o) { 
          return o.y;  
      })); 
      return temp
    } 
    var margin = {
      top: 10,
      right: 20,
      bottom: 100,
      left: 70
    }

    var padding_top = 30
    var default_width = this.width - margin.left - margin.right;
    var default_height = this.height - margin.top - margin.bottom;
    var default_ratio = default_width / default_height;

    const set_size = () => {
        var w, h
        var current_width = window.innerWidth;
        
        var current_height = window.innerHeight;

        var current_ratio = current_width / current_height;

        // desktop
        if (current_ratio > default_ratio) {
            h = default_height;
            w = default_width;

        // mobile
        } else {
            margin.left = 40
            w = current_width - 40;
            h = w / default_ratio;
        }
        // Set new width and height based on graph dimensions
        this.width = w - 50 - margin.right;
        this.height = h - margin.top - margin.bottom;
    }

    set_size();
    //end responsive graph

    // set the ranges
    this.x = d3.scaleTime().range([0, this.width]);
    this.y = d3.scaleLinear().range([this.height, 0])

    this.x.domain([d3.min(dataAll, function (d) {
        return d.x;
    }), d3.max(dataAll, function (d) {
        return d.x;
    })]);
    this.y.domain([d3.min(dataAll, function (d) {
        return d.y - 30;
    }), d3.max(dataAll, function (d) {
        return d.y + 30;
    })]);

    // define the line
    this.valueline = d3.line()
      .x(function (d) {
          return that.x(d.x);
      })
      .y(function (d) {
          return that.y(d.y);
      });
      
    this.svg = d3.select(this.myRef.current).append("svg")
      .attr("width", this.width + margin.left + margin.right)
      .attr("height", this.height + margin.bottom/* + margin.top + margin.bottom + padding_top*/)
      .attr("viewBox", "0 0 "+(this.width + margin.left + margin.right + 40)+" " + (this.height + margin.bottom  /*+ margin.top + margin.bottom + padding_top*/))
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + (margin.top + 40) + ")")    
      
    // X
    this.svg.append("g")
      .attr("transform", "translate(0," + this.height + ")")
      .call(d3.axisBottom(this.x)
      .ticks(6)
      .tickFormat((d) => {
          var format = d3.timeFormat("%Y-%m-%dT%H:%M:%S.%LZ")
          return format(d)
      }));
    // Y
    var yAis = this.svg.append("g")
        .call(d3.axisLeft(this.y).scale(
          d3.scaleLinear()
            .domain([min() - 30 , max() + 30])
            .range([this.height, 0])
        )
        .ticks(6)
        .tickFormat(function (d) {
            return d3.format("")(d)
        }));
    var LineDataPredictFTHT = this.svg.append("path")
      .data([dataPredictFTHT])
      .attr("class", "line")
      .attr("d", this.valueline)
      .attr("stroke", "#ff00ff")
      .attr("stroke-width", 2)
      .attr("fill", "#ff00ff00")
      .attr("visibility", "visible")
    var PointDataPredictFTHT = this.svg.selectAll("myCircles")
      .data(dataPredictFTHT)
      .enter()
      .append("circle")
        .attr("fill", "#ff00ff")
        .attr("stroke", "none")
        .attr("cx", function(d) { return that.x(d.x) })
        .attr("cy", function(d) { return that.y(d.y) })
        .attr("r", 3)
        .on("mouseover", (d, i) => {}) 

    var LineDataFTHT = this.svg.append("path")
      .data([dataFTHT])
      .attr("class", "line")
      .attr("d", this.valueline)
      .attr("stroke", "#aa00aa")
      .attr("stroke-width", 2)
      .attr("fill", "#aa00aa00")
      .attr("visibility", "visible")
    var PointDataFTHT = this.svg.selectAll("myCircles")
      .data(dataFTHT)
      .enter()
      .append("circle")
        .attr("fill", "#aa00aa")
        .attr("stroke", "none")
        .attr("cx", function(d) { return that.x(d.x) })
        .attr("cy", function(d) { return that.y(d.y) })
        .attr("r", 3)
        .on("mouseover", (d, i) => {}) 

    var LineDataPredictFTHP = this.svg.append("path")
        .data([dataPredictFTHP])
        .attr("class", "line")
        .attr("d", this.valueline)
        .attr("stroke", "#ff00ff")
        .attr("stroke-width", 2)
        .attr("fill", "#ff00ff00")
        .attr("visibility", "visible")
      var PointDataPredictFTHP = this.svg.selectAll("myCircles")
        .data(dataPredictFTHP)
        .enter()
        .append("circle")
          .attr("fill", "#ff00ff")
          .attr("stroke", "none")
          .attr("cx", function(d) { return that.x(d.x) })
          .attr("cy", function(d) { return that.y(d.y) })
          .attr("r", 3)
          .on("mouseover", (d, i) => {}) 

    var LineDataFTHP = this.svg.append("path")
      .data([dataFTHP])
      .attr("class", "line")
      .attr("d", this.valueline)
      .attr("stroke", "#29abe2ff")
      .attr("stroke-width", 2)
      .attr("fill", "#29abe200")
      .attr("visibility", "visible")
    var PointDataFTHP = this.svg.selectAll("myCircles")
      .data(dataFTHP)
      .enter()
      .append("circle")
        .attr("fill", "#29abe2ff")
        .attr("stroke", "none")
        .attr("cx", function(d) { return that.x(d.x) })
        .attr("cy", function(d) { return that.y(d.y) })
        .attr("r", 3)
        .on("mouseover", function(d, i) {
          console.log(d)
          console.log(i)
          var elmnt = document.getElementById("tooltip_dataFTHP");
          ToolTipFTHP.transition()
              .duration(50)
              .style("opacity", 1);
          var strout = "<b>FTHP</b><br>value: " + String(d.y) + "<br>" + "Time: " + moment(d.x, 'x').format('DD-MMMM-YYYY hh:mm:ss a')
          ToolTipFTHP.html(strout)
              .style("left", (d3.event.pageX - (elmnt.offsetWidth / 2)) + "px")
              .style("top", (d3.event.pageY - elmnt.offsetHeight - 10) + "px");
        })
        .on("mouseleave", (d, i) => {
          ToolTipFTHP.style("opacity", 0);
        })


      var LineDataCurrent = this.svg.append("path")
        .data([dataCurrent])
        .attr("class", "line")
        .attr("d", this.valueline)
        .attr("stroke", "#F28123")
        .attr("stroke-width", 2)
        .attr("fill", "#F2812300")
        .attr("visibility", "visible")
      var PointDataCurrent = this.svg.selectAll("myCircles")
        .data(dataCurrent)
        .enter()
        .append("circle")
          .attr("fill", "#F28123")
          .attr("stroke", "none")
          .attr("cx", function(d) { return that.x(d.x) })
          .attr("cy", function(d) { return that.y(d.y) })
          .attr("r", 3)
          .on("mouseover", (d, i) => {}) 

      var LineDataFlowRate = this.svg.append("path")
        .data([dataFlowRate])
        .attr("class", "line")
        .attr("d", this.valueline)
        .attr("stroke", "#A0C4E2")
        .attr("stroke-width", 2)
        .attr("fill", "#A0C4E200")
        .attr("visibility", "visible")
      var PointDataFlowRate = this.svg.selectAll("myCircles")
        .data(dataFlowRate)
        .enter()
        .append("circle")
          .attr("fill", "#A0C4E2")
          .attr("stroke", "none")
          .attr("cx", function(d) { return that.x(d.x) })
          .attr("cy", function(d) { return that.y(d.y) })
          .attr("r", 3)
          .on("mouseover", (d, i) => {}) 


      var LineDataFrequency = this.svg.append("path")
        .data([dataFrequency])
        .attr("class", "line")
        .attr("d", this.valueline)
        .attr("stroke", "#3276C6")
        .attr("stroke-width", 2)
        .attr("fill", "#3276C600")
        .attr("visibility", "visible")
      var PointDataFrequency = this.svg.selectAll("myCircles")
        .data(dataFrequency)
        .enter()
        .append("circle")
          .attr("fill", "#3276C6")
          .attr("stroke", "none")
          .attr("cx", function(d) { return that.x(d.x) })
          .attr("cy", function(d) { return that.y(d.y) })
          .attr("r", 3)
          .on("mouseover", (d, i) => {}) 
          
      // LineDataPredictionFrequency
      // PointDataPredictionFrequency

    // START -------------------------------------------------------------------
    // Function point line FTHP
    const PotLinePointFTHP = (data, predict) => {
      LineDataFTHP
        .datum(data)
        .attr("class", "line")
        .attr("d", that.valueline)
        .attr("stroke", "#29abe2ff")
        .attr("stroke-width", 2)
        .attr("fill", "#29abe200")
        .attr("visibility", "visible")
      LineDataPredictFTHP
        .datum(predict)
        .attr("class", "line")
        .attr("d", that.valueline)
        .attr("stroke", "#ff00ff")
        .attr("stroke-width", 2)
        .attr("fill", "#ff00ff00")
        .attr("visibility", "visible")
      PointDataFTHP.remove()
      PointDataPredictFTHP.remove()
      PointDataFTHP = this.svg.selectAll("myCircles")
        .data(data)
        .enter()
        .append("circle")
          .attr("fill", "#29abe2ff")
          .attr("stroke", "none")
          .attr("cx", function(d) { return that.x(d.x) })
          .attr("cy", function(d) { return that.y(d.y) })
          .attr("r", 3)
          .on("mouseover", function(d, i) {
            console.log(d)
            console.log(i)
            var elmnt = document.getElementById("tooltip_dataFTHP");
            ToolTipFTHP.transition()
                .duration(50)
                .style("opacity", 1);
            var strout = "<b>FTHP</b><br>value: " + String(d.y) + "<br>" + "Time: " + moment(d.x, 'x').format('DD-MMMM-YYYY hh:mm:ss a')
            ToolTipFTHP.html(strout)
                .style("left", (d3.event.pageX - (elmnt.offsetWidth / 2)) + "px")
                .style("top", (d3.event.pageY - elmnt.offsetHeight - 10) + "px");
          })
          .on("mouseleave", (d, i) => {
            ToolTipFTHP.style("opacity", 0);
          })
      PointDataPredictFTHP = this.svg.selectAll("myCircles")
        .data(predict)
        .enter()
        .append("circle")
          .attr("fill", "#ff00ff")
          .attr("stroke", "none")
          .attr("cx", function(d) { return that.x(d.x) })
          .attr("cy", function(d) { return that.y(d.y) })
          .attr("r", 3)
          .on("mouseover", (d, i) => {}) 
    }
    // Function point line FTHT
    const PotLinePointFTHT = (data, predict) => {
      LineDataFTHT
        .datum(data)
        .attr("class", "line")
        .attr("d", that.valueline)
        .attr("stroke", "#aa00aa")
        .attr("stroke-width", 2)
        .attr("fill", "#aa00aa00")
        .attr("visibility", "visible")
      LineDataPredictFTHT
        .datum(predict)
        .attr("class", "line")
        .attr("d", that.valueline)
        .attr("stroke", "#ff00ff")
        .attr("stroke-width", 2)
        .attr("fill", "#ff00ff00")
        .attr("visibility", "visible")
      PointDataFTHT.remove()
      PointDataPredictFTHT.remove()
      PointDataFTHT = this.svg.selectAll("myCircles")
        .data(data)
        .enter()
        .append("circle")
          .attr("fill", "#aa00aa")
          .attr("stroke", "none")
          .attr("cx", function(d) { return that.x(d.x) })
          .attr("cy", function(d) { return that.y(d.y) })
          .attr("r", 3)
          .on("mouseover", (d, i) => {}) 
      PointDataPredictFTHT = this.svg.selectAll("myCircles")
        .data(predict)
        .enter()
        .append("circle")
          .attr("fill", "#ff00ff")
          .attr("stroke", "none")
          .attr("cx", function(d) { return that.x(d.x) })
          .attr("cy", function(d) { return that.y(d.y) })
          .attr("r", 3)
          .on("mouseover", (d, i) => {}) 
    }
    // Function point line Current
    const PotLinePointCurrent = (data) => {
      LineDataCurrent
        .datum(data)
        .attr("class", "line")
        .attr("d", that.valueline)
        .attr("stroke", "#F28123")
        .attr("stroke-width", 2)
        .attr("fill", "#F2812300")
        .attr("visibility", "visible")
      PointDataCurrent.remove()
      PointDataCurrent = this.svg.selectAll("myCircles")
        .data(data)
        .enter()
        .append("circle")
          .attr("fill", "#F28123")
          .attr("stroke", "none")
          .attr("cx", function(d) { return that.x(d.x) })
          .attr("cy", function(d) { return that.y(d.y) })
          .attr("r", 3)
          .on("mouseover", (d, i) => {}) 
    }
    // Function point line Flow Rate
    const PotLinePointFlowRate = (data) => {
      LineDataFlowRate
        .datum(data)
        .attr("class", "line")
        .attr("d", that.valueline)
        .attr("stroke", "#A0C4E2")
        .attr("stroke-width", 2)
        .attr("fill", "#A0C4E200")
        .attr("visibility", "visible")
      PointDataFlowRate.remove()
      PointDataFlowRate = this.svg.selectAll("myCircles")
        .data(data)
        .enter()
        .append("circle")
          .attr("fill", "#A0C4E2")
          .attr("stroke", "none")
          .attr("cx", function(d) { return that.x(d.x) })
          .attr("cy", function(d) { return that.y(d.y) })
          .attr("r", 3)
          .on("mouseover", (d, i) => {}) 
    }


    // Function point line Frequency
    const PotLinePointFrequency = (data) => {
      LineDataFrequency
        .datum(data)
        .attr("class", "line")
        .attr("d", that.valueline)
        .attr("stroke", "#3276C6")
        .attr("stroke-width", 2)
        .attr("fill", "#3276C600")
        .attr("visibility", "visible")
      PointDataFrequency.remove()
      PointDataFrequency = this.svg.selectAll("myCircles")
        .data(data)
        .enter()
        .append("circle")
          .attr("fill", "#3276C6")
          .attr("stroke", "none")
          .attr("cx", function(d) { return that.x(d.x) })
          .attr("cy", function(d) { return that.y(d.y) })
          .attr("r", 3)
          .on("mouseover", (d, i) => {}) 
    }
    // END -------------------------------------------------------------------



    const HiddenLinePoint = (Point, Line) => {
      Point.attr("visibility", "hidden")
      Line.attr("visibility", "hidden")
    }
    // update aAixs Y
    const update_aAixs_Y = (data, padding_max, padding_min) => {
      that.y.domain([d3.min(data, function (d) {
          return d.y - padding_max;
        }), d3.max(data, function (d) {
          return d.y + padding_min;
        })]);
      yAis.transition().duration(100).call(d3.axisLeft(that.y))
    }


    const UpdateLineANDaAixs = () => {


      if (that.btnStatus['FTHP']){
        dataFTHP.forEach((item, index) => {
          dataAll.push({
            "x": item.x,
            "y": item.y,
            "tag": item.tag
          })
        })
        dataPredictFTHP.forEach((item, index) => {
          dataAll.push({
            "x": item.x,
            "y": item.y,
            "tag": item.tag
          })
        })
        update_aAixs_Y(dataAll, 30, 30)
        PotLinePointFTHP(dataFTHP, dataPredictFTHP)
        // update all inore youre self
        if (that.btnStatus['FTHT']){
          PotLinePointFTHT(dataFTHT, dataPredictFTHT)
        }
        if (that.btnStatus['FlowRate']){
          PotLinePointFlowRate(dataFlowRate)
        }
        if (that.btnStatus['Current']){
          PotLinePointCurrent(dataCurrent)
        }
        if (that.btnStatus['Frequency']){
          PotLinePointFrequency(dataFrequency)
        }
      }else {
        var newdataAll = []
        dataAll.forEach((item) => {
          if (item.tag !== 'FTHP'){
            newdataAll.push(item)
          }
        })
        dataAll = newdataAll
        HiddenLinePoint(LineDataFTHP, PointDataFTHP)
        HiddenLinePoint(LineDataPredictFTHP, PointDataPredictFTHP)
        update_aAixs_Y(dataAll, 30, 30)
        // update all inore youre self
        if (that.btnStatus['FTHT']){
          PotLinePointFTHT(dataFTHT, dataPredictFTHT)
        }
        if (that.btnStatus['FlowRate']){
          PotLinePointFlowRate(dataFlowRate)
        }
        if (that.btnStatus['Current']){
          PotLinePointCurrent(dataCurrent)
        }
        if (that.btnStatus['Frequency']){
          PotLinePointFrequency(dataFrequency)
        }
      }



      if (that.btnStatus['FTHT']){
        dataFTHT.forEach((item, index) => {
          dataAll.push({
            "x": item.x,
            "y": item.y,
            "tag": item.tag
          })
        })
        dataPredictFTHT.forEach((item, index) => {
          dataAll.push({
            "x": item.x,
            "y": item.y,
            "tag": item.tag
          })
        })
        update_aAixs_Y(dataAll, 30, 30)
        PotLinePointFTHT(dataFTHT, dataPredictFTHT)
        // update all inore youre self
        if (that.btnStatus['FTHP']){
          PotLinePointFTHP(dataFTHP, dataPredictFTHP)
        }
        if (that.btnStatus['FlowRate']){
          PotLinePointFlowRate(dataFlowRate)
        }
        if (that.btnStatus['Current']){
          PotLinePointCurrent(dataCurrent)
        }
        if (that.btnStatus['Frequency']){
          PotLinePointFrequency(dataFrequency)
        }
      }else {
        var newdataAll = []
        dataAll.forEach((item) => {
          if (item.tag !== 'FTHT'){
            newdataAll.push(item)
          }
        })
        dataAll = newdataAll
        HiddenLinePoint(LineDataFTHT, PointDataFTHT)
        HiddenLinePoint(LineDataPredictFTHT, PointDataPredictFTHT)
        update_aAixs_Y(dataAll, 30, 30)
        // update all inore youre self
        if (that.btnStatus['FTHP']){
          PotLinePointFTHP(dataFTHP, dataPredictFTHP)
        }
        if (that.btnStatus['FlowRate']){
          PotLinePointFlowRate(dataFlowRate)
        }
        if (that.btnStatus['Current']){
          PotLinePointCurrent(dataCurrent)
        }
        if (that.btnStatus['Frequency']){
          PotLinePointFrequency(dataFrequency)
        }
      }



      if (that.btnStatus['Current']){
        dataCurrent.forEach((item, index) => {
          dataAll.push({
            "x": item.x,
            "y": item.y,
            "tag": item.tag
          })
        })
        update_aAixs_Y(dataAll, 30, 30)
        PotLinePointCurrent(dataCurrent)
        // update all inore youre self
        if (that.btnStatus['FTHP']){
          PotLinePointFTHP(dataFTHP, dataPredictFTHP)
        }
        if (that.btnStatus['FTHT']){
          PotLinePointFTHT(dataFTHT, dataPredictFTHT)
        }
        if (that.btnStatus['FlowRate']){
          PotLinePointFlowRate(dataFlowRate)
        }
        if (that.btnStatus['Frequency']){
          PotLinePointFrequency(dataFrequency)
        }
      }else {
        var newdataAll = []
        dataAll.forEach((item) => {
          if (item.tag !== 'Current'){
            newdataAll.push(item)
          }
        })
        dataAll = newdataAll
        HiddenLinePoint(LineDataCurrent, PointDataCurrent)
        update_aAixs_Y(dataAll, 30, 30)
        // update all inore youre self
        if (that.btnStatus['FTHP']){
          PotLinePointFTHP(dataFTHP, dataPredictFTHP)
        }
        if (that.btnStatus['FTHT']){
          PotLinePointFTHT(dataFTHT, dataPredictFTHT)
        }
        if (that.btnStatus['FlowRate']){
          PotLinePointFlowRate(dataFlowRate)
        }
        if (that.btnStatus['Frequency']){
          PotLinePointFrequency(dataFrequency)
        }
      }


      if (that.btnStatus['FlowRate']){
        dataFlowRate.forEach((item, index) => {
          dataAll.push({
            "x": item.x,
            "y": item.y,
            "tag": item.tag
          })
        })
        update_aAixs_Y(dataAll, 30, 30)
        PotLinePointFlowRate(dataFlowRate)
        // update all inore youre self
        if (that.btnStatus['FTHP']){
          PotLinePointFTHP(dataFTHP, dataPredictFTHP)
        }
        if (that.btnStatus['FTHT']){
          PotLinePointFTHT(dataFTHT, dataPredictFTHT)
        }
        if (that.btnStatus['Current']){
          PotLinePointCurrent(dataCurrent)
        }
        if (that.btnStatus['Frequency']){
          PotLinePointFrequency(dataFrequency)
        }
      }else {
        var newdataAll = []
        dataAll.forEach((item) => {
          if (item.tag !== 'FlowRate'){
            newdataAll.push(item)
          }
        })
        dataAll = newdataAll
        HiddenLinePoint(LineDataFlowRate, PointDataFlowRate)
        update_aAixs_Y(dataAll, 30, 30)
        // update all inore youre self
        if (that.btnStatus['FTHP']){
          PotLinePointFTHP(dataFTHP, dataPredictFTHP)
        }
        if (that.btnStatus['FTHT']){
          PotLinePointFTHT(dataFTHT, dataPredictFTHT)
        }
        if (that.btnStatus['Current']){
          PotLinePointCurrent(dataCurrent)
        }
        if (that.btnStatus['Frequency']){
          PotLinePointFrequency(dataFrequency)
        }
      }

      if (that.btnStatus['Frequency']){
        dataFrequency.forEach((item, index) => {
          dataAll.push({
            "x": item.x,
            "y": item.y,
            "tag": item.tag
          })
        })
        update_aAixs_Y(dataAll, 30, 30)
        PotLinePointFrequency(dataFrequency)
        // update all inore youre self
        if (that.btnStatus['FTHP']){
          PotLinePointFTHP(dataFTHP, dataPredictFTHP)
        }
        if (that.btnStatus['FTHT']){
          PotLinePointFTHT(dataFTHT, dataPredictFTHT)
        }
        if (that.btnStatus['Current']){
          PotLinePointCurrent(dataCurrent)
        }
        if (that.btnStatus['FlowRate']){
          PotLinePointFlowRate(dataFlowRate)
        }
      }else {
        var newdataAll = []
        dataAll.forEach((item) => {
          if (item.tag !== 'Frequency'){
            newdataAll.push(item)
          }
        })
        dataAll = newdataAll
        HiddenLinePoint(LineDataFrequency, PointDataFrequency)
        update_aAixs_Y(dataAll, 30, 30)
        // update all inore youre self
        if (that.btnStatus['FTHP']){
          PotLinePointFTHP(dataFTHP, dataPredictFTHP)
        }
        if (that.btnStatus['FTHT']){
          PotLinePointFTHT(dataFTHT, dataPredictFTHT)
        }
        if (that.btnStatus['Current']){
          PotLinePointCurrent(dataCurrent)
        }
        if (that.btnStatus['FlowRate']){
          PotLinePointFlowRate(dataFlowRate)
        }
      }


    }

    d3.select("#container_btn").append('button')
      .attr('class', 'btn-button FTHP')
      .text("FTHP")
      .on('click', function(d,i){
        that.btnStatus['FTHP'] = !that.btnStatus['FTHP']
        UpdateLineANDaAixs();
        if (!that.btnStatus['FTHP']){
          d3.select(this).attr('class', 'btn-button btn-button-inactive')
        } else {
          d3.select(this).attr('class', 'btn-button FTHP')
        }
      })

      d3.select("#container_btn").append('button')
      .attr('class', 'btn-button FTHT')
      .text("FTHT")
      .on('click', function(d,i) {
        that.btnStatus['FTHT'] = !that.btnStatus['FTHT']
        UpdateLineANDaAixs();
        if (!that.btnStatus['FTHT']){
          d3.select(this).attr('class', 'btn-button btn-button-inactive')
        } else {
          d3.select(this).attr('class', 'btn-button FTHT')
        }
      })


      d3.select("#container_btn").append('button')
      .attr('class', 'btn-button Current')
      .text("Current")
      .on('click', function(d,i) {
        that.btnStatus['Current'] = !that.btnStatus['Current']
        UpdateLineANDaAixs();
        if (!that.btnStatus['Current']){
          d3.select(this).attr('class', 'btn-button btn-button-inactive')
        } else {
          d3.select(this).attr('class', 'btn-button Current')
        }
      })


      d3.select("#container_btn").append('button')
      .attr('class', 'btn-button FlowRate')
      .text("FlowRate")
      .on('click', function(d,i) {
        that.btnStatus['FlowRate'] = !that.btnStatus['FlowRate']
        UpdateLineANDaAixs();
        if (!that.btnStatus['FlowRate']){
          d3.select(this).attr('class', 'btn-button btn-button-inactive')
        } else {
          d3.select(this).attr('class', 'btn-button FlowRate')
        }
      })


      d3.select("#container_btn").append('button')
      .attr('class', 'btn-button Frequency')
      .text("Frequency")
      .on('click', function(d,i) {
        that.btnStatus['Frequency'] = !that.btnStatus['Frequency']
        UpdateLineANDaAixs();
        if (!that.btnStatus['Frequency']){
          d3.select(this).attr('class', 'btn-button btn-button-inactive')
        } else {
          d3.select(this).attr('class', 'btn-button Frequency')
        }
      })



  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <div 
          className={classes.spacePaddingSvg}
          ref={this.myRef}
        >
        </div>
        <div id='container_btn' className='container-btn'></div>
      </div>
    )
  }
}

export default withStyles(styles)(DynaChart)