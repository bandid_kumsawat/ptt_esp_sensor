//"d3": "^5.16.0",
import React, { Component } from 'react';
import * as d3 from 'd3'; // 5.16.0
import withStyles from '@material-ui/styles/withStyles';
import './ESPLineChart.css'
import moment from 'moment'
// import data from './chartData.json'
import dataJson from './data.json'
import predictMock from './predict.json'

import { 
  esplinecolor,
  esptickcolor,
  espchartsetvalue,
  espshowtime, 
  espsetlinecolor
} from './../../../actions';
import { style } from 'd3';

const styles = theme => ({
  root: {
    color: "#000000",
    fontSize: '1em',
    border: "2px solid #8c8b8b44",
    borderTop: "4px solid #8c8b8b44",
    borderBottom: "10px solid #8c8b8b44",
    borderRight: "4px solid #8c8b8b44",
    borderLeft: "4px solid #8c8b8b44",
    borderRadius: "3px"
  },
  rootNoBorder: {
    color: "#000000",
    fontSize: '1em',
  },
  spacePaddingSvg: {
    paddingTop: '10px'
  }
});

class DynaChart extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.myRef = React.createRef();
    this.state = {

    }
    this.width = this.props.width
    this.height = this.props.height
    this.btnStatus = {
      "FTHP": true,
      "FTHT": true,
      "Current": true,
      "FlowRate": true,
      "Frequency": true,
      "MotorTemp": true,
      "SuctionPressure": true,
      "SuctionTemp": true,
      "Voltage": true,
    }
  }
  componentDidUpdate() {
  }
  componentDidMount(){
    this.LineChartFunction();
  }
  LineChartFunction(){
    var that = this
    // var dataJson = this.props.data
    var countLastValue = 0
    var lastvalue = []
    var lastdateC = String(moment(dataJson.data[0].Time, 'ddd, DD MMM YYYY HH:mm:ss').format('DDMMYYYYHHmmss'))
    var lastdateP = ''
    var data = dataJson.data.map((item) => {
      lastvalue['Current'] = String(item.Current)
      
      if (this.store.getState().SelectFTHP){
        lastvalue['FTHP'] = String(item.FTHP)
      }
      if (this.store.getState().SelectFTHT){
        lastvalue['FTHT'] = String(item.FTHT)
      }
      if (this.store.getState().SelectFlowRate){
        lastvalue['FlowRate'] =  String(item['Flow rate'])
      }

      if (this.store.getState().SelectCurrent){
        lastvalue['Current'] =  String(item['Current'])
      }

      if (this.store.getState().SelectFrequency){
        lastvalue['Frequency'] =  String(item['Frequency'])
      }
      
      if (this.store.getState().SelectSuctionPressure){
        lastvalue['SuctionPressure'] =  String(item['Suction pressure'])
      }

      if (this.store.getState().SelectSuctionTemp){
        lastvalue['SuctionTemp'] =  String(item['Suction temp'])
      }
      
      if (this.store.getState().SelectVoltage){
        lastvalue['Voltage'] =  String(item['Voltage'])
      }

      if (this.store.getState().SelectMotorTemp){
        lastvalue['MotorTemp'] =  String(item['Motor temp'])
      }

      lastvalue['date'] = String(item.Time)

      var temp =  {
        Current: String(item.Current),
        
        FTHP: ((this.store.getState().SelectFTHP) ? String(item.FTHP): 'null'),
        FTHT: ((this.store.getState().SelectFTHT) ? String(item.FTHT): 'null'),
        FlowRate: ((this.store.getState().SelectFlowRate) ? String(item['Flow rate']): 'null'),
        Current: ((this.store.getState().SelectCurrent) ? String(item['Current']): 'null'),
        Frequency: ((this.store.getState().SelectFrequency) ? String(item['Frequency']): 'null'),
        MotorTemp: ((this.store.getState().SelectMotorTemp) ? String(item['Motor temp']): 'null'),
        SuctionPressure: ((this.store.getState().SelectSuctionPressure) ? String(item['Suction pressure']): 'null'),
        SuctionTemp: ((this.store.getState().SelectSuctionTemp) ? String(item['Suction temp']): 'null'),
        
        Voltage: ((this.store.getState().SelectVoltage) ? String(item['Voltage']): 'null'),
    
        PDCurrent: "null",
        PDFTHP: "null",
        PDFTHT: "null",
        PDFlowRate: "null",
        PDFrequency: "null",
        PDMotorTemp: "null",
        PDSuctionPressure: "null",
        PDSuctionTemp: "null",
        PDVoltage: "null",
        
        date: String(moment(item.Time, 'ddd, DD MMM YYYY HH:mm:ss').format('DDMMYYYYHHmmss')), // 01/28/1986 11:39:13
      }
      countLastValue++
      return temp
    })
    data = data.concat(predictMock.data.map((item) => {
      lastdateP = String(moment(item.Time, 'ddd, DD MMM YYYY HH:mm:ss').format('DDMMYYYYHHmmss'))
      return {
        Current: "null",
        FTHP: "null",
        FTHT: "null",
        FlowRate: "null",
        Frequency: "null",
        MotorTemp: "null",
        SuctionPressure: "null",
        SuctionTemp: "null",
        Voltage: "null",

        PDCurrent: ((this.store.getState().SelectCurrent) ? String(item['Current']): 'null'),
        PDFTHP: ((this.store.getState().SelectFTHP) ? String(item.FTHP): 'null'),
        PDFTHT: ((this.store.getState().SelectFTHT) ? String(item.FTHT): 'null'),
        PDFlowRate: ((this.store.getState().SelectFlowRate) ? String(item['Flow rate']): 'null'),
        PDFrequency: ((this.store.getState().SelectFrequency) ? String(item['Frequency']): 'null'),
        PDMotorTemp: ((this.store.getState().SelectMotorTemp) ? String(item['Motor temp']): 'null'),
        PDSuctionPressure: ((this.store.getState().SelectSuctionPressure) ? String(item['Suction pressure']): 'null'),
        PDSuctionTemp: ((this.store.getState().SelectSuctionTemp) ? String(item['Suction temp']): 'null'),

        PDVoltage: ((this.store.getState().SelectVoltage) ? String(item['Voltage']): 'null'),

        date: String(moment(item.Time, 'ddd, DD MMM YYYY HH:mm:ss').format('DDMMYYYYHHmmss')), // 01/28/1986 11:39:13
      }
    }))
    // get attr name
    var dataAttr = []
    for (var name in this.btnStatus){
      dataAttr.push(name)
    }

    // console.log(lastvalue)
    // console.log(dataAttr)
    // console.log(data)

    var margin = {top: 10, right: 10, bottom: 100, left: 40},
        margin2 = {top: 430, right: 10, bottom: 20, left: 40},
        width = this.width - margin.left - margin.right,
        height = this.height - margin.top - margin.bottom,
        height2 = this.height - margin2.top - margin2.bottom;
    var color = d3.scale.category10();
    // 10 04 2015 00 00 00
    var parseDate = d3.time.format("%d%m%Y%H%M%S").parse;
    
    var x = d3.time.scale().range([0, width]),
        x2 = d3.time.scale().range([0, width]),
        y = d3.scale.linear().range([height, 0]),
        y2 = d3.scale.linear().range([height2, 0]);
        // --> กำหนดความกว้างของ x และ y

    var xAxis = d3.svg.axis().scale(x).orient("bottom"),
        xAxis2 = d3.svg.axis().scale(x2).orient("bottom"),
        yAxis = d3.svg.axis().scale(y).orient("left");
        // --> สร้างตำแหน่งกราฟแกน x และ y 
    var brushparse = d3.time.format("%d%m%Y%H%M%S").parse
    // console.log(brushparse(lastdateC))
    // console.log(brushparse(lastdateP))
    var brush = d3.svg.brush()
        .x(x2)
        .on("brush", brush)
        .on('brushstart', function() {
            // this.brushStart();
        })
        .on('brushend', function() {
            // this.brushEnd();
        })
    // --> function สำหรับซูมดูข้อมูล

    var line = d3.svg.line()
        .defined(function(d) { return !isNaN(d.ESPvalues); })
        .interpolate("cubic")
        .x(function(d) { return x(d.date); })
        .y(function(d) { return y(d.ESPvalues); });

    var line2 = d3.svg.line()
        .defined(function(d) { return !isNaN(d.ESPvalues); })
        .interpolate("cubic")
        .x(function(d) {return x2(d.date); })
        .y(function(d) {return y2(d.ESPvalues); });
    
    var svg = d3.select(this.myRef.current).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom);

    svg.append("defs").append("clipPath")
        .attr("id", "clip")
        .append("rect")
        .attr("width", width)
        .attr("height", height)

    var focus = svg.append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
          
    var context = svg.append("g")
      .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");
    
    // start

    // d3.csv("climate4.csv", function(error, data) {
    
    color.domain(d3.keys(data[0]).filter(function(key) { return key !== "date"; }));
    data.forEach(function(d) {
      d.date = parseDate(d.date);
    });

    var sources = color.domain().map(function(name) {
      return {
        name: name,
        values: data.map(function(d) {
          if (d[name] !== 'null'){
            return {date: d.date, ESPvalues: +d[name]};
          }else{
            return {};
          }
        })
      };
    });

    x.domain(d3.extent(data, function(d) { return d.date; }));
    y.domain([
      d3.min(sources, function(c) { return d3.min(c.values, function(v) { return v.ESPvalues; }); }),
      d3.max(sources, function(c) { return d3.max(c.values, function(v) { return v.ESPvalues; }); }) 
    ]);
    x2.domain(x.domain());
    y2.domain(y.domain());
    
    var focuslineGroups = focus.selectAll("g")
      .data(sources)
      .enter().append("g");
    
    var newColor = {}

    if (this.store.getState().ESPSetLineColor[0] === undefined){
      d3.keys(data[0]).filter(function(key) { return key !== "date"; }).forEach((item) => {
        if (item.indexOf('PD') === -1){
          newColor[item] = "#"+ Math.floor(Math.random()*16777215).toString(16);
        }else{
          newColor[item] = newColor[item.split('PD')[1]]+"99"
        }
      })
      this.store.dispatch(espsetlinecolor(newColor))
    }else {
      newColor = this.store.getState().ESPSetLineColor[0]
    }
    // console.log(newColor)
    // set color Select Line
    this.store.dispatch(esptickcolor(false))
    var colorT = {}
    dataAttr.forEach((name) => {
      colorT[name] = newColor[name]
      colorT["PD"+name] = newColor["PD"+name]
    })
    this.store.dispatch(esplinecolor([colorT]))
    setTimeout(() => { this.store.dispatch(esptickcolor(true)) }, 1);

    focuslineGroups.append("path")
      .attr("class","line")
      .attr("id", function(d) { return d.name })
      .attr("d", function(d) { return line(d.values); })
      .style("stroke", function(d) {return newColor[d.name];})
      .attr("clip-path", "url(#clip)")

      console.log("sources start")
      console.log(sources)
      console.log("sources end")

    // var indexies = d3.range( sources.length );
  
    
    // focuslineGroups
    //   .append('clipPath')
    //   .attr('id', 'clip-traffic')
    //   .append('path')
    //   .datum(indexies)
    //   // .data(sources)
    //   .attr('d', d3.svg.area()
    //     .interpolate("cardinal")
    //     .x(function(d) { return sources[d].values.map((item) => {
    //       return new Date(item.date).getTime()
    //     }); }) 
    //     .y0(height)
    //     .y1(function(d) { return sources[d].values.map((item)=> {
    //       return item.ESPvalues
    //     }); })
    //   );


    

    d3.select('#PDSuctionPressure').append('rect')
      .attr("width", width)
      .attr("height", height)
      .attr('fill','#ff00ff')
  

    focus.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);
    // --> วาดเส้นแกน x1 กำหนดมีคลาสชื่อ x axis

    focus.append("g")
        .attr("class", "y axis")
        .call(yAxis);
    // --> วาดเส้นแกน y กำหนดมีคลาสชื่อ y axis
        
    var contextlineGroups = context.selectAll("g")
        .data(sources)
        .enter().append("g");
    
    contextlineGroups.append("path")
        .attr("class", "line")
        .attr("d", function(d) { return line2(d.values); })
        .style("stroke", function(d) {return newColor[d.name];})
        .attr("clip-path", "url(#clip)")
        // .attr("d", d3.area()
        //   .x(function(d, i) { return x(d.data.year); })
        //   .y0(function(d) { return y(d[0]); })
        //   .y1(function(d) { return y(d[1]); })
        // )

    context.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height2 + ")")
        .call(xAxis2);
    // --> --> วาดเส้นแกน x2 กำหนดมีคลาสชื่อ x axis

    // init rect brush
    brush.extent([brushparse(lastdateC), brushparse(lastdateP)]) 
    this.store.dispatch(espshowtime([brushparse(lastdateC), brushparse(lastdateP)]))

    context.append("g")
        .attr("class", "brush")
        .call(brush)
      .selectAll("rect")
        .attr("y", -6)
        .attr("height", height2 + 7)

    context.selectAll(".resize.w").append("path")
        .attr('d','M -4.5 0.5 L 3.5 0.5 L 3.5 15.5 L -4.5 15.5 L -4.5 0.5 M -1.5 4 L -1.5 12 M 0.5 4 L 0.5 12')
        .attr('stroke', '#000')
        .attr('fill', '#f2f2f2')
        .attr("x", 1)
        .attr("y", (height2 + 6) / 8)
        .attr("width", 10)
        .attr("height", height2)

        
    context.selectAll(".resize.e").append("path")
        .attr('d','M -4.5 0.5 L 3.5 0.5 L 3.5 15.5 L -4.5 15.5 L -4.5 0.5 M -1.5 4 L -1.5 12 M 0.5 4 L 0.5 12')
        .attr('stroke', '#000')
        .attr('fill', '#f2f2f2')
        .attr("x", -5)
        .attr("y", (height2 + 6) / 8)
        .attr("width", 10)
        .attr("height", height2)
    // --> สร้างกรอบสี่เหลี่ยนที่ x2 และกำหนด fn brush

    // draw circle
    // var focusCircle = focus.append('g')
    //     .selectAll('g')
    //     .data(sources)
    //     .enter().append('g')
    //       .append('circle')
    //       .attr('opacity', 0)
    //       .attr('r', 6)
    //       .attr('fill', function(d) { console.log(d); return color(d.name); })
    // --> สร้าง จุด เพื่อแสดงจุดเมื่อเกิด hover     
    
    var focusYLine = focus.append('g')
        .selectAll('g')
        .data(sources)
        .enter().append('g')
          .append('line')
          .attr('class', 'focus-line')
          .style('stroke', '#99AAB5ff')
          .attr('opacity', 0);

    var bisectDate = d3.bisector(function(d) { return d.date; }).left;     

    var ToolTipCurrent = d3.select("body")
      .append("div")
      .attr("class", "tooltip-current")
      .attr("id", "tooltip_current")
      .style("display", "none");

    var ToolTipPredict = d3.select("body").append("div")
      .attr("class", "tooltip-predict")
      .attr("id", "tooltip_predict")
      .style("display", "none")
      .style("left", "0px")
      .style("top", "0px");

    focus.append('rect')
      .attr('width', width)
      .attr('height', height)
      .attr('class', 'pane')
      .on('mouseover', function() {
        // focusCircle.attr('opacity', 1);
        focusYLine.attr('opacity', 1);
        ToolTipCurrent.style("opacity", 1)
          .style("display", "block")
        ToolTipPredict.style("opacity", 1)
          .style("display", "block")
      })
      .on('mouseout', function() {
        // focusCircle.attr('opacity', 0);
        focusYLine.attr('opacity', 0);
        ToolTipCurrent.style("display", "none")
          .style("left", "0px")
          .style("top", "0px")
        ToolTipPredict.style("display", "none")
          .style("left", "0px")
          .style("top", "0px")
      })
      .on('mousemove', mouseMove)

    function mouseMove() {
      var x0 = d3.time.minute.round(x.invert(d3.mouse(this)[0])), i = bisectDate(data, x0, 1);
      that.store.dispatch(espchartsetvalue(data[i]))
      var unselect = false
      dataAttr.forEach((name, index) => {
        if (that.store.getState()['Select' + name]){
          unselect = true
        }
      })
      if (unselect){
        if (
          // data[i]['PDCurrent'] !== 'null' 
          countLastValue < i
        ){
          ToolTipPredict.style("display", 'block');
          ToolTipPredict.html(function (d) {
            var   strout = '<table>'
                    strout += "<tr>"
                      strout += "<th class='title-table-tooltip'>"
                        strout += "Prediction"
                      strout += "</th>"
                      strout += "<th class='title-table-tooltip' style='width: 100px'>"
                        strout += "" + ((countLastValue < i) ? "" : "") 
                      strout += "</th>"
                    strout += "</tr>"
                    strout += "<tr>"
                      strout += moment(data[i].date).format("LLLL")
                    strout += "</tr>"
                      dataAttr.forEach((name, index) => {
                        if (that.store.getState()['Select' + name]){
                          strout += "<tr>"
                            strout += "<td class='row-table-tooltip' style='color: "+newColor['PD' + name]+"'>"
                              strout += 'PD' + name
                            strout += "</td>"
                            strout += "<td style='width: 50px'>"
                            strout += "</td>"
                            strout += "<td class='row-table-tooltip-value' style='color: "+newColor['PD' + name]+"'>"
                              strout += (data[i]['PD' + name] === 'null' ? lastvalue['PD' + name] : data[i]['PD' + name])
                            strout += "</td>"
                          strout += "</tr>"
                        }
                      })
                  strout += "</table>"
            return strout
          })
          .style("left", ((d3.event.pageX + 10) + "px"))
          .style("top", ((d3.event.pageY - 10) + "px"))
          ToolTipCurrent.html(function(d) { 
            var   strout = '<table>'
                    strout += "<tr>"
                      strout += "<th class='title-table-tooltip'>"
                        strout += "Current"
                      strout += "</th>"
                      strout += "<th style='width: 50px'></th>"
                      strout += "<th class='title-table-tooltip' style='width: 200px;text-align: left;'>"
                        strout += "" + ((data[i]['Current'] === 'null') ? "last value" : "") 
                      strout += "</th>"
                    strout += "</tr>"
                    strout += "<tr>"
                      strout += moment(lastvalue.date).add(-7, 'hours').format("LLLL")
                    strout += "</tr>"
                      dataAttr.forEach((name, index) => {
                        if (that.store.getState()['Select' + name]){
                          strout += "<tr>"
                            strout += "<td class='row-table-tooltip' style='color: "+newColor[name]+"'>"
                              strout += name
                            strout += "</td>"
                            strout += "<td style='width: 50px'>"
                            strout += "</td>"
                            strout += "<td class='row-table-tooltip-value' style='color: "+newColor[name]+"'>"
                              strout += (data[i][name] === 'null' ? lastvalue[name] : data[i][name])
                            strout += "</td>"
                          strout += "</tr>"
                        }
                      })
                  strout += "</table>"
            return strout
          })
          .style("left", ((d3.event.pageX - 210) + "px"))
          .style("top", ((d3.event.pageY - 10) + "px"))
        } else {
          ToolTipPredict.style("display", 'none');
          ToolTipCurrent.html(function(d) { 
            var   strout = '<table>'
                    strout += "<tr>"
                      strout += "<th class='title-table-tooltip'>"
                        strout += "Current"
                      strout += "</th>"
                      strout += "<th class='title-table-tooltip' style='width: 100%'>"
                        strout += "" + ((countLastValue < i) ? "last value" : "") 
                      strout += "</th>"
                    strout += "</tr>"
                    strout += "<tr>"
                      strout += moment(data[i].date).format("LLLL")
                    strout += "</tr>"
                      dataAttr.forEach((name, index) => {
                        if (that.store.getState()['Select' + name]){
                          strout += "<tr>"
                            strout += "<td class='row-table-tooltip' style='color: "+newColor[name]+"'>"
                              strout += name
                            strout += "</td>"
                            strout += "<td style='width: 50px'>"
                            strout += "</td>"
                            strout += "<td class='row-table-tooltip-value' style='color: "+newColor[name]+"'>"
                              strout += (data[i][name] === 'null' ? lastvalue[name] : data[i][name])
                            strout += "</td>"
                          strout += "</tr>"
                        }
                      })
                  strout += "</table>"
            return strout
          })
          .style("left", ((d3.event.pageX - 210) + "px"))
          .style("top", ((d3.event.pageY - 10) + "px"))
        }
  
        focusYLine.transition()
          .duration(0)
          .delay(0)
          .ease('bounce')
          .attr('x1', x(x0))
          .attr('x2', x(x0))
          .attr('y1', height)
          .attr('y2', function(d) { 
            if (!isNaN(d.values[i].ESPvalues)){
              return y(d.values[i].ESPvalues); 
            }
          });
      } else {
        ToolTipCurrent.style("display", 'none');
        ToolTipPredict.style("display", 'none');
      }
    }        

    function brush() {
      console.log(brush.extent())
      that.store.dispatch(espshowtime(brush.extent()))
      x.domain(brush.empty() ? x2.domain() : brush.extent());
      focus.selectAll("path.line").attr("d",  function(d) { return line(d.values)});
      focus.select(".x.axis").call(xAxis);
      focus.select(".y.axis").call(yAxis);
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <div 
          className={classes.spacePaddingSvg}
          ref={this.myRef}
        >
        </div>
        <div id='container_btn' className='container-btn'></div>
      </div>
    )
  }
}

export default withStyles(styles)(DynaChart)