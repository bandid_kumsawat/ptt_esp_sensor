import React, { Component } from 'react';
import * as d3 from 'd3';
import moment from 'moment'
import withStyles from '@material-ui/styles/withStyles';
import './DynaChart.css'
import { addwellselect } from './../../../actions';

const styles = theme => ({
  root: {
    color: "#000000",
    fontSize: '1em',
    border: "2px solid #8c8b8b44",
    borderTop: "4px solid #8c8b8b44",
    borderBottom: "10px solid #8c8b8b44",
    borderRight: "4px solid #8c8b8b44",
    borderLeft: "4px solid #8c8b8b44",
    borderRadius: "3px"
  },
  rootNoBorder: {
    color: "#000000",
    fontSize: '1em',
  }
});

class DynaChart extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.myRef = React.createRef();
    this.state = {
      Loads: this.props.Loads,
      Positions: this.props.Positions,
      width: (this.props.width === undefined ? 600 : this.props.width),
      height: (this.props.height === undefined ? 400 : this.props.height),
      title: (this.props.subTitle === undefined ? "" : this.props.subTitle),
      subTitle: (this.props.title === undefined ? '' : this.props.title),
      PropsClassName: this.props.className
    }
    this.width = this.props.width
    this.height = this.props.height
    this.x = Object
    this.y = Object
    this.data = Array
    this.svg = Object
    this.valueline = Object
    this.classselected = ''
  }
  // componentWillReceiveProps () {
      
  // }
  componentDidUpdate() {
    // this.LineChartFunction();
  }
  componentDidMount(){
    this.LineChartFunction();
    // this.classselected = this.check_select()
  }
  LineChartFunction(){
    var that = this

    var data = []

    this.state.Loads.forEach((item, index, arr) => {
      var tttt = this.state.Positions[index]
      data.push({
          "x": Number(tttt),
          "y": Number(item),
      })
    })
    
    var  max = () => { 
      var temp = Math.max.apply(Math, data.map(function(o) { 
          return o.y;  
      })); 
      return temp
    }   
    var  min = () => { 
      var temp = Math.min.apply(Math, data.map(function(o) { 
          return o.y;  
      })); 
      return temp
    } 

    var margin = {
      top: 10,
      right: 20,
      bottom: 50,
      left: 70
    }

    var padding_top = 30
    
    var default_width = this.width - margin.left - margin.right;
    
    var default_height = this.height - margin.top - margin.bottom;
    
    var default_ratio = default_width / default_height;

    const set_size = () => {
        var w, h
        var current_width = window.innerWidth;
        
        var current_height = window.innerHeight;

        var current_ratio = current_width / current_height;

        // desktop
        if (current_ratio > default_ratio) {
            h = default_height;
            w = default_width;

        // mobile
        } else {
            margin.left = 40
            w = current_width - 40;
            h = w / default_ratio;
        }
        // Set new width and height based on graph dimensions
        this.width = w - 50 - margin.right;
        this.height = h - margin.top - margin.bottom;
    }

    set_size();
    //end responsive graph cod

    // set the ranges
    this.x = d3.scaleTime().range([0, this.width]);
    this.y = d3.scaleLinear().range([this.height, 0])

    this.x.domain([d3.min(data, function (d) {
        return d.x;
    }), d3.max(data, function (d) {
        return d.x;
    })]);
    this.y.domain([d3.min(data, function (d) {
        return d.y;
    }), d3.max(data, function (d) {
        return d.y;
    })]);

    // define the line
    this.valueline = d3.line()
        .curve(d3.curveCatmullRom.alpha(1))
        .x(function (d) {
            return that.x(d.x);
        })
        .y(function (d) {
            return that.y(d.y);
        });

      // append the svg object to the body of the page
      
      // var svg = d3.select(this.myRef.current).remove()
      this.svg = d3.select(this.myRef.current).append("svg")
          // .attr("style", "padding-top: "+padding_top+";")
          .attr("width", this.width + margin.left + margin.right)
          .attr("height", this.height + margin.top + margin.bottom + padding_top)
          .attr("viewBox", "0 0 "+(this.width + margin.left + margin.right + 40)+" " + (this.height + margin.top + margin.bottom + padding_top))
          .append("g")
          .attr("transform", "translate(" + margin.left + "," + (margin.top + 40) + ")")
        
      // title line chart
      var subtitle = moment(this.state.title).format('DD-MMMM-YYYY HH:MM:SS');
      this.svg.append("text")
          .attr('y', -40)
          .attr("class", "title-line-chart")
          .attr('x', (this.width / 2) + 0)
          .attr('fill', "#000000")
          .attr("style", "font-size: 1.5em")
          .attr("text-anchor","middle")
          .text(subtitle)

      // sub title line chart
      this.svg.append("text")
          .attr('y', -15)
          .attr("class", "sub-title")
          .attr('x', (this.width / 2) + 0)
          .attr('fill', "#000000")
          .attr("text-anchor","middle")
          .text(this.state.subTitle)

      // Add the trendline
      this.svg.append("path")
          .data([data])
          .attr("class", "line")
          .attr("d", this.valueline)
          .attr("stroke", this.props.color)
          .attr("stroke-width", 2)
          .attr("fill", this.props.fill)

      this.svg.append("g")
          .attr("transform", "translate(0," + this.height + ")")
          .call(d3.axisBottom(this.x)
          .ticks(3)
          .tickFormat((d) => {
              return d3.format("")(d)
          }));
      
      var scale = d3.scaleLinear()
          .domain([min() , max()])
          .range([this.height, 0]);

      this.svg.append("text")
          .attr("text-anchor", "end")
          .attr("x", this.width / 2)
          .attr("y", this.height + margin.top + 20)
          .text("Positions");

      // Y
      this.svg.append("g")
          .call(d3.axisLeft(this.y).scale(scale)
          .ticks(6)
          .tickFormat(function (d) {
              return d3.format("")(d)
          }));
      this.svg.append("text")
          .attr("text-anchor", "end")
          .attr("transform", "rotate(-90)")
          .attr("y", -(margin.left)+20)
          .attr("x", -(this.height / 2) + 20)
          .text("Loads")
  }

  check_select () {
    this.store.getState().TempWellID.forEach((item ,index) => {
      try{
        if (this.store.getState().selectWell[0].id === item){
          document.getElementById(item).style.borderTop = "4px solid #0a88beaf";
          document.getElementById(item).style.borderRight = "4px solid #0a88beaf";
          document.getElementById(item).style.borderBottom = "10px solid #0a88beaf";
          document.getElementById(item).style.borderLeft = "4px solid #0a88beaf";
        } else {
          document.getElementById(item).style.borderTop = "4px solid #8c8b8b44";
          document.getElementById(item).style.borderRight = "4px solid #8c8b8b44";
          document.getElementById(item).style.borderBottom = "10px solid #8c8b8b44";
          document.getElementById(item).style.borderLeft = "4px solid #8c8b8b44";
        }
      } catch (e){
        console.log(e)
      }
    })
  }

  render() {
    const { classes } = this.props;
    return (
        <div 
          className={((this.props.border) ? classes.root : classes.rootNoBorder) + " " + this.state.PropsClassName + " " + ((this.props.hover) ? "isHover" : "") + this.classselected}  
          ref={this.myRef}
          id={this.props.cid}
          onClick={() => { 
              if (this.props.onclick){
                this.store.dispatch(addwellselect([undefined]))
                const callFn  = () => {
                  this.store.dispatch(addwellselect([this.store.getState().wellResult.data[this.props.index]]))
                  localStorage.setItem('select_Dynachart', JSON.stringify(this.store.getState().wellResult.data[this.props.index]));
                  this.check_select()
                }
                setTimeout(() => { callFn() }, 1);
              }
          }}
        >
        </div>
    )
  }
}

export default withStyles(styles)(DynaChart)