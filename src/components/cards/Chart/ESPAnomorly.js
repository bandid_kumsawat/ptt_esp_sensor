//"d3": "^5.16.0",
import React, { Component } from 'react';
// import * as d3 from 'd3'; // 5.16.0
import withStyles from '@material-ui/styles/withStyles';
import './ESPLineChart.css'
// import moment from 'moment'
// // import data from './chartData.json'
// import dataJson from './data.json'
// import predictMock from './predict.json'

// import { 
//   esplinecolor,
//   esptickcolor,
//   espchartsetvalue,
//   espshowtime, 
//   espsetlinecolor
// } from './../../../actions';
// import { style } from 'd3';


import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import API from './../../../api/server'
import _ from "underscore"
import moment from 'moment'

const styles = theme => ({
  root: {
    color: "#000000",
    fontSize: '1em',
    border: "2px solid #8c8b8b44",
    borderTop: "4px solid #8c8b8b44",
    borderBottom: "10px solid #8c8b8b44",
    borderRight: "4px solid #8c8b8b44",
    borderLeft: "4px solid #8c8b8b44",
    borderRadius: "3px"
  },
  rootNoBorder: {
    color: "#000000",
    fontSize: '1em',
  },
  spacePaddingSvg: {
    paddingTop: '10px'
  }
});

class ESPAnomorly extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.myRef = React.createRef();
    this.state = {
      train: []
    }
    this.width = this.props.width
    this.height = this.props.height
    this.btnStatus = {
      "FTHP": true,
      "FTHT": true,
      "Current": true,
      "FlowRate": true,
      "Frequency": true,
      "MotorTemp": true,
      "SuctionPressure": true,
      "SuctionTemp": true,
      "Voltage": true,
    }
  }
  componentDidUpdate() {
  }
  async componentDidMount(){
    const res = await API().get('/train')
    this.setState({
      train: res.data
    })
    this.LineChartFunction();
  }
  LineChartFunction(){
    // var ESPdata = this.props.data
    am4core.useTheme(am4themes_animated);
    let chart = am4core.create(this.myRef.current, am4charts.XYChart);

    let data = [];
    let price1 = 1000
    for (var i = 0; i < 3000; i++) {
      price1 = this.state.train[i]
      
      data.push({ date1: new Date( moment('2015-04-09 00:00:00').add(5 * i, 'minutes').format('LLLL') ), price1: price1 , close: price1 + 10, open: price1 - 10});
    }
    chart.data = data;

    // Axis X
    let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.renderer.grid.template.location = 0;
    dateAxis.renderer.labels.template.fill = am4core.color("#000000");

    let dateAxis2 = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis2.renderer.grid.template.location = 0;
    dateAxis2.renderer.labels.template.fill = am4core.color("#000000");
    // Axis X
    
    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.labels.template.fill = am4core.color("#0000ffff");
    valueAxis.renderer.minWidth = 60;

    let valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis2.tooltip.disabled = true;
    valueAxis2.renderer.labels.template.fill = am4core.color("#ff0000ff");
    valueAxis2.renderer.minWidth = 60;

    let series = []
    if (1){
      series[0] = chart.series.push(new am4charts.LineSeries());
      series[0].name = "FHTP";
      series[0].dataFields.dateX = "date1";
      series[0].dataFields.valueY = "price1";
      series[0].strokeWidth = 2;

    }
    var bullet = series[0].bullets.push(new am4charts.Bullet());
    bullet.tooltipText = "Value: {valueY}";
    
    bullet.adapter.add("fill", function(fill, target){
        if(target.dataItem.valueY > 0.2){
            return am4core.color("#ff0000");
        }
        return fill;
    })

    var range = valueAxis.createSeriesRange(series[0]);
    range.value = 0.2;
    range.endValue = 1000;
    range.contents.stroke = am4core.color("#FF0000");
    range.contents.fill = range.contents.stroke;


    chart.cursor = new am4charts.XYCursor();
    chart.cursor.xAxis = dateAxis;

  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <div 
          className={classes.spacePaddingSvg}
          ref={this.myRef}
          style={{height: "400px"}}
        >
        </div>
      </div>
    )
  }
}

export default withStyles(styles)(ESPAnomorly)