import React, { Component } from 'react';
import * as d3 from 'd3';
import withStyles from '@material-ui/styles/withStyles';
import './ESPLineChart.css'
import moment from 'moment'
import data from './data.json'

const styles = theme => ({
  root: {
    color: "#000000",
    fontSize: '1em',
    border: "2px solid #8c8b8b44",
    borderTop: "4px solid #8c8b8b44",
    borderBottom: "10px solid #8c8b8b44",
    borderRight: "4px solid #8c8b8b44",
    borderLeft: "4px solid #8c8b8b44",
    borderRadius: "3px"
  },
  rootNoBorder: {
    color: "#000000",
    fontSize: '1em',
  },
  spacePaddingSvg: {
    paddingTop: '10px'
  }
});

class DynaChart extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.myRef = React.createRef();
    this.state = {
    }
    this.width = this.props.width
    this.height = this.props.height
    this.x = Object
    this.y = Object
    this.svg = Object
    this.valueline = Object
    this.classselected = ''
    this.FTHP = true
    this.current = true
    this.flowrate = true
  }
  componentDidUpdate() {
  }
  componentDidMount(){
    this.LineChartFunction();
  }
  LineChartFunction(){
    // section tooltip hover data
    var ToolTipFTHT = d3.select("body").append("div")
      .attr("class", "tooltip-dataFTHT")
      .attr("id", "tooltip_dataFTHT")
      .style("opacity", 0);
    var ToolTipFTHP = d3.select("body").append("div")
      .attr("class", "tooltip-dataFTHP")
      .attr("id", "tooltip_dataFTHP")
      .style("opacity", 0);
    // ---------------------------
    var that = this

    var dataFTHT = []
    var dataFTHP = []

    var dataAll = []

    // props data
    // var espdata = this.props.data
    var espdata = data

    espdata.data.forEach((item, index, arr) => {
      // console.log(new Date(item.Time))

      dataAll.push({
        "x": Number(moment.utc(new Date(item.Time)).format("x")),
        "y": Number(item.FTHT),
        "tag": "FTHT"
      })
      dataFTHT.push({
        "x": Number(moment.utc(new Date(item.Time)).format("x")),
        "y": Number(item.FTHT)
      })

      dataAll.push({
        "x": Number(moment.utc(new Date(item.Time)).format("x")),
        "y": Number(item.FTHP),
        "tag": "FTHP"
      })
      dataFTHP.push({
        "x": Number(moment.utc(new Date(item.Time)).format("x")),
        "y": Number(item.FTHP)
      })
    
    })
    
    
    var  max = () => { 
      var temp = Math.max.apply(Math, dataAll.map(function(o) { 
          return o.y;  
      })); 
      return temp
    }   
    var  min = () => { 
      var temp = Math.min.apply(Math, dataAll.map(function(o) { 
          return o.y;  
      })); 
      return temp
    } 
    var margin = {
      top: 10,
      right: 20,
      bottom: 100,
      left: 70
    }

    var padding_top = 30
    
    var default_width = this.width - margin.left - margin.right;
    
    var default_height = this.height - margin.top - margin.bottom;
    
    var default_ratio = default_width / default_height;

    const set_size = () => {
        var w, h
        var current_width = window.innerWidth;
        
        var current_height = window.innerHeight;

        var current_ratio = current_width / current_height;

        // desktop
        if (current_ratio > default_ratio) {
            h = default_height;
            w = default_width;

        // mobile
        } else {
            margin.left = 40
            w = current_width - 40;
            h = w / default_ratio;
        }
        // Set new width and height based on graph dimensions
        this.width = w - 50 - margin.right;
        this.height = h - margin.top - margin.bottom;
    }

    set_size();
    //end responsive graph

    // set the ranges
    this.x = d3.scaleTime().range([0, this.width]);
    this.y = d3.scaleLinear().range([this.height, 0])

    this.x.domain([d3.min(dataAll, function (d) {
        return d.x;
    }), d3.max(dataAll, function (d) {
        return d.x;
    })]);
    this.y.domain([d3.min(dataAll, function (d) {
        return d.y - 30;
    }), d3.max(dataAll, function (d) {
        return d.y + 30;
    })]);

    // define the line
    this.valueline = d3.line()
        // .curve(d3.curveCatmullRom.alpha(1))
        .x(function (d) {
            return that.x(d.x);
        })
        .y(function (d) {
            return that.y(d.y);
        });

      // append the svg object to the body of the page
      
      // var svg = d3.select(this.myRef.current).remove()
      this.svg = d3.select(this.myRef.current).append("svg")
          // .attr("style", "padding-top: "+padding_top+";")
          .attr("width", this.width + margin.left + margin.right)
          .attr("height", this.height + margin.bottom/* + margin.top + margin.bottom + padding_top*/)
          .attr("viewBox", "0 0 "+(this.width + margin.left + margin.right + 40)+" " + (this.height + margin.bottom  /*+ margin.top + margin.bottom + padding_top*/))
          .append("g")
          .attr("transform", "translate(" + margin.left + "," + (margin.top + 40) + ")")

      // title line chart
      var title = this.props.title
      this.svg.append("text")
          .attr('y', -40)
          .attr("class", "title-line-chart")
          .attr('x', (this.width / 2) + 0)
          .attr('fill', "#000000")
          .attr("style", "font-size: 1.5em")
          .attr("text-anchor","middle")
          .text(title)
        
      this.svg.append('rect')
        .attr('y', 0)
        .attr('x', 0)
        .attr('fill', "#000000")
        .attr("style", "font-size: 1.5em")
        .text('clickme')

      // sub title line chart
      this.svg.append("text")
          .attr('y', -15)
          .attr("class", "sub-title")
          .attr('x', (this.width / 2) + 0)
          .attr('fill', "#000000")
          .attr("text-anchor","middle")
          .text(this.state.subTitle)

      // Add the trendline

      // for line FTHT
      var LineDataFTHT = this.svg.append("path")
          .data([dataFTHT])
          .attr("class", "line")
          .attr("d", this.valueline)
          .attr("stroke", "#29abe266")
          .attr("stroke-width", 2)
          .attr("fill", "#ffffff")
      // for line FTHP
      var LineDataFTHP = this.svg.append("path")
          .data([dataFTHP])
          .attr("class", "line")
          .attr("d", this.valueline)
          .attr("stroke", "#29abe266")
          .attr("stroke-width", 2)
          .attr("fill", "#ffffff")
        
      var xAis = this.svg.append("g")
          .attr("transform", "translate(0," + this.height + ")")
          .call(d3.axisBottom(this.x)
          .ticks(6)
          .tickFormat((d) => {
              var format = d3.timeFormat("%Y-%m-%dT%H:%M:%S.%LZ")
              return format(d)
          }));
      
      var scale = d3.scaleLinear()
          .domain([min() - 30 , max() + 30])
          .range([this.height, 0]);

      this.svg.append("text")
          .attr("text-anchor", "end")
          .attr("x", this.width / 2)
          .attr("y", this.height + margin.top + 20)
          .text("");

      // Y
      var yAis = this.svg.append("g")
          .call(d3.axisLeft(this.y).scale(scale)
          .ticks(6)
          .tickFormat(function (d) {
              return d3.format("")(d)
          }));
      this.svg.append("text")
          .attr("text-anchor", "end")
          .attr("transform", "rotate(-90)")
          .attr("y", -(margin.left)+20)
          .attr("x", -(this.height / 2) + 20)
          .text("")

      // for dot FTHP
      var DotDataFTHP = this.svg
        .append("g")
        .selectAll("dot")
        .data(dataFTHP)
        .enter()
        .append("circle")
          .attr("class", "myCircle")
          .attr("cx", function(d) { return that.x(d.x) } )
          .attr("cy", function(d) { return that.y(d.y) } )
          .attr("r", 1)
          .attr("stroke", "#29abe2ff")
          .attr("stroke-width", 3)
          .attr("fill", "white")
          .on("mouseover", (d, i) => {
            var elmnt = document.getElementById("tooltip_dataFTHP");
            ToolTipFTHP.transition()
                .duration(50)
                .style("opacity", 1);
            var strout = "<b>FTHP</b><br>value: " + String(d.y) + "<br>" + "Time: " + moment(d.x, 'x').format('DD-MMMM-YYYY hh:mm:ss a')
            ToolTipFTHP.html(strout)
                .style("left", (d3.event.pageX - (elmnt.offsetWidth / 2)) + "px")
                .style("top", (d3.event.pageY - elmnt.offsetHeight - 10) + "px");
          })
          .on("mouseleave", (d, i) => {
            ToolTipFTHP.style("opacity", 0);
          })
      // for dot FTHT
      var DotDataFTHT = this.svg
        .append("g")
        .selectAll("dot")
        .data(dataFTHT)
        .enter()
        .append("circle")
          .attr("class", "myCircle")
          .attr("cx", function(d) { return that.x(d.x) } )
          .attr("cy", function(d) { return that.y(d.y) } )
          .attr("r", 1)
          .attr("stroke", "#00ff00")
          .attr("stroke-width", 3)
          .attr("fill", "white")
          .on("mouseover", (d, i) => {
            var elmnt = document.getElementById("tooltip_dataFTHT");
            ToolTipFTHT.transition()
                .duration(50)
                .style("opacity", 1);
            var strout = "<b>FTHT</b><br>value: " +d.y + "<br>" + "Time: " + moment(d.x, 'x').format('DD-MMMM-YYYY hh:mm:ss a')
            ToolTipFTHT.html(strout)
                .style("left", (d3.event.pageX - (elmnt.offsetWidth / 2)) + "px")
                .style("top", (d3.event.pageY - elmnt.offsetHeight - 10) + "px");
          })
          .on("mouseleave", (d, i) => {
            ToolTipFTHT.style("opacity", 0);
          })
          
      d3.select("#container_btn").append('button')
        .attr('class', 'btn-button FTHP')
        .text("FTHP")
        .on("click", function(d) {
          if (that.FTHP){
            console.log(dataAll)
            d3.select(this).attr('class', 'btn-button btn-button-inactive')
            
            // remove old dot and line
            // DotDataFTHP.remove()
            // LineDataFTHP.remove()
            // DotDataFTHT.remove()
            // LineDataFTHT.remove()

            // remove data
            dataAll.forEach((item, index) => {
              if (item.tag === 'FTHP') {
                dataAll.splice(index, 1)
              }
            })

            that.y.domain([d3.min(dataAll, function (d) {
                return d.y - 30;
            }), d3.max(dataAll, function (d) {
                return d.y + 30;
            })]);

            yAis.transition().duration(1000).call(d3.axisLeft(that.y))

            DotDataFTHP.attr("visibility", "hidden")
            LineDataFTHP.attr("visibility", "hidden")
            DotDataFTHT.remove()
            LineDataFTHT
                .datum(dataFTHT)
                .transition().duration(100)
                .attr("class", "line")
                .attr("d", that.valueline)
                .attr("stroke", "#29abe266")
                .attr("stroke-width", 2)
                .attr("fill", "#ffffff")
                
            DotDataFTHT = that.svg
                .append("g")
                .selectAll("dot")
                .data(dataFTHT)
                .enter()
                .append("circle")
                  .attr("class", "myCircle")
                  .attr("cx", function(d) { return that.x(d.x) } )
                  .attr("cy", function(d) { return that.y(d.y) } )
                  .attr("r", 1)
                  .attr("stroke", "#00ff00")
                  .attr("stroke-width", 3)
                  .attr("fill", "white")
                  .on("mouseover", (d, i) => {
                    var elmnt = document.getElementById("tooltip_dataFTHT");
                    ToolTipFTHT.transition()
                        .duration(50)
                        .style("opacity", 1);
                    var strout = "<b>FTHT</b><br>value: " +d.y + "<br>" + "Time: " + moment(d.x, 'x').format('DD-MMMM-YYYY hh:mm:ss a')
                    ToolTipFTHT.html(strout)
                        .style("left", (d3.event.pageX - (elmnt.offsetWidth / 2)) + "px")
                        .style("top", (d3.event.pageY - elmnt.offsetHeight - 10) + "px");
                  })
                  .on("mouseleave", (d, i) => {
                    ToolTipFTHT.style("opacity", 0);
                  })
          } else {
            console.log(dataAll)
            espdata.data.forEach((item, index) => {
              dataAll.push({
                "x": Number(moment.utc(new Date(item.Time)).format("x")),
                "y": Number(item.FTHP),
                "tag": "FTHP"
              })
            })

            that.y.domain([d3.min(dataAll, function (d) {
                return d.y - 30;
            }), d3.max(dataAll, function (d) {
                return d.y + 30;
            })]);

            yAis.transition().duration(1000).call(d3.axisLeft(that.y))
            
            DotDataFTHP.attr("visibility", "visible")
            LineDataFTHP.attr("visibility", "visible")

            LineDataFTHT
              .datum(dataFTHT)
              .transition().duration(100)
              .attr("class", "line")
              .attr("d", that.valueline)
              .attr("stroke", "#29abe266")
              .attr("stroke-width", 2)
              .attr("fill", "#ffffff")
            DotDataFTHT.remove()
            DotDataFTHT = that.svg
              .append("g")
              .selectAll("dot")
              .data(dataFTHT)
              .enter()
              .append("circle")
                .attr("class", "myCircle")
                .attr("cx", function(d) { return that.x(d.x) } )
                .attr("cy", function(d) { return that.y(d.y) } )
                .attr("r", 1)
                .attr("stroke", "#00ff00")
                .attr("stroke-width", 3)
                .attr("fill", "white")
                .on("mouseover", (d, i) => {
                  var elmnt = document.getElementById("tooltip_dataFTHT");
                  ToolTipFTHT.transition()
                      .duration(50)
                      .style("opacity", 1);
                  var strout = "<b>FTHT</b><br>value: " +d.y + "<br>" + "Time: " + moment(d.x, 'x').format('DD-MMMM-YYYY hh:mm:ss a')
                  ToolTipFTHT.html(strout)
                      .style("left", (d3.event.pageX - (elmnt.offsetWidth / 2)) + "px")
                      .style("top", (d3.event.pageY - elmnt.offsetHeight - 10) + "px");
                })
                .on("mouseleave", (d, i) => {
                  ToolTipFTHT.style("opacity", 0);
                })



            d3.select(this).attr('class', 'btn-button FTHP')
          }
          
          that.FTHP = !that.FTHP
        })

        d3.select("#container_btn").append('button')
          .attr('class', 'btn-button FTHT')
          .text("FTHT")
          .on("click", function(d) {
            if (that.FTHT){
              d3.select(this).attr('class', 'btn-button btn-button-inactive')

              DotDataFTHT.remove()
              LineDataFTHT.remove()

              // remove data
              dataFTHT.forEach((item, index) => {
                if (item.tag === 'FTHT') {
                  dataAll.push(index, 1)
                }
              })

              that.y.domain([d3.min(dataAll, function (d) {
                  return d.y - 30;
              }), d3.max(dataAll, function (d) {
                  return d.y + 30;
              })]);

              yAis.transition().duration(1000).call(d3.axisLeft(that.y))
              } else {
                d3.select(this).attr('class', 'btn-button FTHT')

                DotDataFTHT.remove()
                LineDataFTHT.remove()

                // remove data
                dataAll.forEach((item, index) => {
                  if (item.tag === 'FTHT') {
                    dataAll.splice(index, 1)
                  }
                })
                
              }
            that.FTHT = !that.FTHT
          })
              
  }

  UpdateLine(){

  }

  render() {
    const { classes } = this.props;
    return (
        <div>
          <div 
            className={classes.spacePaddingSvg}
            ref={this.myRef}
          >
          </div>
          <div id='container_btn' className='container-btn'></div>
        </div>
    )
  }
}

export default withStyles(styles)(DynaChart)