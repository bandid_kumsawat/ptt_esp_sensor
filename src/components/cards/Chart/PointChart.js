import React, { Component } from 'react';
import * as d3 from 'd3'
import withStyles from '@material-ui/styles/withStyles';
import moment from 'moment'

import { addwellselect} from './../../../actions';

const styles = theme => ({
  root: {
    color: "#000000",
    fontSize: '1em'
  }
});

class DynaChart extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.myRef = React.createRef();
    this.state = {
      pointdata: [],
      str: '',
      wellid: []
    }
    this.SVG = Object
    this.scatter = Array
  }

  componentDidUpdate() {

  }

  async componentDidMount(){
    // const res = await API().post('/PointChart', {
    //   "WellName": localStorage.getItem('device'),
    // })
    this.setState({pointdata: this.props.PointChart})
    
    // this.setState({wellid: this.store.getState().TempWellID})
    this.LineChartFunction()
  }

  LineChartFunction() {

    // tooltip
    var PointChartToolTip = d3.select("body").append("div")
      .attr("class", "point-tip")
      .style("opacity", 0);

    var margin = {top: 10, right: 30, bottom: 30, left: 60},
        width = this.props.width - margin.left - margin.right,
        height = this.props.height - margin.top - margin.bottom;

    this.SVG = d3.select(this.myRef.current)
        .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
        .append("g")
          .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")");

    d3.select("body").append("div")
        .attr("class", "tooltip")
        .style("opacity", 0);

    var x = d3.scaleLinear()
        .domain([0,1])
        .range([ 0, width ]);

    this.SVG.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x));

    var y = d3.scaleLinear()
        .domain([0, 1])
        .range([ height, 0]);
    
    this.SVG.append("g")
        .call(d3.axisLeft(y));

    this.SVG.append("defs").append("SVG:clipPath")
        .attr("id", "clip")
        .append("SVG:rect")
        .attr("width", width )
        .attr("height", height )
        .attr("x", 0)
        .attr("y", 0);

    this.scatter = []
    const color = "#29abe2"
    var that = this
    for (var i =0;i < this.props.PointChart.length;i ++){
        this.scatter[i] = this.SVG.append('g')
            .attr("clip-path", "url(#clip)")
        this.scatter[i].selectAll("circle")
            .data([ this.props.PointChart[i] ])
            .enter()
            .append("circle")
              .attr('id', this.props.PointChart[i].id)
              .attr("cx", function (d) { return x(d.x); } )
              .attr("cy", function (d) { return y(d.y); } )
              .attr("r", 4)
              .attr("fill", color)
              .style("opacity", 1)
            .on('mouseover', function (d, i) {
              d3.select(this).transition()
                  .duration('100')
                  .attr('fill', "#0971bb")
                  .attr("r", 6)
              PointChartToolTip.transition()
                  .duration(50)
                  .style("opacity", 1);
              var strout = "<div style='font-size: 10px' class='ttrow'><div style='font-size: 10px' class='tttitle'>Well Name: </div><div class='ttvalue' style='font-size: 10px'>"+d.WellName+"</div></div>     <div class='ttrow' style='font-size: 10px'><div class='tttitle' style='font-size: 10px'>Card Report Date: </div><div class='ttvalue' style='font-size: 10px'>"+ moment(d.CardDateTime).format('DD-MMMM-YYYY HH:MM:SS')+"</div>    <div class='ttrow' style='font-size: 10px'><div class='tttitle' style='font-size: 10px'>Card Type: </div><div class='ttvalue' style='font-size: 10px'>"+ d.CardType +"</div>    <div class='ttrow' style='font-size: 10px'><div class='tttitle' style='font-size: 10px'>X: </div><div class='ttvalue' style='font-size: 10px'>"+d.x+"</div>    <div class='ttrow' style='font-size: 10px'><div class='tttitle' style='font-size: 10px'>Y: </div><div class='ttvalue' style='font-size: 10px'>"+d.y+"</div>"
              PointChartToolTip.html(strout)
                  .style("left", (d3.event.pageX + 10) + "px")
                  .style("top", (d3.event.pageY - 15) + "px");
            })
            .on('mouseout', function (d, i) {
              d3.select(this).transition()
                  .duration('100')
                  .attr('fill', color)
                  .attr("r", 4)
              PointChartToolTip.style("opacity", 0);
            })
            .on('click', (d, i) => {
              d3.selectAll(this).call(this.onshow_data, d, that, i);
            })
    }


  }
  onshow_data(onshow_data, data, that, wellid) {
    localStorage.setItem('select_Dynachart', JSON.stringify(data));
    that.store.dispatch(addwellselect([undefined]))
    const callFn  = () => {
      that.store.dispatch(addwellselect([data]))
      that.store.getState().TempWellID.forEach((item, index) => {
        try{
          if (that.store.getState().selectWell[0].id === item){
            document.getElementById(item).scrollIntoView();
            document.getElementById(item).style.borderTop = "4px solid #0a88beaf";
            document.getElementById(item).style.borderRight = "4px solid #0a88beaf";
            document.getElementById(item).style.borderBottom = "10px solid #0a88beaf";
            document.getElementById(item).style.borderLeft = "4px solid #0a88beaf";
          } else {
            document.getElementById(item).style.borderTop = "4px solid #8c8b8b44";
            document.getElementById(item).style.borderRight = "4px solid #8c8b8b44";
            document.getElementById(item).style.borderBottom = "10px solid #8c8b8b44";
            document.getElementById(item).style.borderLeft = "4px solid #8c8b8b44";
          }
        }catch(e){
          console.log(e)
        }

      })
    }
    setTimeout(() => { callFn() }, 1);
    // window.location.reload()
  }
  render() {
    const { classes } = this.props;
    return (
        <div 
          className={classes.root}
        >
          <div ref={this.myRef}></div>
          {this.state.str}
        </div>
    )
  }
}

export default withStyles(styles)(DynaChart)