//"d3": "^5.16.0",
import React, { Component } from 'react';
// import * as d3 from 'd3'; // 5.16.0
import withStyles from '@material-ui/styles/withStyles';
import './ESPLineChart.css'
import moment from 'moment'
// // import data from './chartData.json'
// import dataJson from './data.json'
// import predictMock from './predict.json'

// import { 
//   esplinecolor,
//   esptickcolor,
//   espchartsetvalue,
//   espshowtime, 
//   espsetlinecolor
// } from './../../../actions';
// import { style } from 'd3';


import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";


const styles = theme => ({
  root: {
    color: "#000000",
    fontSize: '1em',
    border: "2px solid #8c8b8b44",
    borderTop: "4px solid #8c8b8b44",
    borderBottom: "10px solid #8c8b8b44",
    borderRight: "4px solid #8c8b8b44",
    borderLeft: "4px solid #8c8b8b44",
    borderRadius: "3px"
  },
  rootNoBorder: {
    color: "#000000",
    fontSize: '1em',
  },
  spacePaddingSvg: {
    paddingTop: '10px'
  }
});

class DynaChart extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.myRef = React.createRef();
    this.state = {

    }
    this.width = this.props.width
    this.height = this.props.height
    this.btnStatus = {
      "FTHP": true,
      "FTHT": true,
      "Current": true,
      "FlowRate": true,
      "Frequency": true,
      "MotorTemp": true,
      "SuctionPressure": true,
      "SuctionTemp": true,
      "Voltage": true,
    }
  }
  componentDidUpdate() {
  }
  componentDidMount(){
    this.LineChartFunction();
  }
  LineChartFunction(){
    var ESPdata = this.props.data.data
    let SeriesColor = this.props.color
    am4core.useTheme(am4themes_animated);
    let chart = am4core.create(this.myRef.current, am4charts.XYChart);
    let data = [];
    let dateRange = ''
    // Current
    for (var m = 0; m < ESPdata.length; m++) {
      var color = am4core.color(SeriesColor['FTHP'] + "FF");
      var temp = {
        date_FTHP: new Date(ESPdata[m]['Time']),
        FTHP: Number(ESPdata[m]['FTHP']),

        color: color
      }
      data.push(temp);
    }
    for (var m = 0; m < ESPdata.length; m++) {
      var color = am4core.color(SeriesColor['FTHT'] + "FF")
      var temp = {
        date_FTHT: new Date(ESPdata[m]['Time']),
        FTHT: Number(ESPdata[m]['FTHT']),

        color: color
      }
      data.push(temp);
    }
    for (var m = 0; m < ESPdata.length; m++) {
      var color = am4core.color(SeriesColor['FlowRate'] + "FF")
      var temp = {
        date_FlowRate: new Date(ESPdata[m]['Time']),
        FlowRate: Number(ESPdata[m]['Flow rate']),

        color: color
      }
      data.push(temp);
    }
    for (var m = 0; m < ESPdata.length; m++) {
      var color = am4core.color(SeriesColor['Current'] + "FF")
      var temp = {
        date_Current: new Date(ESPdata[m]['Time']),
        Current: Number(ESPdata[m]['Current']),
    
        color: color
      }
      data.push(temp);
    }
    for (var m = 0; m < ESPdata.length; m++) {
      var color = am4core.color(SeriesColor['Frequency'] + "FF")
      var temp = {
        date_Frequency: new Date(ESPdata[m]['Time']),
        Frequency: Number(ESPdata[m]['Frequency']),
    
        color: color
      }
      data.push(temp);
    }
    for (var m = 0; m < ESPdata.length; m++) {
      var color = am4core.color(SeriesColor['MotorTemp'] + "FF")
      var temp = {
        date_MotorTemp: new Date(ESPdata[m]['Time']),
        MotorTemp: Number(ESPdata[m]['Motor temp']),
    
        color: color
      }
      data.push(temp);
    }
    for (var m = 0; m < ESPdata.length; m++) {
      var color = am4core.color(SeriesColor['SuctionPressure'] + "FF")
      var temp = {
        date_SuctionPressure: new Date(ESPdata[m]['Time']),
        SuctionPressure: Number(ESPdata[m]['Suction pressure']),
    
        color: color
      }
      data.push(temp);
    }
    for (var m = 0; m < ESPdata.length; m++) {
      var color = am4core.color(SeriesColor['SuctionTemp'] + "FF")
      var temp = {
        date_SuctionTemp: new Date(ESPdata[m]['Time']),
        SuctionTemp: Number(ESPdata[m]['Suction temp']),
    
        color: color
      }
      data.push(temp);
    }
    for (var m = 0; m < ESPdata.length; m++) {
      var color = am4core.color(SeriesColor['Voltage'] + "FF")
      var temp = {
        date_Voltage: new Date(ESPdata[m]['Time']),
        Voltage: Number(ESPdata[m]['Voltage']),
    
        color: color
      }
      data.push(temp);
    }
    // Predict

    const Predict = () => {
      console.log('this.props.predict')
      console.log(this.props.predict)
      this.props.predict.forEach((item ,index) => {

        var color = am4core.color(SeriesColor['FTHP'] + "88");
        var temp = { 
          date_FTHP: new Date( moment(item['Time']).format('LLLL') ),
          FTHP: Number(item['FTHP']),

          color: color
        }
        dateRange = temp.date_FTHP

        var color = am4core.color(SeriesColor['FlowRate'] + "88")
        var temp = {
          date_FlowRate: new Date( moment(item['Time']).format('LLLL') ),
          FlowRate: Number(item['Liquid flow rate:']),

          color: color
        }
        data.push(temp);

        var color = am4core.color(SeriesColor['FTHT'] + "88")
        var temp = {
          date_FTHT: new Date( moment(item['Time']).format('LLLL') ),
          FTHT: Number(item['FTHT']),

          color: color
        }
        data.push(temp);

        var color = am4core.color(SeriesColor['Current'] + "88")
        var temp = {
          date_Current: new Date( moment(item['Time']).format('LLLL') ),
          Current: Number(item['Current']),
      
          color: color
        }
        data.push(temp);

        var color = am4core.color(SeriesColor['Frequency'] + "88")
        var temp = {
          date_Frequency: new Date( moment(item['Time']).format('LLLL') ),
          Frequency: Number(item['Frequency']),
      
          color: color
        }
        data.push(temp);


        var color = am4core.color(SeriesColor['MotorTemp'] + "88")
        var temp = {
          date_MotorTemp: new Date( moment(item['Time']).format('LLLL') ),
          MotorTemp: Number(item['Motor temp']),
      
          color: color
        }
        data.push(temp);

        var color = am4core.color(SeriesColor['SuctionPressure'] + "88")
        var temp = {
          date_SuctionPressure: new Date( moment(item['Time']).format('LLLL') ),
          SuctionPressure: Number(item['Suction pressure']),
      
          color: color
        }
        data.push(temp);


        var color = am4core.color(SeriesColor['SuctionTemp'] + "88")
        var temp = {
          date_SuctionTemp: new Date( moment(item['Time']).format('LLLL') ),
          SuctionTemp: Number(item['Suction temp']),
      
          color: color
        }
        data.push(temp);

        var color = am4core.color(SeriesColor['Voltage'] + "88")
        var temp = {
          date_Voltage: new Date( moment(item['Time']).format('LLLL') ),
          Voltage: Number(item['Voltage']),
      
          color: color
        }
        data.push(temp);
        console.log(data)
      })
    }
    Predict()
    // for (var m = 0; m < 1000; m++) {
    //   var color = am4core.color(SeriesColor['FTHP'] + "88");
    //   var temp = { 
    //     date_FTHP: new Date( moment(ESPdata[ESPdata.length - 1]['Time']).add(5 * m, 'minutes').format('LLLL') ),
    //     FTHP: Number(ESPdata[ESPdata.length - 1]['FTHP']),

    //     color: color
    //   }
    //   data.push(temp);
    //   dateRange = temp.date_FTHP
    // }
    // for (var m = 0; m < 1000; m++) {
    //   var color = am4core.color(SeriesColor['FlowRate'] + "88")
    //   var temp = {
    //     date_FlowRate: new Date( moment(ESPdata[ESPdata.length - 1]['Time']).add(5 * m, 'minutes').format('LLLL') ),
    //     FlowRate: Number(ESPdata[ESPdata.length - 1]['Flow rate']),

    //     color: color
    //   }
    //   data.push(temp);
    // }
    // for (var m = 0; m < 1000; m++) {
    //   var color = am4core.color(SeriesColor['FTHT'] + "88")
    //   var temp = {
    //     date_FTHT: new Date( moment(ESPdata[ESPdata.length - 1]['Time']).add(5 * m, 'minutes').format('LLLL') ),
    //     FTHT: Number(ESPdata[ESPdata.length - 1]['FTHT']),

    //     color: color
    //   }
    //   data.push(temp);
    // }
    // for (var m = 0; m < 1000; m++) {
    //   var color = am4core.color(SeriesColor['Current'] + "88")
    //   var temp = {
    //     date_Current: new Date( moment(ESPdata[ESPdata.length - 1]['Time']).add(5 * m, 'minutes').format('LLLL') ),
    //     Current: Number(ESPdata[ESPdata.length - 1]['Current']),
    
    //     color: color
    //   }
    //   data.push(temp);
    // }
    // for (var m = 0; m < 1000; m++) {
    //   var color = am4core.color(SeriesColor['Frequency'] + "88")
    //   var temp = {
    //     date_Frequency: new Date( moment(ESPdata[ESPdata.length - 1]['Time']).add(5 * m, 'minutes').format('LLLL') ),
    //     Frequency: Number(ESPdata[ESPdata.length - 1]['Frequency']),
    
    //     color: color
    //   }
    //   data.push(temp);
    // }
    // for (var m = 0; m < 1000; m++) {
    //   var color = am4core.color(SeriesColor['MotorTemp'] + "88")
    //   var temp = {
    //     date_MotorTemp: new Date( moment(ESPdata[ESPdata.length - 1]['Time']).add(5 * m, 'minutes').format('LLLL') ),
    //     MotorTemp: Number(ESPdata[ESPdata.length - 1]['Motor temp']),
    
    //     color: color
    //   }
    //   data.push(temp);
    // }
    // for (var m = 0; m < 1000; m++) {
    //   var color = am4core.color(SeriesColor['SuctionPressure'] + "88")
    //   var temp = {
    //     date_SuctionPressure: new Date( moment(ESPdata[ESPdata.length - 1]['Time']).add(5 * m, 'minutes').format('LLLL') ),
    //     SuctionPressure: Number(ESPdata[ESPdata.length - 1]['Suction pressure']),
    
    //     color: color
    //   }
    //   data.push(temp);
    // }
    // for (var m = 0; m < 1000; m++) {
    //   var color = am4core.color(SeriesColor['SuctionTemp'] + "88")
    //   var temp = {
    //     date_SuctionTemp: new Date( moment(ESPdata[ESPdata.length - 1]['Time']).add(5 * m, 'minutes').format('LLLL') ),
    //     SuctionTemp: Number(ESPdata[ESPdata.length - 1]['Suction temp']),
    
    //     color: color
    //   }
    //   data.push(temp);
    // }
    // for (var m = 0; m < 1000; m++) {
    //   var color = am4core.color(SeriesColor['Voltage'] + "88")
    //   var temp = {
    //     date_Voltage: new Date( moment(ESPdata[ESPdata.length - 1]['Time']).add(5 * m, 'minutes').format('LLLL') ),
    //     Voltage: Number(ESPdata[ESPdata.length - 1]['Voltage']),
    
    //     color: color
    //   }
    //   data.push(temp);
    // }

    chart.data = data;

    // Axis X
    let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.renderer.grid.template.location = 0;
    dateAxis.renderer.labels.template.fill = am4core.color("#000000");

    // dateAxis.groupData = true;
    // dateAxis.groupCount = 1000;

    // range
    let range = dateAxis.axisRanges.create();
    range.date = new Date(ESPdata[0]['Time']);
    range.endDate = new Date(ESPdata[ESPdata.length - 1]['Time']);
    range.axisFill.fill = am4core.color("#0877c966");
    range.axisFill.fillOpacity = 0.2;

    range.label.text = "Current";
    range.label.inside = true;
    range.label.rotation = 90;
    range.label.horizontalCenter = "right";
    range.label.verticalCenter = "bottom";


    let range2 = dateAxis.axisRanges.create();
    range2.date = new Date(ESPdata[ESPdata.length - 1]['Time']);
    range2.endDate = dateRange;
    range2.axisFill.fill = am4core.color("#db02d1aa");
    range2.axisFill.fillOpacity = 0.2;

    range2.label.text = "Prediction";
    range2.label.inside = true;
    range2.label.rotation = 90;
    range2.label.horizontalCenter = "right";
    range2.label.verticalCenter = "bottom";

    let dateAxis2 = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis2.renderer.grid.template.location = 0;
    dateAxis2.renderer.labels.template.fill = am4core.color("#ff00ff");
    // Axis X
    
    // 1. add Axis


  

    // 2 add Series
    let series = [], scrollbarXseries

    // FTHP
    if (this.store.getState().SelectFTHP){
      let valueAxis_FTHP = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis_FTHP.tooltip.disabled = true;
      valueAxis_FTHP.renderer.labels.template.fill = am4core.color(SeriesColor['FTHP'] + "ff");
      valueAxis_FTHP.renderer.minWidth = 60;
      series[0] = chart.series.push(new am4charts.LineSeries());
      series[0].name = "FTHP";
      series[0].dataFields.dateX = "date_FTHP";
      series[0].dataFields.valueY = "FTHP";
      series[0].yAxis = valueAxis_FTHP;
      series[0].tooltipText = "FTHP: {valueY.value}";
      series[0].propertyFields.stroke = "color";
      series[0].strokeWidth = 2;


      var bullet = series[0].bullets.push(new am4charts.Bullet());
      var square = bullet.createChild(am4core.Rectangle);
      square.width = 2;
      square.height = 2;



      scrollbarXseries = series[0]
    }

    // FTHT
    if (this.store.getState().SelectFTHT){
      let valueAxis_FTHT = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis_FTHT.tooltip.disabled = true;
      valueAxis_FTHT.renderer.labels.template.fill = am4core.color(SeriesColor['FTHT'] + "ff");
      valueAxis_FTHT.renderer.minWidth = 60;
      series[1] = chart.series.push(new am4charts.LineSeries());
      series[1].name = "FTHT";
      series[1].dataFields.dateX = "date_FTHT";
      series[1].dataFields.valueY = "FTHT";
      series[1].yAxis = valueAxis_FTHT;
      series[1].tooltipText = "FTHT: {valueY.value}";
      series[1].propertyFields.stroke = "color";
      series[1].strokeWidth = 2;

      var bullet = series[1].bullets.push(new am4charts.Bullet());
      var square = bullet.createChild(am4core.Rectangle);
      square.width = 2;
      square.height = 2;

      scrollbarXseries = series[1]
    }

    // FlowRate
    if (this.store.getState().SelectFlowRate){
      let valueAxis_FlowRate = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis_FlowRate.tooltip.disabled = true;
      valueAxis_FlowRate.renderer.labels.template.fill = am4core.color(SeriesColor['FlowRate'] + "ff");
      valueAxis_FlowRate.renderer.minWidth = 60;
      series[2] = chart.series.push(new am4charts.LineSeries());
      series[2].name = "FlowRate";
      series[2].dataFields.dateX = "date_FlowRate";
      series[2].dataFields.valueY = "FlowRate";
      series[2].yAxis = valueAxis_FlowRate;
      series[2].tooltipText = "FlowRate: {valueY.value}";
      series[2].propertyFields.stroke = "color";
      series[2].strokeWidth = 2;

      var bullet = series[2].bullets.push(new am4charts.Bullet());
      var square = bullet.createChild(am4core.Rectangle);
      square.width = 2;
      square.height = 2;

      scrollbarXseries = series[2]
    }

    // Current
    if (this.store.getState().SelectCurrent){
      let valueAxis_Current = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis_Current.tooltip.disabled = true;
      valueAxis_Current.renderer.labels.template.fill = am4core.color(SeriesColor['Current'] + "ff");
      valueAxis_Current.renderer.minWidth = 60;
      series[3] = chart.series.push(new am4charts.LineSeries());
      series[3].name = "Current";
      series[3].dataFields.dateX = "date_Current";
      series[3].dataFields.valueY = "Current";
      series[3].yAxis = valueAxis_Current;
      series[3].tooltipText = "Current: {valueY.value}";
      series[3].propertyFields.stroke = "color";
      series[3].strokeWidth = 2;

      var bullet = series[3].bullets.push(new am4charts.Bullet());
      var square = bullet.createChild(am4core.Rectangle);
      square.width = 2;
      square.height = 2;
      
      scrollbarXseries = series[3]
    }

    // Frequency
    if (this.store.getState().SelectFrequency){
      let valueAxis_Frequency = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis_Frequency.tooltip.disabled = true;
      valueAxis_Frequency.renderer.labels.template.fill = am4core.color(SeriesColor['Frequency'] + "ff");
      valueAxis_Frequency.renderer.minWidth = 60;
      series[4] = chart.series.push(new am4charts.LineSeries());
      series[4].name = "Frequency";
      series[4].dataFields.dateX = "date_Frequency";
      series[4].dataFields.valueY = "Frequency";
      series[4].yAxis = valueAxis_Frequency;
      series[4].tooltipText = "Frequency: {valueY.value}";
      series[4].propertyFields.stroke = "color";
      series[4].strokeWidth = 2;

      var bullet = series[4].bullets.push(new am4charts.Bullet());
      var square = bullet.createChild(am4core.Rectangle);
      square.width = 2;
      square.height = 2;

      scrollbarXseries = series[4]
    }

    // MotorTemp
    if (this.store.getState().SelectMotorTemp){
      
      let valueAxis_MotorTemp = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis_MotorTemp.tooltip.disabled = true;
      valueAxis_MotorTemp.renderer.labels.template.fill = am4core.color(SeriesColor['MotorTemp'] + "ff");
      valueAxis_MotorTemp.renderer.minWidth = 60;
      series[5] = chart.series.push(new am4charts.LineSeries());
      series[5].name = "MotorTemp";
      series[5].dataFields.dateX = "date_MotorTemp";
      series[5].dataFields.valueY = "MotorTemp";
      series[5].yAxis = valueAxis_MotorTemp;
      series[5].tooltipText = "MotorTemp: {valueY.value}";
      series[5].propertyFields.stroke = "color";
      series[5].strokeWidth = 2;

      var bullet = series[5].bullets.push(new am4charts.Bullet());
      var square = bullet.createChild(am4core.Rectangle);
      square.width = 2;
      square.height = 2;
      
      scrollbarXseries = series[5]
    }

    // SuctionPressure
    if (this.store.getState().SelectSuctionPressure){
      let valueAxis_SuctionPressure = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis_SuctionPressure.tooltip.disabled = true;
      valueAxis_SuctionPressure.renderer.labels.template.fill = am4core.color(SeriesColor['SuctionPressure'] + "ff");
      valueAxis_SuctionPressure.renderer.minWidth = 60;
      series[6] = chart.series.push(new am4charts.LineSeries());
      series[6].name = "SuctionPressure";
      series[6].dataFields.dateX = "date_SuctionPressure";
      series[6].dataFields.valueY = "SuctionPressure";
      series[6].yAxis = valueAxis_SuctionPressure;
      series[6].tooltipText = "SuctionPressure: {valueY.value}";
      series[6].propertyFields.stroke = "color";
      series[6].strokeWidth = 2;

      var bullet = series[6].bullets.push(new am4charts.Bullet());
      var square = bullet.createChild(am4core.Rectangle);
      square.width = 2;
      square.height = 2;

      scrollbarXseries = series[6]
    }

    // SuctionTemp
    if (this.store.getState().SelectSuctionTemp){
      let valueAxis_SuctionTemp = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis_SuctionTemp.tooltip.disabled = true;
      valueAxis_SuctionTemp.renderer.labels.template.fill = am4core.color(SeriesColor['SuctionTemp'] + "ff");
      valueAxis_SuctionTemp.renderer.minWidth = 60;
      series[7] = chart.series.push(new am4charts.LineSeries());
      series[7].name = "SuctionTemp";
      series[7].dataFields.dateX = "date_SuctionTemp";
      series[7].dataFields.valueY = "SuctionTemp";
      series[7].yAxis = valueAxis_SuctionTemp;
      series[7].tooltipText = "SuctionTemp: {valueY.value}";
      series[7].propertyFields.stroke = "color";
      series[7].strokeWidth = 2;

      var bullet = series[7].bullets.push(new am4charts.Bullet());
      var square = bullet.createChild(am4core.Rectangle);
      square.width = 2;
      square.height = 2;
      
      scrollbarXseries = series[7]
    }

    // Voltage
    if (this.store.getState().SelectVoltage){
      let valueAxis_Voltage = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis_Voltage.tooltip.disabled = true;
      valueAxis_Voltage.renderer.labels.template.fill = am4core.color(SeriesColor['Voltage'] + "ff");
      valueAxis_Voltage.renderer.minWidth = 60;
      series[8] = chart.series.push(new am4charts.LineSeries());
      series[8].name = "Voltage";
      series[8].dataFields.dateX = "date_Voltage";
      series[8].dataFields.valueY = "Voltage";
      series[8].yAxis = valueAxis_Voltage;
      series[8].tooltipText = "Voltage: {valueY.value}";
      series[8].propertyFields.stroke = "color";
      series[8].strokeWidth = 2;

      var bullet = series[8].bullets.push(new am4charts.Bullet());
      var square = bullet.createChild(am4core.Rectangle);
      square.width = 2;
      square.height = 2;

      scrollbarXseries = series[8]
    }

    if (
      this.store.getState().SelectFTHP ||
      this.store.getState().SelectFTHT ||
      this.store.getState().SelectCurrent ||
      this.store.getState().SelectFlowRate ||
      this.store.getState().SelectFrequency ||
      this.store.getState().SelectMotorTemp ||
      this.store.getState().SelectSuctionPressure ||
      this.store.getState().SelectSuctionTemp ||
      this.store.getState().SelectVoltage
    ) { 
      chart.cursor = new am4charts.XYCursor();
      chart.cursor.xAxis = dateAxis;
  
      let scrollbarX = new am4charts.XYChartScrollbar();
      scrollbarX.series.push(scrollbarXseries);
      chart.scrollbarX = scrollbarX;
    }

  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <div 
          className={classes.spacePaddingSvg}
          ref={this.myRef}
          style={{height: "400px"}}
        >
        </div>
        <div id='container_btn' className='container-btn'></div>
      </div>
    )
  }
}

export default withStyles(styles)(DynaChart)