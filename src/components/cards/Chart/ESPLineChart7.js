//"d3": "^5.16.0",
import React, { Component } from 'react';
import * as d3 from 'd3'; // 5.16.0
import withStyles from '@material-ui/styles/withStyles';
import './ESPLineChart.css'
import moment from 'moment'
import data from './chartData.json'
import dataJson from './data.json'
import predictMock from './predict.json'

const styles = theme => ({
  root: {
    color: "#000000",
    fontSize: '1em',
    border: "2px solid #8c8b8b44",
    borderTop: "4px solid #8c8b8b44",
    borderBottom: "10px solid #8c8b8b44",
    borderRight: "4px solid #8c8b8b44",
    borderLeft: "4px solid #8c8b8b44",
    borderRadius: "3px"
  },
  rootNoBorder: {
    color: "#000000",
    fontSize: '1em',
  },
  spacePaddingSvg: {
    paddingTop: '10px'
  }
});

class DynaChart extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.myRef = React.createRef();
    this.state = {
    }
    this.width = this.props.width
    this.height = this.props.height
    this.x = Object
    this.y = Object
    this.svg = Object
    this.valueline = Object
    this.classselected = ''
    this.valuePadding = []
    this.btnStatus = {
      "FTHP": true,
      "FTHT": true,
      "Current": true,
      "FlowRate": true,
      "Frequency": true,
      "MotorTemp": true,
      "SuctionPressure": true,
      "SuctionTemp": true,
      "Voltage": true,
    }
  }
  componentDidUpdate() {
  }
  componentDidMount(){
    this.LineChartFunction();
  }
  LineChartFunction(){


    
    var data = []
    var count = dataJson.data.length, i = 0 
    while ( count > 0 ) {
      data.push({
        Current: String(dataJson.data[i].Current),
        FTHP: String(dataJson.data[i].FTHP),
        FTHT: String(dataJson.data[i].FTHT),
        FlowRate: String(dataJson.data[i]['Flow rate']),
        Frequency: String(dataJson.data[i]['Frequency']),
        MotorTemp: String(dataJson.data[i]['Motor temp']),
        SuctionPressure: String(dataJson.data[i]['Suction pressure']),
        SuctionTemp: String(dataJson.data[i]['Suction temp']),
        Voltage: String(dataJson.data[i]['Voltage']),

        PDCurrent: "null",
        PDFTHP: "null",
        PDFTHT: "null",
        PDFlowRate: "null",
        PDFrequency: "null",
        PDMotorTemp: "null",
        PDSuctionPressure: "null",
        PDSuctionTemp: "null",
        PDVoltage: "null",
        // 01/28/1986 11:39:13
        date: String(moment(dataJson.data[i].Time, 'ddd, DD MMM YYYY HH:mm:ss').format('DDMMYYYYHHmmss')),
      });
      count--;
      i++;
    }

    var dataPredict = []
    var count = predictMock.data.length, i = 0 
    while ( count > 0 ) {
      data.push({

        Current: "null",
        FTHP: "null",
        FTHT: "null",
        FlowRate: "null",
        Frequency: "null",
        MotorTemp: "null",
        SuctionPressure: "null",
        SuctionTemp: "null",
        Voltage: "null",

        PDCurrent: String(predictMock.data[i]['Current']),
        PDFTHP: String(predictMock.data[i]['FTHP']),
        PDFTHT: String(predictMock.data[i]['FTHT']),
        PDFlowRate: String(predictMock.data[i]['Flow rate']),
        PDFrequency: String(predictMock.data[i]['Frequency']),
        PDMotorTemp: String(predictMock.data[i]['Motor temp']),
        PDSuctionPressure: String(predictMock.data[i]['Suction pressure']),
        PDSuctionTemp: String(predictMock.data[i]['Suction temp']),
        PDVoltage: String(predictMock.data[i]['Voltage']),
        
        date: String(moment(predictMock.data[i].Time, 'ddd, DD MMM YYYY HH:mm:ss').format('DDMMYYYYHHmmss')),
      });
      count--;
      i++;
    }

    var margin = {top: 10, right: 10, bottom: 100, left: 40},
        margin2 = {top: 430, right: 10, bottom: 20, left: 40},
        width = 960 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom,
        height2 = 500 - margin2.top - margin2.bottom;
    
    var color = d3.scale.category10();
    // 10 04 2015 00 00 00
    var parseDate = d3.time.format("%d%m%Y%H%M%S").parse;
    
    var x = d3.time.scale().range([0, width]),
        x2 = d3.time.scale().range([0, width]),
        y = d3.scale.linear().range([height, 0]),
        y2 = d3.scale.linear().range([height2, 0]);
    
    var xAxis = d3.svg.axis().scale(x).orient("bottom"),
        xAxis2 = d3.svg.axis().scale(x2).orient("bottom"),
        yAxis = d3.svg.axis().scale(y).orient("left");
    
    var brush = d3.svg.brush()
        .x(x2)
        .on("brush", brush);
    
    var line = d3.svg.line()
        .defined(function(d) { return !isNaN(d.ESPvalues); })
        .interpolate("cubic")
        .x(function(d) { return x(d.date); })
        .y(function(d) { return y(d.ESPvalues); });
    
    var line2 = d3.svg.line()
        .defined(function(d) { return !isNaN(d.ESPvalues); })
        .interpolate("cubic")
        .x(function(d) {return x2(d.date); })
        .y(function(d) {return y2(d.ESPvalues); });
    
    var svg = d3.select(this.myRef.current).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom);
    
    svg.append("defs").append("clipPath")
        .attr("id", "clip")
      .append("rect")
        .attr("width", width)
        .attr("height", height);
    
    var focus = svg.append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
          
    var context = svg.append("g")
      .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");
    
    // start

    // d3.csv("climate4.csv", function(error, data) {
    
    color.domain(d3.keys(data[0]).filter(function(key) { return key !== "date"; }));
  
    data.forEach(function(d) {
      d.date = parseDate(d.date);
    });

    var sources = color.domain().map(function(name) {
      return {
        name: name,
        values: data.map(function(d) {
          if (d[name] !== 'null'){
            return {date: d.date, ESPvalues: +d[name]};
          }else{
            return {};
          }
        })
      };
    });
    
    console.log(sources)

    x.domain(d3.extent(data, function(d) { return d.date; }));
    y.domain([d3.min(sources, function(c) { return d3.min(c.values, function(v) { return v.ESPvalues; }); }),
              d3.max(sources, function(c) { return d3.max(c.values, function(v) { return v.ESPvalues; }); }) ]);
    x2.domain(x.domain());
    y2.domain(y.domain());
    
    var focuslineGroups = focus.selectAll("g")
        .data(sources)
      .enter().append("g");
      
    var focuslines = focuslineGroups.append("path")
        .attr("class","line")
        .attr("d", function(d) { return line(d.values); })
        .style("stroke", function(d) {return color(d.name);})
        .attr("clip-path", "url(#clip)");
    
    focus.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    focus.append("g")
        .attr("class", "y axis")
        .call(yAxis);
        
    var contextlineGroups = context.selectAll("g")
        .data(sources)
        .enter().append("g");
    
    var contextLines = contextlineGroups.append("path")
        .attr("class", "line")
        .attr("d", function(d) { return line2(d.values); })
        .style("stroke", function(d) {return color(d.name);})
        .attr("clip-path", "url(#clip)");

    context.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height2 + ")")
        .call(xAxis2);

    context.append("g")
        .attr("class", "x brush")
        .call(brush)
      .selectAll("rect")
        .attr("y", -6)
        .attr("height", height2 + 7);

    // draw circle
    var focusCircle = focus.append('g')
        .selectAll('g')
        .data(sources)
        .enter().append('g')
          .append('circle')
          .attr('opacity', 0)
          .attr('r', 4)
          .attr('fill', function(d) { return color(d.name); })


    var focusXLine = focus.append('g')
        .selectAll('g')
        .data(sources)
        .enter().append('g')
          .append('line')
          .attr('class', 'focus-line')
          .style('stroke', function(d) { return color(d.name); })
          .attr('opacity', 0);
          
    var focusYLine = focus.append('g')
        .selectAll('g')
        .data(sources)
        .enter().append('g')
          .append('line')
          .attr('class', 'focus-line')
          .style('stroke', function(d) { return color(d.name); })
          .attr('opacity', 0);

    var bisectDate = d3.bisector(function(d) { return d.date; }).left;     

    focus.append('rect')
      .attr('width', width)
      .attr('height', height)
      .attr('class', 'pane')
      .on('mouseover', function() {
        focusCircle.attr('opacity', 1);
        // focusXLine.attr('opacity', 0.5);
        focusYLine.attr('opacity', 1);
      })
      .on('mouseout', function() {
        focusCircle.attr('opacity', 0);
        // focusXLine.attr('opacity', 0);
        focusYLine.attr('opacity', 0);
      })
      .on('mousemove', mouseMove)

    function tooltipFn () {
      console.log("tooltipFn")
      console.log("X : " + d3.event.pageX)
      console.log("Y : " + d3.event.pageY)
    }

    
    function mouseMove() {
      focusCircle.attr('opacity', 1).call(tooltipFn);
      var x0 = d3.time.minute.round(x.invert(d3.mouse(this)[0])), i = bisectDate(data, x0, 1);
      focusCircle.transition()
        .duration(0)
        .delay(0)
        .ease('bounce')
        .attr('cx', x(x0))
        .attr('cy', function(d) { 
          if (!isNaN(d.values[i].ESPvalues)){
            return y(d.values[i].ESPvalues); 
          }

        });
          
        focusYLine.transition()
          .duration(0)
          .delay(0)
          .ease('bounce')
          .attr('x1', x(x0))
          .attr('x2', x(x0))
          .attr('y1', height)
          .attr('y2', function(d) { 
            if (!isNaN(d.values[i].ESPvalues)){
              return y(d.values[i].ESPvalues); 
            }
           });
    }        
    // });
    // end 


    function brush() {
      x.domain(brush.empty() ? x2.domain() : brush.extent());
      focus.selectAll("path.line").attr("d",  function(d) {return line(d.values)});
      focus.select(".x.axis").call(xAxis);
      focus.select(".y.axis").call(yAxis);
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <div 
          className={classes.spacePaddingSvg}
          ref={this.myRef}
        >
        </div>
        <div id='container_btn' className='container-btn'></div>
      </div>
    )
  }
}

export default withStyles(styles)(DynaChart)