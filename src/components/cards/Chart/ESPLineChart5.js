//"d3": "^5.16.0",
import React, { Component } from 'react';
import * as d3 from 'd3'; // 5.16.0
import withStyles from '@material-ui/styles/withStyles';
import './ESPLineChart.css'
import moment from 'moment'
import data from './data.json'
import predictMock from './predict.json'
const styles = theme => ({
  root: {
    color: "#000000",
    fontSize: '1em',
    border: "2px solid #8c8b8b44",
    borderTop: "4px solid #8c8b8b44",
    borderBottom: "10px solid #8c8b8b44",
    borderRight: "4px solid #8c8b8b44",
    borderLeft: "4px solid #8c8b8b44",
    borderRadius: "3px"
  },
  rootNoBorder: {
    color: "#000000",
    fontSize: '1em',
  },
  spacePaddingSvg: {
    paddingTop: '10px'
  }
});

class DynaChart extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.myRef = React.createRef();
    this.state = {
    }
    this.width = this.props.width
    this.height = this.props.height
    this.x = Object
    this.y = Object
    this.svg = Object
    this.valueline = Object
    this.classselected = ''
    this.valuePadding = []
    this.btnStatus = {
      "FTHP": true,
      "FTHT": true,
      "Current": true,
      "FlowRate": true,
      "Frequency": true,
      "MotorTemp": true,
      "SuctionPressure": true,
      "SuctionTemp": true,
      "Voltage": true,
    }
  }
  componentDidUpdate() {
  }
  componentDidMount(){
    this.LineChartFunction();
  }
  LineChartFunction(){
    var margin = {top: 20, right: 20, bottom: 90, left: 50},
    margin2 = {top: 230, right: 20, bottom: 30, left: 50},
    width = 1400 - margin.left - margin.right,
    height = 300 - margin.top - margin.bottom,
    height2 = 300 - margin2.top - margin2.bottom;
	
	//add svg with margin !important
	//this is svg is actually group
	var svg = d3.select(this.myRef.current).append("svg")
				.attr("width",width+margin.left+margin.right)
				.attr("height",height+margin.top+margin.bottom);
		
	var focus = svg.append("g")  //add group to leave margin for axis
				.attr("transform","translate("+margin.left+","+margin.top+")");
	var context = svg.append("g")
				.attr("transform","translate("+margin2.left+","+margin2.top+")");
  
        
        
	var dataset = [[5, 20], [480, 90], [250, 50], [100, 33], [330, 95],
                [410, 12], [475, 44], [25, 67], [85, 21], [220, 88]];
  dataset = []
  data.data.forEach((item ,index) => {
    // [x, y]
    dataset.push([Number(moment.utc(new Date(item.Time)).format("x")), Number(item.FTHT)])
  })

	//for each d, d[0] is the first num, d[1] is the second num
	//set y scale
	var yScale = d3.scaleLinear().range([0,height]).domain([
    d3.max(dataset,function(d){return d[1];}) + 10,
    d3.min(dataset,function(d){return d[1];}) - 10,
  ]);
	//add x axis
	var xScale = d3.scaleLinear().range([0,width]).domain([
    d3.min(dataset, function(d){return d[0];}),
    d3.max(dataset, function(d){return d[0];})
  ]);//scaleBand is used for  bar chart
	var xScale2 = d3.scaleLinear().range([
    0,
    width
  ]).domain([
    d3.min(dataset, function(d){return d[0];}),
    d3.max(dataset, function(d){return d[0];})
  ]);
	var yScale2 = d3.scaleLinear().range([0,height2]).domain([
    d3.max(dataset,function(d){return d[1];}) + 10,
    d3.min(dataset,function(d){return d[1];}) - 10,
  ]);
	//sort x
	dataset.sort(function(a,b){
		if(a[0]<b[0]){
			return -1;
		}
		else{
			return 1;
		}
	})
	var line = d3.line()
				.x(function(d){return xScale(d[0]);})
				.y(function(d){return yScale(d[1]);})
				.curve(d3.curveBasis);//default is d3.curveLinear
  focus.append("path").attr("class","line").attr("d",line(dataset));
  
  // add point in chart
  // focus.selectAll("myCircles")
  //   .data(dataset)
  //   .enter()
  //   .append("circle")
  //     .attr("fill", "#ff00ff44")
  //     .attr("stroke", "none")
  //     .attr("cx", function(d) { return xScale(d[0]); })
  //     .attr("cy", function(d) { return yScale(d[1]); })
  //     .attr("r", 3)
  //     .on("mouseover", (d, i) => {}) 
      
	var line2 = d3.line()
			.x(function(d){return xScale2(d[0]);})
			.y(function(d){return yScale2(d[1]);})
			.curve(d3.curveBasis);//default is d3.curveLinear
	context.append("path").attr("class","line").attr("d",line2(dataset));
		
	//add x and y axis
	var yAxis = d3.axisLeft(yScale).tickSize(-width);
	var yAxisGroup = focus.append("g").call(yAxis);
	

	var xAxis = d3.axisBottom(xScale).tickSize(-height).tickFormat((d) => {
          var format = d3.timeFormat("%Y-%m-%d %H:%M:%S")
          return format(d)
      }); /*.tickFormat("");remove tick label*/
	var xAxisGroup = focus.append("g").call(xAxis).attr("transform", "translate(0,"+height+")");
	
		
	var xAxis2 = d3.axisBottom(xScale2).tickFormat((d) => {
          var format = d3.timeFormat("%Y-%m-%d %H:%M:%S")
          return format(d)
      });//no need to create grid
	var xAxisGroup2 = context.append("g").call(xAxis2).attr("transform","translate(0,"+height2+")")	

		
		
	//add zoom
	var zoom = d3.zoom()
				.scaleExtent([1,Infinity])// <1 means can resize smaller than  original size
				.translateExtent([[0,0],[width,height]])
				.extent([[0,0],[width,height]])//view point size
				.on("zoom",zoomed);
	svg.append("rect")
	  .attr("class","zoom")
      .attr("width", width)
      .attr("height", height)
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
      .call(zoom);
		
	//add brush
	//Brush must be added in a group
	var brush = d3.brushX()
				.extent([[0,0],[width,height2]])//(x0,y0)  (x1,y1)
				.on("brush end",brushed);//when mouse up, move the selection to the exact tick //start(mouse down), brush(mouse move), end(mouse up)
		
	context.append("g")
		.attr("class","brush")
		.call(brush)
		.call(brush.move,xScale2.range());
		
		
	function zoomed(){	
		if(d3.event.sourceEvent && d3.event.sourceEvent.type === "brush") return; // ignore zoom-by-brush
		xScale.domain(d3.event.transform.rescaleX(xScale2).domain());
		focus.select(".line").attr("d",line(dataset));
		xAxisGroup.call(xAxis);//rescale x
		//brush area
		context.select(".brush").call(brush.move, [xScale2(d3.event.transform.rescaleX(xScale2).domain()[0]),xScale2(d3.event.transform.rescaleX(xScale2).domain()[1])]);
	}
	
	function brushed(){
    if(d3.event.sourceEvent && d3.event.sourceEvent.type === "zoom") return; // ignore brush-by-zoom
    if (d3.event.selection !== null){
      xScale.domain([xScale2.invert(d3.event.selection[0]),xScale2.invert(d3.event.selection[1])]);
      focus.select(".line").attr("d",line(dataset));
      xAxisGroup.call(xAxis);//rescale x
    }
	}
		
	//add clip path to the svg
	svg.append("defs").append("clipPath").attr("id","clip")
	.append("rect").attr("width",width).attr("height",height);
	focus.select(".line").attr("clip-path","url(#clip)");
            	
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <div 
          className={classes.spacePaddingSvg}
          ref={this.myRef}
        >
        </div>
        <div id='container_btn' className='container-btn'></div>
      </div>
    )
  }
}

export default withStyles(styles)(DynaChart)