import React, { Component } from 'react';
import * as d3 from 'd3';
import withStyles from '@material-ui/styles/withStyles';
import './ESPLineChart.css'
import moment from 'moment'
import dataJson from './data.json'
import predictMock from './predict.json'
const styles = theme => ({
  root: {
    color: "#000000",
    fontSize: '1em',
    border: "2px solid #8c8b8b44",
    borderTop: "4px solid #8c8b8b44",
    borderBottom: "10px solid #8c8b8b44",
    borderRight: "4px solid #8c8b8b44",
    borderLeft: "4px solid #8c8b8b44",
    borderRadius: "3px"
  },
  rootNoBorder: {
    color: "#000000",
    fontSize: '1em',
  },
  spacePaddingSvg: {
    paddingTop: '10px'
  }
});

class DynaChart extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.myRef = React.createRef();
    this.state = {
    }
    this.width = this.props.width
    this.height = this.props.height
    this.x = Object
    this.y = Object
    this.svg = Object
    this.valueline = Object
    this.classselected = ''
    this.valuePadding = []
    this.btnStatus = {
      "FTHP": true,
      "FTHT": true,
      "Current": true,
      "FlowRate": true,
      "Frequency": true,
      "MotorTemp": true,
      "SuctionPressure": true,
      "SuctionTemp": true,
      "Voltage": true,
    }
  }
  componentDidUpdate() {
  }
  componentDidMount(){
    this.LineChartFunction();
  }
  LineChartFunction(){
    /*
    * Dummy data
    */

    var data = []
    var count = dataJson.data.length, i = 0 
    while ( count > 0 ) {
      data.push({
        Current: Number(dataJson.data[i].Current),
        FTHP: Number(dataJson.data[i].FTHP),
        FTHT: Number(dataJson.data[i].FTHT),
        Current: Number(dataJson.data[i]['Current']),
        FlowRate: Number(dataJson.data[i]['Flow rate']),
        Frequency: Number(dataJson.data[i]['Frequency']),
        MotorTemp: Number(dataJson.data[i]['Motor temp']),
        SuctionPressure: Number(dataJson.data[i]['Suction pressure']),
        SuctionTemp: Number(dataJson.data[i]['Suction temp']),
        Voltage: Number(dataJson.data[i]['Voltage']),
        date: new Date(dataJson.data[i].Time),
      });
      count--;
      i++;
    }

    var dataPredict = []
    var count = predictMock.data.length, i = 0 
    while ( count > 0 ) {
      dataPredict.push({
        PDFTHP: Number(predictMock.data[i].FTHP),
        PDFTHT: Number(predictMock.data[i].FTHT),
        PDCurrent: Number(predictMock.data[i]['Current']),
        PDFlowRate: Number(predictMock.data[i]['Flow rate']),
        PDFrequency: Number(predictMock.data[i]['Frequency']),
        PDMotorTemp: Number(predictMock.data[i]['Motor temp']),
        PDSuctionPressure: Number(predictMock.data[i]['Suction pressure']),
        PDSuctionTemp: Number(predictMock.data[i]['Suction temp']),
        PDVoltage: Number(predictMock.data[i]['Voltage']),
        date: new Date(predictMock.data[i].Time)
      });
      count--;
      i++;
    }
    
    var margin = { top: 2, right: 2, bottom: 74, left: 45},
        margin2 = { top: 250, right: 2, bottom: 15, left: 45},
        width = 1100 - margin.left - margin.right,
        height = 300 - margin.top - margin.bottom,
        height2 = 300 - margin2.top - margin2.bottom;
        
    /*
    * Initial setup/color/stats
    */
    
    var svg = d3.select('#favorite-content') 
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom);
      
    var color = d3.scale.category10()
      .domain(d3.keys(data[0]).filter(function(key) { return key !== 'date'; }));
      
    var bisectDate = d3.bisector(function(d) { return d.date; }).left;

    var stats = color.domain().map(function(name) {
      return {
        name: name,
        values: data.map(function(d) {
          return { date: d.date, stat: d[name] };
        })
      };
    });
    
    var color2 = d3.scale.category10()
      .domain(d3.keys(dataPredict[0]).filter(function(key) { return key !== 'date'; }));
    var bisectDate2 = d3.bisector(function(d) { return d.date; }).left;
    var stats2 = color2.domain().map(function(name) {
      return {
        name: name,
        values: dataPredict.map(function(d) {
          return { date: d.date, stat: d[name] };
        })
      };
    });

    console.log(stats2)
    /*
    * Scales
    */

    var x = d3.time.scale()
      .range([0, width])
      .domain([data[0].date, dataPredict[dataPredict.length-1].date]);

    var x2 = d3.time.scale()
      .range([0, width])
      .domain(x.domain());

    var y = d3.scale.linear()
      .range([height, 0])
      .domain([
        d3.min(stats, function(s) { return d3.min(s.values, function(v) { return v.stat; }); }) - 30,
        d3.max(stats, function(s) { return d3.max(s.values, function(v) { return v.stat; }); }) + 30,
      ]);
    
    var y2 = d3.scale.linear()
      .range([height2, 0])
      .domain([y.domain()[0] - 30, y.domain()[1] + 30]);
      
    /*
    * Axises
    */
      
    var xAxis = d3.svg.axis()
      .scale(x)
      .orient('bottom')
      .ticks(3);
      
    var xAxis2 = d3.svg.axis()
      .scale(x2)
      .orient('bottom')
      .ticks(3);
      
    var yAxis = d3.svg.axis()
      .scale(y)
      .orient('left');
      
    /*
    * Brush and Zoom
    */
      
    var brush = d3.svg.brush()
      .x(x2)
      .on('brush', brushed);
    var zoom = d3.behavior.zoom()
      .on('zoom', draw)
      .x(x);
      
    zoom.x(x);
      
    /*
    * Focus/Context lines
    */
      
    var line = d3.svg.line()
      .x(function(d) { return x(d.date); })
      .y(function(d) { return y(d.stat); });
      
    var line2 = d3.svg.line()
      .interpolate('basis')
      .x(function(d) { return x2(d.date); })
      .y(function(d) { return y2(d.stat); });
      
    svg.append('defs').append('clipPath')
      .attr('id', 'clip')
      .append('rect')
      .attr('width', width)
      .attr('height', height);
      
    var focus = svg.append('g')
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    var context = svg.append('g')
      .attr('transform', 'translate(' + margin2.left + ',' + margin2.top + ')');
      
    /*
    * Place Axises on Focus and Context graphs
    */
      
    focus.append('g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0,' + height + ')')
      .call(xAxis);
      
      
    context.append('g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0,' + height2 + ')')
      .call(xAxis2);
      
    focus.append('g')
      .attr('class', 'y axis')
      .call(yAxis)
      .append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', 0 - margin.left)
      .attr('x', 0 - (height / 2))
      .attr('dy', '.71em')
      .attr('text-anchor', 'middle')
      .attr('Active Users');
      
    var focusStat = focus.selectAll('.favorite-stat')
      .data(stats)
      .enter().append('g')
        .attr('class', 'stat');

    focusStat.append('path')
      .attr('class', 'focus-path')
      .attr('d', function(d) { return line(d.values); })
      .style('stroke', function(d) { return color(d.name); });


    var focusStat2 = focus.selectAll('.favorite-stat')
      .data(stats2)
      .enter().append('g')
        .attr('class', 'stat');

    focusStat2.append('path')
      .attr('class', 'focus-path')
      .attr('d', function(d) { return line(d.values); })
      .style('stroke', function(d) { return color(d.name); });

    var focusCircle = focus.append('g')
      .selectAll('g')
      .data(stats)
      .enter().append('g')
        .append('circle')
        .attr('opacity', 0)
        .attr('r', 4)
        .attr('fill', function(d) { return color(d.name); })

    var focusXLine = focus.append('g')
      .selectAll('g')
      .data(stats)
      .enter().append('g')
        .append('line')
        .attr('class', 'focus-line')
        .style('stroke', function(d) { return color(d.name); })
        .attr('opacity', 0);

    var focusYLine = focus.append('g')
      .selectAll('g')
      .data(stats)
      .enter().append('g')
        .append('line')
        .attr('class', 'focus-line')
        .style('stroke', function(d) { return color(d.name); })
        .attr('opacity', 0);
    
    // // mouse scroll
    focus.append('rect')
      .attr('width', width)
      .attr('height', height)
      .attr('class', 'pane')
      .on('mouseover', function() {
        focusCircle.attr('opacity', 1);
        focusXLine.attr('opacity', 0.5);
        focusYLine.attr('opacity', 0.5);
      })
      .on('mouseout', function() {
        focusCircle.attr('opacity', 0);
        focusXLine.attr('opacity', 0);
        focusYLine.attr('opacity', 0);
      })
      .on('mousemove', mouseMove)
      // .call(zoom);
      
    var contextStat = context.selectAll('.context-stat')
      .data(stats)
      .enter().append('g')
        .attr('class', 'stat');
        
    contextStat.append('path')
      .attr('class', 'context-path')
      .attr('d', function(d) { return line2(d.values); })
      .style('stroke', function(d) { return color(d.name); });
      
    context.append('g')
      .attr('class', 'x brush')
      .call(brush)
      .selectAll('rect')
        .attr('y', -6)
        .attr('height', height2 + 7);

    function draw() {
      focus.selectAll('.focus-path').attr('d', function(d) { return line(d.values); });
      focus.selectAll('.context-path').attr('d', function(d) { return line2(d.values); });
      focus.select('.x.axis').call(xAxis);
      brush.extent(x.domain());
      svg.select('.brush').call(brush);
    }

    function brushed() {
      console.log(brush.empty())
      x.domain(brush.empty() ? x2.domain() : brush.extent());
      focus.selectAll('.focus-path').attr('d', function(d) { return line(d.values); });
      focus.select('.x.axis').call(xAxis);
      zoom.x(x);
    }

    function tooltipFn () {
      console.log("tooltipFn")
      console.log("X : " + d3.event.pageX)
      console.log("Y : " + d3.event.pageY)
    }

    function mouseMove() {
      focusCircle.attr('opacity', 1).call(tooltipFn);
      var x0 = d3.time.minute.round(x.invert(d3.mouse(this)[0])), i = bisectDate(data, x0, 1);
      console.log(x(x0))
      focusCircle.transition()
        .duration(0)
        .delay(0)
        .ease('bounce')
        .attr('cx', x(x0))
        .attr('cy', function(d) { console.log(d); return y(d.values[i].stat); });
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <div 
          className={classes.spacePaddingSvg}
          ref={this.myRef}
        >
        </div>
        <svg id="favorite-content"></svg>
        <div id='container_btn' className='container-btn'></div>
      </div>
    )
  }
}

export default withStyles(styles)(DynaChart)