import React, { Component } from 'react';
import * as d3 from 'd3';

// import { setdchart1 } from './../actions';

class chart1 extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.myRef = React.createRef();
    this.svg = Object;
    this.x = Object
    this.y = Object
    this.data1 = Array
    this.xAxis = Object
    this.yAxis = Object
  }


  // componentWillReceiveProps (nextProps) {
  //   console.log("componentWillReceiveProps")
  //   this.update(this.props.dchartdata)   
  // }
  componentDidUpdate() {
    console.log("componentDidUpdate")
    this.update(this.props.dchartdata)   
  }
  componentDidMount(){
    this.data1 = [
      {ser1: 0, ser2: 0}
    ];
    this.updatelinechart()
  }
  updatelinechart() {

    // set the dimensions and margins of the graph
    var margin = {top: 10, right: 30, bottom: 30, left: 50},
      width = 260 - margin.left - margin.right,
      height = 200 - margin.top - margin.bottom;

    // append the svg object to the body of the page
    this.svg = d3.select(this.myRef.current)
      .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("g")
        .attr("transform",
              "translate(" + margin.left + "," + margin.top + ")");

    // Initialise a X axis:
    this.x = d3.scaleLinear().range([0,width]);
    this.xAxis = d3.axisBottom().scale(this.x);
    this.svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .attr("class","myXaxis")

    // Initialize an Y axis
    this.y = d3.scaleLinear().range([height, 0]);
    this.yAxis = d3.axisLeft().scale(this.y);
    this.svg.append("g")
      .attr("class","myYaxis")

    this.update(this.data1)
  }

  update = (data) => {
    var that = this
    that.x.domain([0, d3.max(data, function(d) { return d.ser1 }) ]);
    that.svg.selectAll(".myXaxis").transition()
      .duration(500)
      .call(this.xAxis);

      that.y.domain([0, d3.max(data, function(d) { return d.ser2  }) ]);
    that.svg.selectAll(".myYaxis")
      .transition()
      .duration(500)
      .call(this.yAxis);

    var u = that.svg.selectAll(".lineTest")
      .data([data], function(d){ return d.ser1 });

    u
      .enter()
      .append("path")
      .attr("class","lineTest")
      .merge(u)
      .transition()
      .duration(500)
      .attr("d", d3.line()
        .x(function(d) { return that.x(d.ser1); })
        .y(function(d) { return that.y(d.ser2); }))
        .attr("fill", "none")
        .attr("stroke", "steelblue")
        .attr("stroke-width", 2.5)
  }

  LineChartFunction(){
      var width = 800, height = 600
      
      var data = []
      var tttt = this.props.dchartdata

      tttt.forEach((item, index, arr) => {
        data.push({
            "x": Number(item.x),
            "y": Number(item.y),
        })
      })

      var  max = () => { 
        var temp = Math.max.apply(Math, data.map(function(o) { 
            return o.y;  
        })); 
        return temp
      }   
      var  min = () => { 
        var temp = Math.min.apply(Math, data.map(function(o) { 
            return o.y;  
        })); 
        return temp
      } 

      var margin = {
        top: 10,
        right: 20,
        bottom: 50,
        left: 70
      }

      var padding_top = 30

      // var div = d3.select("body").append("div")
      //     .attr("class", "tooltip")
      //     .style("opacity", 0);

      var default_width = width - margin.left - margin.right;
      // 600 - 40 - 20 = 540
      var default_height = height - margin.top - margin.bottom;
      // 400 - 20 - 30 = 350 
      var default_ratio = default_width / default_height;
      // 540 / 350 = 1.542857142857143

      const set_size = () => {
          var w, h
          var current_width = window.innerWidth;
          
          var current_height = window.innerHeight;

          var current_ratio = current_width / current_height;

          // desktop
          if (current_ratio > default_ratio) {
              h = default_height;
              w = default_width;

          // mobile
          } else {
              margin.left = 40
              w = current_width - 40;
              h = w / default_ratio;
          }
          // Set new width and height based on graph dimensions
          width = w - 50 - margin.right;
          height = h - margin.top - margin.bottom;
      }

      set_size();
      //end responsive graph cod

      // set the ranges
      var x = d3.scaleTime().range([0, width]);
      var y = d3.scaleLinear().range([height, 0])

      x.domain([d3.min(data, function (d) {
          return d.x;
      }), d3.max(data, function (d) {
          return d.x;
      })]);
      y.domain([d3.min(data, function (d) {
          return d.y;
      }), d3.max(data, function (d) {
          return d.y;
      })]);

      // define the line
      var valueline = d3.line()
          .curve(d3.curveCatmullRom.alpha(1))
          .x(function (d) {
              return x(d.x);
          })
          .y(function (d) {
              return y(d.y);
          });

        // append the svg object to the body of the page
        
        // var svg = d3.select(this.myRef.current).remove()
        var svg = d3.select(this.myRef.current).append("svg")
            // .attr("style", "padding-top: "+padding_top+";")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom + padding_top)
            .attr("viewBox", "0 0 "+(width + margin.left + margin.right + 40)+" " + (height + margin.top + margin.bottom + padding_top))
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + (margin.top + 40) + ")")
          
        // title line chart
        svg.append("text")
            .attr('y', -40)
            .attr("class", "title-line-chart")
            .attr('x', (height / 2) + 60)
            .attr('fill', "#000000")
            .attr("style", "font-size: 1.5em")
            .attr("text-anchor","middle")
            .text("hello")

        // sub title line chart
        var subtitle = "hello"
        svg.append("text")
            .attr('y', -15)
            .attr("class", "sub-title")
            .attr('x', (height / 2) + 60)
            .attr('fill', "#000000")
            .attr("text-anchor","middle")
            .text(subtitle)

        // Add the trendline
        svg.append("path")
            .data([data])
            .attr("class", "line")
            .attr("d", valueline)
            .attr("stroke", "#29abe2")
            .attr("stroke-width", 2)
            .attr("fill", "#FFFFFF")

        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x)
            .ticks(3)
            .tickFormat((d) => {
                return d3.format("")(d)
            }));
        
        var scale = d3.scaleLinear()
            .domain([min() , max()])
            .range([height, 0]);

        svg.append("text")
            .attr("text-anchor", "end")
            .attr("x", width / 2)
            .attr("y", height + margin.top + 20)
            .text("Positions");

        // Y
        svg.append("g")
            .call(d3.axisLeft(y).scale(scale)
            .ticks(6)
            .tickFormat(function (d) {
                return d3.format("")(d)
            }));
        svg.append("text")
            .attr("text-anchor", "end")
            .attr("transform", "rotate(-90)")
            .attr("y", -(margin.left)+20)
            .attr("x", -(height / 2) + 20)
            .text("Loads")
  }

  render() {
    return (
      <div 
        ref={this.myRef}
      >
      </div>
    )
  }
}

export default chart1