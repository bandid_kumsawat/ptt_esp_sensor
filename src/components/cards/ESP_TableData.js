import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import moment from 'moment'
import _ from 'underscore'

const columns = [
  {
    id: 'Time',
    label: 'Time',
    minWidth: 240,
    align: 'center',
    format: (value) => moment(value).format("LLL")
  },
  {
    id: 'Current',
    label: 'Current',
    minWidth: 170,
    align: 'right'
  },
  {
    id: 'FTHP',
    label: 'FTHP',
    minWidth: 170,
    align: 'right'
  },
  {
    id: 'FTHT',
    label: 'FTHT',
    minWidth: 170,
    align: 'right'
  },
  {
    id: 'Flow rate',
    label: 'Flow rate',
    minWidth: 170,
    align: 'right'
  },
  {
    id: 'Frequency',
    label: 'Frequency',
    minWidth: 170,
    align: 'right'
  },
  {
    id: 'Motor temp',
    label: 'Motor temp',
    minWidth: 170,
    align: 'right'
  },
  {
    id: 'Suction pressure',
    label: 'Suction pressure',
    minWidth: 170,
    align: 'right'
  },
  {
    id: 'Suction temp',
    label: 'Suction temp',
    minWidth: 170,
    align: 'right'
  },
  {
    id: 'Voltage',
    label: 'Voltage',
    minWidth: 170,
    align: 'right'
  },
];

function createData(name, code, population, size) {
  const density = population / size;
  return { name, code, population, size, density };
}

const rows = [
  createData('India', 'IN', 1324171354, 3287263),
  createData('China', 'CN', 1403500365, 9596961),
  createData('Italy', 'IT', 60483973, 301340),
  createData('United States', 'US', 327167434, 9833520),
  createData('Canada', 'CA', 37602103, 9984670),
  createData('Australia', 'AU', 25475400, 7692024),
  createData('Germany', 'DE', 83019200, 357578),
  createData('Ireland', 'IE', 4857000, 70273),
  createData('Mexico', 'MX', 126577691, 1972550),
  createData('Japan', 'JP', 126317000, 377973),
  createData('France', 'FR', 67022000, 640679),
  createData('United Kingdom', 'GB', 67545757, 242495),
  createData('Russia', 'RU', 146793744, 17098246),
  createData('Nigeria', 'NG', 200962417, 923768),
  createData('Brazil', 'BR', 210147125, 8515767),
];

const useStyles = makeStyles({
  root: {
    width: '100%',
    paddingTop: '20px'
  },
  container: {
    maxHeight: 440,
  },
  title:{
    fontSize: "1.3em",
    paddingTop: '5px',
    paddingBottom: '10px',
    fontWeight: 600,
    // display: 'flex',
    // flexDirection: 'row',
    margin: '0px auto 0px auto',
    textAlign: "center",
  },
  flex: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '20px 0 20px 0'
  },
  currentBox: {
    border: '1px solid #000',
    width: '15px',
    height: '15px',
    borderRadius: "10px",
    backgroundColor: '#CCF2FF',
    marginRight: "10px"
  },
  predictBox: {
    border: '1px solid #000',
    width: '15px',
    height: '15px',
    borderRadius: "10px",
    backgroundColor: '#F4DEE2',
    marginLeft: "10px"
  },
  currentText: {
    marginRight: "10px",
    fontSize: '1em',
    fontWeight: '400'
  },
  predictText: {
    marginLeft: "10px",
    fontSize: '1em',
    fontWeight: '400'
  }
});

export default function StickyHeadTable(props) {
  var rows = props.data
  _.each(rows, function(data){
    data.Time = moment(data.Time).format("LLLL")
  });
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <div className={classes.root}>
      <div className={classes.title}>
        Table Sensor Data
        <div className={classes.flex}>
          <div className={classes.currentBox}></div><div className={classes.currentText}>Current</div>
          <div className={classes.predictBox}></div><div className={classes.predictText}>Predict</div>
        </div>
      </div>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column, index) => (
                <TableCell
                  key={index}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, index) => {
              console.log(row["Time"])
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={index}>
                  {columns.map((column, index) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={index} align={column.align} style={
                        ((row['tag'] === "predict") ? {backgroundColor: "#F4DEE2"} : {backgroundColor: "#CCF2FF"})
                        }>
                        {column.format && typeof value === 'number' ? column.format(value) : value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </div>
  );
}
