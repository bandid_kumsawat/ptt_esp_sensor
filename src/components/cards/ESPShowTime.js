import React, { Component } from 'react';
import withStyles from '@material-ui/styles/withStyles';
// import Grid from "@material-ui/core/Grid";
import moment from 'moment'
// More Function
// import API from './../../api/server'
// import { 
//   selectfthp, 
//   selectftht, 
//   selectcurrent, 
//   selectflowrate, 
//   selectmotortemp, 
//   selectsuctionpressure, 
//   selectsuctiontemp, 
//   selectvoltage, 
//   selectfrequency, 
//   esprentderchart
// } from './../../actions';

const styles = theme => ({
  root: {
    maxWidth: '100%',
    display:'flex',
    justifyContent: 'flex-end',
    paddingTop: '20px'
    // flexGrow: 1,
  },
  StateDate: {
    border: '1px solid #99AAB5',
    backgroundColor: '#C1CCD2',
    padding: '2px'
  },
  To: {
    paddingTop: '2px',
    paddingRight: '10px',
    paddingLeft: '10px'
  },
  From: {
    paddingTop: '2px',
    paddingRight: '10px',
    paddingLeft: '10px'
  }
});

class ESPShowTime extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.state = {

    }
  }
  async componentDidMount() {

  }
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <div className={classes.From}>Device Name</div>
        <div className={classes.StateDate}>{
          'LKU-Z11'
        }</div>
        <div className={classes.From}>From</div>
        <div className={classes.StateDate}>{
          ((this.store.getState().ESPShowTime[0] !== undefined) ? moment(this.store.getState().ESPShowTime[0][0]).format("LLLL") : "")
        }</div>
        <div className={classes.To}>To</div>
        <div className={classes.StateDate}>{
          ((this.store.getState().ESPShowTime[0] !== undefined) ? moment(this.store.getState().ESPShowTime[0][1]).format("LLLL") : "")
        }</div>
      </div>
    )
  }
}

export default withStyles(styles)(ESPShowTime);
