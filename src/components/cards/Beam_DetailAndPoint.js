import React, { Component } from 'react';
import withStyles from '@material-ui/styles/withStyles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { Grid } from '@material-ui/core';
import DynaChart from './Chart/DynaChart'
import PointChart from './Chart/PointChart'
import moment from 'moment'
import "./Beam_DetailAndPoint.css"

import { store } from './../../store'

// import { addwellselect } from './../../actions';

import API from './../../api/server'

const styles = theme => ({
  root: {
    maxWidth: '100%',
    flexGrow: 1,
  },
  hieghtlightchart: {
    borderTop: "3px solid #0a88be"
  },
  WellName: {
    fontSize: "18px",
    fontWeight: "500",
    marginLeft: "20px",
    paddingBotton: "20px"
  },
  WellInformation: {
    fontSize: "20px",
    fontWeight: "500",
    marginLeft: "20px",
    paddingBotton: "20px"
  },
  WellClusterChart: {
    padding: "0px 30% 12px 30%",
    textAlign: "center",
    fontSize: "20px",
    fontWeight: "500",
  },
  TableInformation: {
    fontSize: "16px",
    width: "100%",
    paddingRight: '20%',
    paddingLeft: '20%'
  },
  TableRow:{
    // marginBottom: "6px !important",
    padding: "5px 0px 1px 10px"
  },
  ChartPointInformation: {
    padding: "0 30% 0 10%"
  },
  ChartInformation: {
    padding: "2% 00% 0 00%"
  },
  TableDBcolor: {
    backgroundColor: "#B2ACAF",
    borderColor: '#ff00ff'
  }
});

class BeamViewSelect extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.state = {
      localS: null,
      pointdata: []
    }
  }
  async componentDidMount() {
    const res = await API().post('/pointchart', {
      "WellName": ((localStorage.getItem('device') !== null) ? localStorage.getItem('device') : ""),
    })
    this.setState({pointdata: res.data})
    this.setState({localS: JSON.parse(localStorage.getItem('select_Dynachart'))})
    // console.log(this.state.pointdata)
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Card className={classes.hieghtlightchart}>
          <CardContent>
            <Grid container spacing={3}>
            <Grid item sm={6}>
              <Grid item sm={12}>

              <Grid container spacing={3}>
                <Grid item sm={12}>
                  <div className={classes.WellInformation}><center>Well Information</center></div>
                </Grid>
              </Grid>


              <Grid container spacing={3}>
                <Grid item sm={12} className={classes.TableInformation}>
                  
                  <table id="table-show-detail-well">
                    <tbody>
                      <tr>
                        <td style={{textAlign: "left"}}>Well Name</td>
                          <td className='param-show-well'>
                          {
                            ((this.store.getState().selectWell[0] === undefined) ? "" : this.store.getState().selectWell[0].WellName)
                            // ((this.state.localS !== null) ? JSON.parse(localStorage.getItem('select_Dynachart')).WellName : "null")
                          }
                        </td>
                      </tr>
                      <tr>
                        <td style={{textAlign: "left"}}>Card Report Date</td>
                        <td className='param-show-well'>
                          {
                            ((this.store.getState().selectWell[0] === undefined) ? "" : moment(this.store.getState().selectWell[0].CardDateTime).format("DD-MMMM-YYYY HH:mm:ss"))
                            // ((this.state.localS !== null) ? moment(JSON.parse(localStorage.getItem('select_Dynachart')).CardDateTime).format("DD-MMMM-YYYY HH:mm:ss") : "null")
                          }
                        </td>
                      </tr>
                      <tr>
                        <td style={{textAlign: "left"}}>Card Report Type</td>
                        <td className='param-show-well'>
                          {
                            ((this.store.getState().selectWell[0] === undefined) ? "" : this.store.getState().selectWell[0].CardType)
                            // ((this.state.localS !== null) ? JSON.parse(localStorage.getItem('select_Dynachart')).CardType : "null")
                          }
                        </td>
                      </tr>
                    </tbody>
                  </table>
{/*         
                <table className={classes.TableInformation}>
                  <tbody>
                    <tr className={classes.TableDBcolor}>
                      <td style={{textAlign: "left"}}>
                        <div className={classes.TableRow}>
                          Well Name
                        </div>
                      </td>
                      <td style={{textAlign: "right"}}>
                        <div className={classes.TableRow}>
                          {
                            ((this.state.localS !== null) ? JSON.parse(localStorage.getItem('select_Dynachart')).WellName : "")
                          }
                        </div>
                      </td>
                    </tr>
                    <tr className={classes.TableDBcolor}>
                      <td style={{textAlign: "left"}} bordercolor='red'>
                        <div className={classes.TableRow}>
                          Card Report Date
                        </div>
                      </td>
                      <td style={{textAlign: "right"}}>
                        <div className={classes.TableRow}>
                          {
                            ((this.state.localS !== null) ? JSON.parse(localStorage.getItem('select_Dynachart')).CardDateTime : "")
                          }
                        </div>
                      </td>
                    </tr>
                    <tr className={classes.TableDBcolor}>
                      <td style={{textAlign: "left"}}>
                        <div className={classes.TableRow}>
                          Card Report Type
                        </div>
                      </td>
                      <td style={{textAlign: "right"}}>
                        <div className={classes.TableRow}>
                          {
                            ((this.state.localS !== null) ? JSON.parse(localStorage.getItem('select_Dynachart')).CardType : "")
                          }
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table> */}

                </Grid>
              </Grid>
              
              </Grid>
                <hr></hr>
                <div className={classes.ChartInformation}>
                  {
                    ((this.store.getState().selectWell[0] === undefined) ? "" : this.store.getState().selectWell.map((item ,index) => {
                      return <DynaChart 
                        key={index}
                        className=""
                        color='#29abe2'
                        fill='#E8F7FF'
                        store={store}
                        Loads={item.Loads} 
                        Positions={item.Positions} 
                        width='800'
                        height='470'
                        title={item.WellName} 
                        subTitle={item.CardDateTime} 
                        hover={false}
                        border={false}
                        onclick={false}
                      />
                    }))
                  }
                </div>
              </Grid>
              <Grid item sm={6}>
                <div className={classes.WellClusterChart}>Well Cluster Chart</div>
                <div className={classes.ChartPointInformation}>
                  {
                    ((this.store.getState().PointChart[0] === undefined) ? "" : this.store.getState().PointChart.map((item, index) => {
                      return <PointChart 
                        key={index}
                        PointChart={item}
                        store={store}
                        width={800}
                        height={560}
                      />
                    }))
                  }
                </div>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </div>
    )
  }
}

export default withStyles(styles)(BeamViewSelect);