import React, { Component } from 'react';
import withStyles from '@material-ui/styles/withStyles';
import { red } from '@material-ui/core/colors';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import FormHelperText from '@material-ui/core/FormHelperText';
import TextField from '@material-ui/core/TextField';
import Grid from "@material-ui/core/Grid";
import Button from '@material-ui/core/Button';

// More Function
import API from './../../api/server'
import _ from "underscore"
import moment from 'moment'
import { setesprawdata, esptablesensor, ESPPREDICT } from './../../actions';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  root: {
    maxWidth: '100%',
    flexGrow: 1,
  },
  content: {
    fontSize: "16px"
  },
  media: {
    height: 0,
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
  formControl: {
    minWidth: "100%",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  buttonMargin: {
    marginTop: "12px",
    width: '100%'
  },
  buttonFloat: {
    display: 'block',
    float: 'right'
  },
  containerCenter:{

  }
});

class ESPFrom extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.state = {
      device: [],
      selectDevice: 'LKU-Z11',
      selectDateStart: "2015-04-09",
      selectDateEnd: "2015-04-10",
      selectMonth: "มกราคม",
      selectYear: 2020
    }
    this.handleDevice = this.handleDevice.bind(this);
    this.handleDatePickerStart = this.handleDatePickerStart.bind(this)
    this.handleDatePickerEnd = this.handleDatePickerEnd.bind(this)
    this.handleViewData = this.handleViewData.bind(this)
  }
  async componentDidMount() {
    const device = await API().get('/EspSensorDevice')
    this.setState({
      device: device.data.device
    })
  }
  handleDevice (ev){
    this.setState({selectDevice: ev.target.value});
  }
  handleDatePickerStart (ev){
    this.setState({selectDateStart: ev.target.value});
  }
  handleDatePickerEnd (ev){
    this.setState({selectDateEnd: ev.target.value});
  }
  async handleViewData(){
    // 2015-04-10 00:00:00
    this.store.dispatch(setesprawdata([undefined]))
    const res = await API().post('/EspSensor', {
      "WellName": this.state.selectDevice ,
      "start": this.state.selectDateStart + " 00:00:00",
      "end": this.state.selectDateEnd + " 00:00:00"
    })

    localStorage.setItem('DateState', this.state.selectDateStart + " 00:00:00");
    localStorage.setItem('DateEnd', this.state.selectDateEnd + " 00:00:00");
    localStorage.setItem('Device', this.state.selectDevice);

    this.store.dispatch(setesprawdata([res.data]))
    const res2 = await API().post('/EspSensorPredict')
    res2.data.data = res.data.predict
    _.each(res.data.data, function(data){
      data.tag = "current"
    });
    _.each(res2.data.data, function(data){
      data.tag = "predict"
    });
    this.store.dispatch(ESPPREDICT(res2.data.data))
    var newArraySensor = res.data.data.concat(res2.data.data)
    _.each(newArraySensor, function(data){
      data.Time = new Date(moment(data.Time).add(-7, 'hours').format("LLLL"))
    });

    this.store.dispatch(esptablesensor(newArraySensor))
  }
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item sm={2}>
            <FormControl className={classes.formControl}>
              <InputLabel shrink htmlFor="select-native-label-placeholder">
                เลือกอุปกรณ์
              </InputLabel>
              <NativeSelect
                value={this.state.selectDevice}
                onChange={this.handleDevice }
                inputProps={{
                  name: 'Select Device',
                  id: 'select-native-label-placeholder',
                }}
              >
                <option aria-label="None" value="" />
                {
                  this.state.device.map((item, index) => {
                    return (<option key={index} value={item}>{item}</option>)
                  })
                }
              </NativeSelect>
              <FormHelperText></FormHelperText>
            </FormControl>
          </Grid>
          <Grid item sm={2}>
            <FormControl className={classes.formControl}>
              <InputLabel shrink htmlFor="select-native-label-placeholder">
                เลือกเดือน
              </InputLabel>
              <NativeSelect
                value={this.state.selectMonth}
                onChange={this.handleMonth }
                inputProps={{
                  name: 'Select Device',
                  id: 'select-native-label-placeholder',
                }}
              >
                <option aria-label="None" value="" />
                {
                  ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"].map((item, index) => {
                    return (<option key={index} value={item}>{item}</option>)
                  })
                }
              </NativeSelect>
              <FormHelperText></FormHelperText>
            </FormControl>         
          </Grid>
          <Grid item sm={2}>
          <FormControl className={classes.formControl}>
              <InputLabel shrink htmlFor="select-native-label-placeholder">
                เลือกปี
              </InputLabel>
              <NativeSelect
                value={this.state.selectYear}
                onChange={this.handleYear }
                inputProps={{
                  name: 'Select Device',
                  id: 'select-native-label-placeholder',
                }}
              >
                <option aria-label="None" value="" />
                {
                  [1998,1999,2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,2024].map((item, index) => {
                    return (<option key={index} value={item}>{item}</option>)
                  })
                }
              </NativeSelect>
              <FormHelperText></FormHelperText>
            </FormControl>
          </Grid>
          <Grid item sm={2}>        
          </Grid>
          <Grid item sm={2}></Grid>
          <Grid item sm={2}>    
            <div className={classes.buttonFloat}>  
              <Button 
                className={classes.buttonMargin}
                variant="outlined" 
                color="default" 
                onClick={this.handleViewData}
              >
                เลือกดูข้อมูล
              </Button>
            </div>
          </Grid>
        </Grid>
      </div>
    )
  }
}

export default withStyles(styles)(ESPFrom);
