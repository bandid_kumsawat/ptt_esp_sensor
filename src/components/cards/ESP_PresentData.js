import React, { Component } from 'react';
import withStyles from '@material-ui/styles/withStyles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { Grid } from '@material-ui/core';

// this is Component
import ESPLineChart9 from './Chart/ESPLineChart9'
import ESPAnomorly from './Chart/ESPAnomorly'
import ESPSelectLine from './ESP_SelectLine'
import ESPFrom from './ESP_From'
import ESPShowTime from './ESPShowTime'
import API from './../../api/server'
import { setesprawdata, esptablesensor } from './../../actions';
import _ from 'underscore'
import moment from 'moment'
// import { add } from 'numeral';
import ESPTableData from './ESP_TableData'

const styles = theme => ({
  root: {
    maxWidth: '100%',
    flexGrow: 1,
  },
  hieghtlightchart: {
    borderTop: "3px solid #0a88be"
  },
  content: {
    fontSize: "16px"
  },
  media: {
    height: 0,
    // paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  formControl: {
    minWidth: "100%",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  buttonMargin: {
    marginTop: "12px",
    width: '100%'
  },
  buttonFloat: {
    display: 'block',
    float: 'left'
  },
  containerCenter:{

  },
  title: {
    width: "100%",
    textAlign: "center",
    margin: "20px",
    padding: "8px",
    borderBottom: '1px solid #0000007a',
    fontSize: '20px'
  },
  title2: {
    width: "100%",
    textAlign: "center",
    margin: "0px",
    padding: "8px",
    fontSize: '20px'
  },
  spacingPadding: {
    paddingTop: "2%",
    paddingRight: "2%",
    paddingLeft: "2%",
  }
});

class BeamViewSelect extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.myElement = React.createRef()
    this.widthElement = null
    this.state = {

    }
  }

  async componentDidMount() {
    this.widthElement = this.myElement.current.offsetWidth
    this.store.dispatch(setesprawdata([undefined]))
    const res = await API().post('/EspSensor', {
      "WellName": "LKU-Z11",
      "start": "2016-01-01 00:00:00",
      "end": "2016-01-31 00:00:00"
    })
    this.store.dispatch(setesprawdata([res.data]))
    const res2 = await API().post('/EspSensorPredict')
    _.each(res.data.data, function(data){
      data.tag = "current"
    });
    _.each(res2.data.data, function(data){
      data.tag = "predict"
    });
    var newArraySensor = res.data.data.concat(res2.data.data)
    _.each(newArraySensor, function(data){
      data.Time = new Date(moment(data.Time).add(-7, 'hours').format("LLLL"))
    });
    this.store.dispatch(esptablesensor(newArraySensor))
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Card className={classes.hieghtlightchart}>
          <CardContent>
            <Grid container spacing={1} >
              <Grid item sm={12} className={classes.spacingPadding}>
                <ESPFrom store={this.store}/>
              </Grid>
              {/* <Grid item sm={12} className={classes.spacingPadding}>
                <ESPShowTime store={this.store}/>
              </Grid> */}
              <Grid item sm={12} className={classes.spacingPadding} ref={this.myElement}>
                {
                  // ((
                  //   this.widthElement !== null &&
                  //   this.store.getState().ESPRentderChart &&
                  //   this.store.getState().ESPRawData[0] !== undefined
                  // ) ? 
                  //   <ESPShowTime store={this.store}/>
                  //     : ""
                  // )
                }
                {
                  ((
                    this.widthElement !== null &&
                    this.store.getState().ESPRentderChart &&
                    this.store.getState().ESPRawData[0] !== undefined &&
                    this.store.getState().ESPRawData[0].length !== 0 &&

                    this.store.getState().ESPPredict &&
                    this.store.getState().ESPPredict[0] !== undefined &&
                    this.store.getState().ESPPredict[0].length !== 0 
                  ) ? 
                    <ESPLineChart9
                      store={this.store} 
                      data={this.store.getState().ESPRawData[0]}
                      predict={this.store.getState().ESPPredict[0]}
                      // dataPredict={this.store.getState().}
                      width={this.widthElement - 20}
                      height={500}
                      title={"ESP Line Chart"}
                      color={this.store.getState().ESPLineColor[0]}
                    /> : ""
                  )
                }
                <ESPSelectLine store={this.store} />
                <ESPAnomorly 
                  store={this.store} 
                  data={this.store.getState().ESPRawData[0]}
                  // dataPredict={this.store.getState().}
                  width={this.widthElement - 20}
                  height={500}
                  title={"ESP Line Chart"}
                />
              </Grid>
              {/* <Grid item sm={12} className={classes.spacingPadding}>
                <ESPSelectLine store={this.store} />
              </Grid> */}
              <Grid item sm={12} className={classes.spacingPadding}>
                {
                  ((this.store.getState().ESPShowTime[0] !== undefined && this.store.getState().ESPTableSensor[0] !== undefined) ? 
                    <ESPTableData data={this.store.getState().ESPTableSensor[0]} store={this.store} />
                  : "") 
                }
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </div>
    )
  }
}

export default withStyles(styles)(BeamViewSelect);