import React, { Component } from 'react';
import withStyles from '@material-ui/styles/withStyles';
import { red } from '@material-ui/core/colors';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import FormHelperText from '@material-ui/core/FormHelperText';
import TextField from '@material-ui/core/TextField';
import Grid from "@material-ui/core/Grid";
import Button from '@material-ui/core/Button';

// More Function
import API from './../../api/server'
import _ from "underscore"
import moment from 'moment'
import { setesprawdata, esptablesensor,ESPPREDICT } from './../../actions';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  root: {
    maxWidth: '100%',
    flexGrow: 1,
  },
  content: {
    fontSize: "16px"
  },
  media: {
    height: 0,
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
  formControl: {
    minWidth: "100%",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  buttonMargin: {
    marginTop: "12px",
    width: '100%'
  },
  buttonFloat: {
    display: 'block',
    float: 'right'
  },
  containerCenter:{

  }
});

class ESPFrom extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.state = {
      device: [],
      selectDevice: 'LKU-Z11',
      selectDateStart: (localStorage.getItem('DateState') === null ? "2015-04-09" : localStorage.getItem('DateState')),
      selectDateEnd: (localStorage.getItem('DateEnd') === null ? "2015-04-10" : localStorage.getItem('DateEnd')),
    }
    this.handleDevice = this.handleDevice.bind(this);
    this.handleDatePickerStart = this.handleDatePickerStart.bind(this)
    this.handleDatePickerEnd = this.handleDatePickerEnd.bind(this)
    this.handleViewData = this.handleViewData.bind(this)
  }
  async componentDidMount() {
    const device = await API().get('/EspSensorDevice')
    this.setState({
      device: device.data.device
    })
    if (localStorage.getItem('DateState') === null || localStorage.getItem('DateEnd') === null){
      localStorage.setItem('Device', 'LKU-Z11')
      localStorage.setItem('DateState', '2015-04-09')
      localStorage.setItem('DateEnd', '2015-04-10')
    }
    this.setState({
      device: device.data.device,
      selectDateStart: localStorage.getItem('DateState'),
      selectDateEnd: localStorage.getItem('DateEnd'),
    })
    this.handleViewData()
  }
  handleDevice (ev){
    this.setState({selectDevice: ev.target.value});
  }
  handleDatePickerStart (ev){
    this.setState({selectDateStart: ev.target.value});
  }
  handleDatePickerEnd (ev){
    this.setState({selectDateEnd: ev.target.value});
  }
  async handleViewData(){
    
    var monthsDiff = moment(this.state.selectDateEnd).diff(moment(this.state.selectDateStart), 'months');
    
    if (monthsDiff > 1){
      alert("สามารถเลือกดูข้อมูลได้ 1 เดือน")
    }else{
      this.store.dispatch(setesprawdata([undefined]))
      const res = await API().post('/EspSensor', {
        "WellName": this.state.selectDevice ,
        "start": this.state.selectDateStart + " 00:00:00",
        "end": this.state.selectDateEnd + " 00:00:00"
      })
      if (res.data.error === undefined){
        console.log('setstate')
        localStorage.setItem('DateState', this.state.selectDateStart);
        localStorage.setItem('DateEnd', this.state.selectDateEnd);
        localStorage.setItem('Device', this.state.selectDevice);
        this.store.dispatch(setesprawdata([res.data]))
        const res2 = await API().post('/EspSensorPredict')
        res2.data.data = res.data.predict
        _.each(res.data.data, function(data){
          data.tag = "current"
        });
        _.each(res2.data.data, function(data){
          data.tag = "predict"
        });
        this.store.dispatch(ESPPREDICT(res2.data.data))
        var newArraySensor = res.data.data.concat(res2.data.data)
        console.log(newArraySensor)
        _.each(newArraySensor, function(data){
          data.Time = new Date(moment(data.Time).add(-7, 'hours').format("LLLL"))
        });
        this.store.dispatch(esptablesensor(newArraySensor))
      }
    }

  }
  render() {
    const { classes } = this.props;
    
    return (
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item sm={2}>
            <FormControl className={classes.formControl}>
              <InputLabel shrink htmlFor="select-native-label-placeholder">
                เลือกอุปกรณ์
              </InputLabel>
              <NativeSelect
                value={this.state.selectDevice}
                onChange={this.handleDevice }
                inputProps={{
                  name: 'Select Device',
                  id: 'select-native-label-placeholder',
                }}
              >
                <option aria-label="None" value="" />
                {
                  this.state.device.map((item, index) => {
                    return (<option key={index} value={item}>{item}</option>)
                  })
                }
              </NativeSelect>
              <FormHelperText></FormHelperText>
            </FormControl>
          </Grid>
          <Grid item sm={2}>
            <FormControl className={classes.formControl}>
              <TextField
                id="date"
                label="เลือกวันที่เริ่มต้น"
                type="date"
                defaultValue={this.state.selectDateStart}
                className={classes.textField}
                onChange={this.handleDatePickerStart}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </FormControl>            
          </Grid>
          <Grid item sm={2}>
            <FormControl className={classes.formControl}>
              <TextField
                id="date"
                label="เลือกเวลาสิ้นสุด"
                type="date"
                defaultValue={this.state.selectDateEnd}
                className={classes.textField}
                onChange={this.handleDatePickerEnd}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </FormControl>
          </Grid>
          <Grid item sm={2}>        
          </Grid>
          <Grid item sm={2}></Grid>
          <Grid item sm={2}>    
            <div className={classes.buttonFloat}>  
              <Button 
                className={classes.buttonMargin}
                variant="outlined" 
                color="default" 
                onClick={this.handleViewData}
              >
                เลือกดูข้อมูล
              </Button>
            </div>
          </Grid>
        </Grid>
      </div>
    )
  }
}

export default withStyles(styles)(ESPFrom);
