import axios from 'axios'

export default (token) => {
  var api = axios.create({
    // baseURL: 'http://127.0.0.1:11111'
    baseURL: 'http://127.0.0.1:4000/api'
    // baseURL: 'https://espsensors.info/api'
    // baseURL: 'http://192.168.1.143:11111'
  })
  api.defaults.headers.common['Authorization'] = 'Bearer ' + token
  return api
}