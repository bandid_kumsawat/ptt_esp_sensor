import React from 'react'
import { Route, HashRouter, Switch } from 'react-router-dom'
// import Dashboard from './components/Dashboard'
// import Wizard from './components/Wizard'
import ESPSensor from './../components/ESPSensor'
import ESPLogin from './../components/ESPLogin'
// import Current from './../components/Current'

// import Main from './components/Main'
// import Signup from './components/Signup'
import ScrollToTop from './../components/ScrollTop'

// import test from './../components/test'

export default props => (
    <HashRouter>
      <ScrollToTop>
        <Switch>
        <Route exact path='/' component={ ESPSensor } />
        <Route exact path='/ESPSensor' component={ ESPSensor } />
        <Route exact path='/login' component={ ESPLogin } />
        {/* <Route exact path='/History' component={ BeamPumpMainPage } /> */}
        </Switch>
      </ScrollToTop>
    </HashRouter>
  )