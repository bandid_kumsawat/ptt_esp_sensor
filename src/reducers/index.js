import initialState from './state'

export default (state = initialState, action) => {
  switch (action.type) {
    case 'RESULTWELLDATA':
      return Object.assign({}, state, {
        wellResult: action.payload
      })
    case "SELECTWELLDATA": 
      return Object.assign({}, state, {
        selectWell: action.payload
      })
    case "REFLASEVIEWCHART":
        return Object.assign({}, state, {
          refrash_view_chart: action.payload
        })
    case "SETTEMPWELLID":
        return Object.assign({}, state, {
          TempWellID: action.payload
        })
    case "SETPOINTCHART":
        return Object.assign({}, state, {
          PointChart: [action.payload]
        })
    case "SETPOINTCHARTSELECT":
      return Object.assign({}, state, {
        PointSelect: action.payload
      })
    case "SETCURRENTDEVICE":
      return Object.assign({}, state, {
        currentWell: action.payload
      })
    case "SETPREDICTION":
      return Object.assign({}, state, {
        currentWellPrediction: action.payload
      })
    case "SETPREDICTIONCURRENT":
      return Object.assign({}, state, {
        Prediction: action.payload
      })
    case "SETESPRAWDATA":
      return Object.assign({}, state, {
        ESPRawData: action.payload
      })
    case "SELECTFTHP":
      return Object.assign({}, state, {
        SelectFTHP: action.payload
      })
    case "SELECTFTHT":
      return Object.assign({}, state, {
        SelectFTHT: action.payload
      })
    case "SELECTCURRENT":
      return Object.assign({}, state, {
        SelectCurrent: action.payload
      })
    case "SELECTFLOWRATE":
      return Object.assign({}, state, {
        SelectFlowRate: action.payload
      })
    case "SELECTFREQUENCY":
      return Object.assign({}, state, {
        SelectFrequency: action.payload
      })
    case "SELECTMOTORTEMP":
      return Object.assign({}, state, {
        SelectMotorTemp: action.payload
      })
    case "SELECTSUCTIONPRESSURE":
      return Object.assign({}, state, {
        SelectSuctionPressure: action.payload
      })
    case "SELECTSUCTIONTEMP":
      return Object.assign({}, state, {
        SelectSuctionTemp: action.payload
      })
    case "SELECTVOLTAGE":
      return Object.assign({}, state, {
        SelectVoltage: action.payload
      })

    case "ESPRENTDERCHART":
      return Object.assign({}, state, {
        ESPRentderChart: action.payload
      })
    
    case "ESPLINECOLOR":
      return Object.assign({}, state, {
        ESPLineColor: action.payload
      })
    
    case "ESPTICKCOLOR":
      return Object.assign({}, state, {
        ESPTickColor: action.payload
      })

    case "ESPCHARTSETVALUE":
      return Object.assign({}, state, {
        ESPChartSetValue: [action.payload]
      })

    case "ESPSHOWTIME": 
      return Object.assign({}, state, {
        ESPShowTime: [action.payload]
      })

    case "ESPTABLESENSOR":
      return Object.assign({}, state, {
        ESPTableSensor: [action.payload]
      })
    
    case "ESPSETLINECOLOR": 
      return Object.assign({}, state, {
        ESPSetLineColor: [action.payload]
      })

    case "ESPSETCOLORLINE":
      return Object.assign({}, state, {
        ESPSetColorLine: [action.payload]
      })
    case "ESPPREDICT": 
      return Object.assign({}, state, {
        ESPPredict: [action.payload]
      })
    case "ACCESSTOKEN": 
      return Object.assign({}, state, {
        AccessToken: action.payload
      })
    case "TYPE": 
      return Object.assign({}, state, {
        type: action.payload
      })
    case "EMAIL": 
      return Object.assign({}, state, {
        email: action.payload
      })
    default:
      return state
  }
}
