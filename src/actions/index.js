export const addresultwelldata = (payload) => {
  return {
    type: 'RESULTWELLDATA',
    payload
  }
}
export const addwellselect = (payload) => {
  return {
    type: "SELECTWELLDATA",
    payload
  }
}

export const reflasheviewchart = (payload) => {
  return {
    type: "REFLASEVIEWCHART",
    payload
  }
} 

export const settempwellid = (payload) => {
  return {
    type: "SETTEMPWELLID",
    payload
  }
}

export const setpointchart = (payload) => {
  return {
    type : "SETPOINTCHART",
    payload
  }
}

export const setpointselect = (payload) => {
  return {
    type : "SETPOINTCHARTSELECT",
    payload
  }
}

export const setcurrentdevice = (payload) => {
  return {
    type: 'SETCURRENTDEVICE',
    payload 
  }
}

export const setprediction = (payload) => {
  return {
    type: "SETPREDICTION",
    payload
  }
}

export const setpredictioncurrent = (payload) => {
  return {
    type: "SETPREDICTIONCURRENT",
    payload
  }
}

export const setesprawdata = (payload) => {
  return {
    type: "SETESPRAWDATA",
    payload
  }
}

export const selectfthp = (payload) => {
  return {
    type: "SELECTFTHP",
    payload
  }
}
export const selectftht = (payload) => {
  return {
    type: "SELECTFTHT",
    payload
  }
}
export const selectcurrent = (payload) => {
  return {
    type: "SELECTCURRENT",
    payload
  }
}
export const selectflowrate = (payload) => {
  return {
    type: "SELECTFLOWRATE",
    payload
  }
}
export const selectmotortemp = (payload) => {
  return {
    type: "SELECTMOTORTEMP",
    payload
  }
}
export const selectsuctionpressure = (payload) => {
  return {
    type: "SELECTSUCTIONPRESSURE",
    payload
  }
}

export const selectsuctiontemp = (payload) => {
  return {
    type: "SELECTSUCTIONTEMP",
    payload
  }
}
export const selectvoltage = (payload) => {
  return {
    type: "SELECTVOLTAGE",
    payload
  }
}
export const selectfrequency = (payload) => {
  return {
    type: "SELECTFREQUENCY",
    payload
  }
}

export const esprentderchart = (payload) => {
  return {
    type: 'ESPRENTDERCHART',
    payload
  }
}

export const esplinecolor = (payload) => {
  return {
    type: 'ESPLINECOLOR',
    payload
  }
}

export const esptickcolor = (payload) => {
  return {
    type: 'ESPTICKCOLOR',
    payload
  }
}

export const espchartsetvalue = (payload) => {
  return {
    type: "ESPCHARTSETVALUE",
    payload
  }
}

export const espshowtime = (payload) => {
  return {
    type: "ESPSHOWTIME",
    payload
  }
}

export const esptablesensor = (payload) => {
  return {
    type: "ESPTABLESENSOR",
    payload
  }
}

export const espsetlinecolor = (payload) => {
  return {
    type: "ESPSETLINECOLOR",
    payload
  }
}

export const espsetcolorline = (payload) => {
  return {
    type: 'ESPSETCOLORLINE',
    payload
  }
}

export const ESPPREDICT = (payload) => {
  return {
    type:'ESPPREDICT',
    payload
  }
}

export const accesstoken = (payload) => {
  return {
    type:'ACCESSTOKEN',
    payload
  }
}

export const email = (payload) => {
  return {
    type: "EMAIL",
    payload
  }
}

export const type = (payload) => {
  return {
    type: "TYPE",
    payload
  }
}