import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import './components/cards/Chart/PointChart.css'
import { store } from './store'

function render () {
  ReactDOM.render(
    <div className="container">
      <App store={store} />
      {/* <Results store={store} /> */}
    </div>
    ,
    document.getElementById('root')
  );
}

store.subscribe(render);

render();
